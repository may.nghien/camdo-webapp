package customer

import (
	"Golang/internal/controller/customer"
	"github.com/go-chi/chi"
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/volatiletech/null/v8"
)

type EditCustomerInp struct {
	CustomerID   int64       `boil:"customer_id" json:"customer_id" toml:"customer_id" yaml:"customer_id"`
	CustomerName string      `boil:"customer_name" json:"customer_name" toml:"customer_name" yaml:"customer_name"`
	CCCD         null.String `boil:"cccd" json:"cccd,omitempty" toml:"cccd" yaml:"cccd,omitempty"`
	DayCCCD      null.Time   `boil:"day_cccd" json:"day_cccd,omitempty" toml:"day_cccd" yaml:"day_cccd,omitempty"`
	AddressCCCD  null.String `boil:"address_cccd" json:"address_cccd,omitempty" toml:"address_cccd" yaml:"address_cccd,omitempty"`
	Gender       null.String `boil:"gender" json:"gender,omitempty" toml:"gender" yaml:"gender,omitempty"`
	Address      null.String `boil:"address" json:"address,omitempty" toml:"address" yaml:"address,omitempty"`
	BirthDate    null.Time   `boil:"birth_date" json:"birth_date,omitempty" toml:"birth_date" yaml:"birth_date,omitempty"`
	Phone        null.String `boil:"phone" json:"phone,omitempty" toml:"phone" yaml:"phone,omitempty"`
}
type EditCustomerInput struct {
	CustomerID   int64
	CustomerName string
	CCCD         null.String
	DayCCCD      null.Time
	AddressCCCD  null.String
	Gender       null.String
	Address      null.String
	BirthDate    null.Time
	Phone        null.String
}

func validateDataInputEdit(inp EditCustomerInp) (customer.EditCustomerInput, error) {
	return customer.EditCustomerInput{CustomerID: inp.CustomerID,
		CustomerName: inp.CustomerName,
		CCCD:         null.String(inp.CCCD),
		Gender:       null.String(inp.Gender),
		Address:      inp.Address,
		BirthDate:    inp.BirthDate,
		Phone:        inp.Phone,
		AddressCCCD:  inp.AddressCCCD,
		DayCCCD:      inp.DayCCCD,
	}, nil
}
func RespondWithJSON(w http.ResponseWriter, status int, data interface{}) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)

	enc := json.NewEncoder(w)
	enc.SetIndent("", "  ")
	enc.Encode(data)
}
func (h Handler) EditCustomer(w http.ResponseWriter, r *http.Request) {
	idStr := chi.URLParam(r, "id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		http.Error(w, "Invalid orderID", http.StatusBadRequest)
	}
	var req EditCustomerInp
	err = json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	input,err := validateDataInputEdit(req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	res, err := h.customerController.EditCustomer(int64(id), input)
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Trả về thông tin order đã được cập nhật
	RespondWithJSON(w, http.StatusOK, res)


}
