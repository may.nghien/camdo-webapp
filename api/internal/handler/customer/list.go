package customer

import (
	"github.com/go-chi/chi"
	"encoding/json"
	"net/http"
	"strconv"
)

func (h Handler) GetListCustomer(w http.ResponseWriter, r *http.Request) {
	res, err := h.customerController.ListCustomer()
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Send token as response
	json.NewEncoder(w).Encode(res)
}

func (h Handler) GetNewestCustomer(w http.ResponseWriter, r *http.Request) {
	res, err := h.customerController.GetNewestCustomer()
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Send token as response
	json.NewEncoder(w).Encode(res)
}

func (h Handler) GetCustomerByID(w http.ResponseWriter, r *http.Request) {
	customerIDStr := chi.URLParam(r, "id")
	customerID, err := strconv.Atoi(customerIDStr)
	res, err := h.customerController.GetCustomerByID(int64(customerID))
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Send token as response
	json.NewEncoder(w).Encode(res)
}
