package customer

import (
	"encoding/json"
	"net/http"

)

type InputSearch struct {
	Name string `json:"name"`
}

type InputSearchPhone struct {
	Phone string `json:"phone"`
}

type InputSearchCCCD struct {
	CCCD string `json:"cccd"`
}

func (h Handler) SearchByCustomerName(w http.ResponseWriter, r *http.Request) {
	var req InputSearch
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	res, err := h.customerController.SearchCustomerByName(req.Name)
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Send token as response
	json.NewEncoder(w).Encode(res)
}
func (h Handler) SearchByCustomerPhone(w http.ResponseWriter, r *http.Request) {
	var req InputSearchPhone
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	res, err := h.customerController.SearchCustomerByPhone(req.Phone)
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Send token as response
	json.NewEncoder(w).Encode(res)
}

func (h Handler) SearchByCustomerCCCD(w http.ResponseWriter, r *http.Request) {
	var req InputSearchCCCD
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	res, err := h.customerController.SearchCustomerByCCCD(req.CCCD)
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Send token as response
	json.NewEncoder(w).Encode(res)
}