package order_goal

import "Golang/internal/controller/order_goal"

type Handler struct {
	orderController order_goal.Controller
}

func New(orderController order_goal.Controller) Handler {
	return Handler{orderController: orderController}
}
