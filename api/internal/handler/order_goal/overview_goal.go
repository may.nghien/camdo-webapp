package order_goal

import (
	"encoding/json"
	"net/http"
)

type OverviewStruct struct {
	Month int `json:"month"`
	Year  int `json:"year"`
}
func (h Handler) ExportOverViewGoal(w http.ResponseWriter, r *http.Request){
	var req OverviewStruct
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	data, err := h.orderController.ExportOverView(req.Month, req.Year)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	
	json.NewEncoder(w).Encode(data)
}
func (h Handler) GetOverView(w http.ResponseWriter, r *http.Request) {
	var req OverviewStruct
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	res, err:= h.orderController.GetOverView(req.Month, req.Year)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	// Send token as response
	json.NewEncoder(w).Encode(res)

}

func (h Handler) GetOrdersByMonthAndYear(w http.ResponseWriter, r *http.Request) {
	var req OverviewStruct
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	order, err := h.orderController.ListOrderGoalByMonthAndYear(req.Month, req.Year)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	json.NewEncoder(w).Encode(order)
}
func (h Handler) GetAddPaymentByMonthAndYear(w http.ResponseWriter, r *http.Request) {
	var req OverviewStruct
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	order, err := h.orderController.ListAddPaymentByMonthAndYear(req.Month, req.Year)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	json.NewEncoder(w).Encode(order)
}


func (h Handler) GetListPaymentsGoalByMonthAndYear(w http.ResponseWriter, r *http.Request) {
	var req OverviewStruct
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	res, err := h.orderController.ListPaymentByMonthAndYear(req.Month, req.Year)
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Send token as response
	json.NewEncoder(w).Encode(res)
}

func (h Handler) GetListRedeemGoalByMonthAndYear(w http.ResponseWriter, r *http.Request) {
	var req OverviewStruct
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	res, err := h.orderController.ListRedeemByMonthAndYear(req.Month, req.Year)
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Send token as response
	json.NewEncoder(w).Encode(res)
}