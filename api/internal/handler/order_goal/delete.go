package order_goal

import (
	"net/http"
	"strconv"

	"github.com/go-chi/chi"
)

func (h Handler) DeleteOrderGoalByID(w http.ResponseWriter, r *http.Request) {
	orderIDStr := chi.URLParam(r, "orderID")
	orderID, err := strconv.Atoi(orderIDStr)
	if err != nil {
		http.Error(w, "Invalid orderID", http.StatusBadRequest)
		return
	}

	err = h.orderController.DeleteOrderGoal(r.Context(),int64(orderID))
	if err != nil {
		http.Error(w, "Lỗi trong quá trình xóa sản phẩm", http.StatusInternalServerError)
		return
	}

	
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Sản phẩm đã được xóa thành công"))
}
