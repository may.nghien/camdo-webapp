package order_goal

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/go-chi/chi"
)

func (h Handler) GetOrderGoal(w http.ResponseWriter, r *http.Request) {
	res, err := h.orderController.GetOrderGoal()
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Send token as response
	json.NewEncoder(w).Encode(res)
}

func (h Handler) GetOrderGoalByID(w http.ResponseWriter, r *http.Request) {
	orderIDStr := chi.URLParam(r, "orderID")
	orderID, err := strconv.Atoi(orderIDStr)
	if err != nil {
		http.Error(w, "Invalid orderID", http.StatusBadRequest)
		return
	}
	res, err := h.orderController.GetOrderGoalByID(int64(orderID))
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Send token as response
	json.NewEncoder(w).Encode(res)
}

func (h Handler) GetOrderGoalByCustomerID(w http.ResponseWriter, r *http.Request) {
	orderIDStr := chi.URLParam(r, "customerID")
	orderID, err := strconv.Atoi(orderIDStr)
	if err != nil {
		http.Error(w, "Invalid orderID", http.StatusBadRequest)
		return
	}
	res, err := h.orderController.GetOrderGoalByCustomerID(int64(orderID))
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Send token as response
	json.NewEncoder(w).Encode(res)
}

func (h Handler) GetNewestOrderGoal(w http.ResponseWriter, r *http.Request) {
	res, err := h.orderController.GetNewestOrderGoal()
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Send token as response
	json.NewEncoder(w).Encode(res)
}

func (h Handler) GetOrderGoalOver(w http.ResponseWriter, r *http.Request) {
	res, err := h.orderController.ListOrdersGoalOver()
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	// Send token as response
	json.NewEncoder(w).Encode(res)
}

func (h Handler) GetProductsByOrderID(w http.ResponseWriter, r *http.Request) {
	orderIDStr := chi.URLParam(r, "orderID")
	orderID, err := strconv.Atoi(orderIDStr)
	if err != nil {
		http.Error(w, "Invalid orderID", http.StatusBadRequest)
		return
	}
	res, err := h.orderController.GetProductListByOrderID(int64(orderID))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	json.NewEncoder(w).Encode(res)
}
func (h Handler) GetOrderGoldAndProducts(w http.ResponseWriter, r *http.Request) {
	res, err := h.orderController.GetOrderGoalAndProducts()
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	json.NewEncoder(w).Encode(res)
}
