package order_goal

import (
	"Golang/internal/controller/order_goal"
	models "Golang/internal/repository/dbmodel"
	"github.com/go-chi/chi"
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/volatiletech/null/v8"
)

type RedeemOutput struct {
	ID         int64                       `json:"id"`
	Status     null.String                 `json:"status"`
	ModelInput models.InterestPaymentsGoal `json:"model_input"`
	CreateRedeemInput models.RedeemOrderGoal `json:"create_redeem"`
}

func validateDataInputRedeem(inp RedeemOutput) (order_goal.RedeemInput, error) {
	return order_goal.RedeemInput{
		ID:         inp.ID,
		Status:     inp.Status,
		ModelInput: inp.ModelInput,
		CreateRedeemInput: inp.CreateRedeemInput,
	}, nil
}
func (h Handler) RedeemOrderGoal(w http.ResponseWriter, r *http.Request) {
	var input RedeemOutput
	err := json.NewDecoder(r.Body).Decode(&input)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	inputVali, err := validateDataInputRedeem(input)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	redeem, err := h.orderController.RedeemOrderGoal(inputVali)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	json.NewEncoder(w).Encode(redeem)

}


func (h Handler) GetListRedeemGoalByOrderID(w http.ResponseWriter, r *http.Request) {
	idStr := chi.URLParam(r, "orderID")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	redeem, err := h.orderController.ListRedeemGoal(int64(id))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	json.NewEncoder(w).Encode(redeem)

}


func (h Handler) GetListRedeemGoal(w http.ResponseWriter, r *http.Request) {

	redeem, err := h.orderController.GetListRedeemGoal()
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	json.NewEncoder(w).Encode(redeem)

}