package order_goal

import (
	"encoding/json"
	"net/http"
)


func (h Handler) SearchOrderGoalByID(w http.ResponseWriter, r *http.Request) {
	var input InputSearchID
	err := json.NewDecoder(r.Body).Decode(&input)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	order, err := h.orderController.SearchOrderGoalByID(input.ID)
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Send token as response
	json.NewEncoder(w).Encode(order)
}

