package order_goal

import (
	"encoding/json"
	"net/http"

	"github.com/volatiletech/null/v8"
)

type OrderInput struct {
	ID     int64       `json:"id"`
	Status null.String `json:"status"`
}

func (h Handler) UpdateStatusOrderGoal(w http.ResponseWriter, r *http.Request) {
	var input OrderInput
	err := json.NewDecoder(r.Body).Decode(&input)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	order, err := h.orderController.UpdateStatus(input.ID, input.Status)
	if err != nil {
		http.Error(w, "Invalid orderID", http.StatusBadRequest)
		return
	}

	json.NewEncoder(w).Encode(order)

}
