package order_goal

import (
	"encoding/json"
	"net/http"
)


func (h Handler) GetOrdersGoldToday(w http.ResponseWriter, r *http.Request) {

	order, err := h.orderController.ListOrderGoldToday()
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	json.NewEncoder(w).Encode(order)
}

func (h Handler) GetListPaymentsGoldToday(w http.ResponseWriter, r *http.Request) {

	res, err := h.orderController.ListPaymentToday()
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Send token as response
	json.NewEncoder(w).Encode(res)
}

func (h Handler) GetListRedeemGoldToday(w http.ResponseWriter, r *http.Request) {

	res, err := h.orderController.ListRedeemToday()
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Send token as response
	json.NewEncoder(w).Encode(res)
}
func (h Handler) GetListAddPaymentGoldToday(w http.ResponseWriter, r *http.Request) {

	res, err := h.orderController.ListAddPaymentToday()
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Send token as response
	json.NewEncoder(w).Encode(res)
}

func (h Handler) GetOverviewGoldToday(w http.ResponseWriter, r *http.Request){

	res, err := h.orderController.GetOverviewToday()
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Send token as response
	json.NewEncoder(w).Encode(res)
}