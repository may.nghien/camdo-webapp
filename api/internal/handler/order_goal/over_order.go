package order_goal

import (
	"encoding/json"
	"net/http"
)

type InputSearchID struct {
	ID int64 `json:"id"`
}
func (h Handler) GetOrderGoldOver(w http.ResponseWriter, r *http.Request) {
	res, err := h.orderController.GetOrderGoldOver()
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	// Send token as response
	json.NewEncoder(w).Encode(res)
}

func (h Handler) GetOrderGoldHavePaymentsOver(w http.ResponseWriter, r *http.Request) {
	res, err := h.orderController.GetOrderHavePaymentsOver()
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	// Send token as response
	json.NewEncoder(w).Encode(res)
}

func (h Handler) ListSearchOrderGoalOver(w http.ResponseWriter, r *http.Request) {
	var input InputSearchID
	err := json.NewDecoder(r.Body).Decode(&input)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	res, err := h.orderController.SearchOrderGoalOverByID(input.ID)
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	// Send token as response
	json.NewEncoder(w).Encode(res)
}

func (h Handler) ListSearchOrderGoalByPaymentOver(w http.ResponseWriter, r *http.Request) {
	var input InputSearchID
	err := json.NewDecoder(r.Body).Decode(&input)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	res, err := h.orderController.SearchOrderGoalOverPaymentByID(input.ID)
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	json.NewEncoder(w).Encode(res)
}
