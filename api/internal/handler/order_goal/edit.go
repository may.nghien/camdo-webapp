package order_goal

import (
	"Golang/internal/controller/order_goal"
	models "Golang/internal/repository/dbmodel"
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/go-chi/chi"
	"github.com/volatiletech/null/v8"
)

type EditOrderOutput struct {
	ProductName       null.String `boil:"product_name" json:"product_name,omitempty" toml:"product_name" yaml:"product_name,omitempty"`
	AddPayments       null.Int    `json:"add_payments"`
	Price             null.Int    `boil:"price" json:"price,omitempty" toml:"price" yaml:"price,omitempty"`
	OrderDate         null.Time   `boil:"order_date" json:"order_date,omitempty" toml:"order_date" yaml:"order_date,omitempty"`
	CustomerID        null.Int64  `boil:"customer_id" json:"customer_id,omitempty" toml:"customer_id" yaml:"customer_id,omitempty"`
	Percent           null.Int64  `boil:"percent" json:"percent,omitempty" toml:"percent" yaml:"percent,omitempty"`
	ReturnDate        null.Time   `boil:"return_date" json:"return_date,omitempty" toml:"return_date" yaml:"return_date,omitempty"`
	Status            null.String `boil:"status" json:"status,omitempty" toml:"status" yaml:"status,omitempty"`
	InterestPaidCount null.Int    `boil:"interest_paid_count" json:"interest_paid_count,omitempty" toml:"interest_paid_count" yaml:"interest_paid_count,omitempty"`
}

type OrderUpdate struct {
	ID                int64       `json:"id"`
	ProductName       null.String `json:"product_name,omitempty"`
	EstimatedPrice    null.Int    `json:"estimated_price,omitempty"`
	Price             null.Int    `json:"price,omitempty"`
	OrderDate         null.Time   `json:"order_date,omitempty"`
	CustomerID        null.Int64  `json:"customer_id,omitempty"`
	AddPayments       null.Int  `json:"add_payments,omitempty"`
	Percent           null.Int64  `json:"percent,omitempty"`
	ReturnDate        null.Time   `json:"return_date,omitempty"`
	Status            null.String `json:"status,omitempty"`
	InterestPaidCount null.Int    `json:"interest_paid_count,omitempty"`
}

type UpdateStatusInput struct {
	ID   int64       `json:"id"`
	Note null.String `json:"note"`
	Miss null.Int    `json:"miss"`
}

func validateDataInputEdit(inp CreateOrderOutput) (order_goal.OrderInputEdit, error) {
	return order_goal.OrderInputEdit{
		Status:            inp.Status,
		InterestPaidCount: inp.InterestPaidCount,
		ReturnDate:        inp.ReturnDate,
		Percent:           inp.Percent,
		ProductName:       null.String(inp.ProductName),
		AddPayments:       inp.AddPayments,
		Price:             null.Int(inp.Price),
		OrderDate:         null.Time(inp.OrderDate),
		CustomerID:        inp.CustomerID,
	}, nil
}
func GetOrderIDFromRequest(w http.ResponseWriter, r *http.Request) int64 {
	orderIDStr := chi.URLParam(r, "orderID")
	orderID, err := strconv.Atoi(orderIDStr)
	if err != nil {
		http.Error(w, "Invalid orderID", http.StatusBadRequest)
	}
	return int64(orderID)
}

func ParseOrderFromRequestBody(r *http.Request, order *models.OrderGoal) error {
	err := json.NewDecoder(r.Body).Decode(order)
	if err != nil {
		return err
	}
	defer r.Body.Close()

	return nil
}

func RespondWithJSON(w http.ResponseWriter, status int, data interface{}) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)

	enc := json.NewEncoder(w)
	enc.SetIndent("", "  ")
	enc.Encode(data)
}
func (h Handler) EditOrder(w http.ResponseWriter, r *http.Request) {
	// Trích xuất orderID từ URL hoặc body request (tùy thuộc vào thiết kế API của bạn)
	orderID := GetOrderIDFromRequest(w, r)
	var update models.OrderGoal
	err := ParseOrderFromRequestBody(r, &update)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Gọi hàm xử lý từ controller
	updatedOrder, err := h.orderController.EditOrder(orderID, update)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Trả về thông tin order đã được cập nhật
	RespondWithJSON(w, http.StatusOK, updatedOrder)
}

func (h Handler) UpdateStatus(w http.ResponseWriter, r *http.Request) {
	var req UpdateStatusInput
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	update, err := h.orderController.UpdateNote(req.Note, req.Miss, req.ID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(update)
}
