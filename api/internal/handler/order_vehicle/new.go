package ordervehicle

import "Golang/internal/controller/order_vehicle"

type Handler struct {
	orderController ordervehicle.Controller
}

func New(orderController ordervehicle.Controller) Handler {
	return Handler{orderController: orderController}
}
