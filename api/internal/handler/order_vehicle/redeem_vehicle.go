package ordervehicle

import (
	"Golang/internal/controller/order_vehicle"
	models "Golang/internal/repository/dbmodel"
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/go-chi/chi"
	"github.com/volatiletech/null/v8"
)

type RedeemOutput struct {
	ID                int64                       `json:"id"`
	Status            null.String                 `json:"status"`
	ModelInput        models.InterestPaymentsVehicle `json:"model_input"`
	CreateRedeemInput models.RedeemOrderVehicle      `json:"create_redeem"`
}

func validateDataInputRedeem(inp RedeemOutput) (ordervehicle.RedeemInput, error) {
	return ordervehicle.RedeemInput{
		ID:                inp.ID,
		Status:            inp.Status,
		ModelInput:        inp.ModelInput,
		CreateRedeemInput: inp.CreateRedeemInput,
	}, nil
}
func (h Handler) RedeemOrderVehicle(w http.ResponseWriter, r *http.Request) {
	var input RedeemOutput
	err := json.NewDecoder(r.Body).Decode(&input)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	inputVali, err := validateDataInputRedeem(input)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	redeem, err := h.orderController.RedeemOrderVehicle(inputVali)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	json.NewEncoder(w).Encode(redeem)

}

func (h Handler) GetListRedeemVehicle(w http.ResponseWriter, r *http.Request) {
	idStr := chi.URLParam(r, "orderID")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	redeem, err := h.orderController.ListRedeemVehicle(int64(id))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	json.NewEncoder(w).Encode(redeem)

}
