package ordervehicle

import (
	ordervehicle "Golang/internal/controller/order_vehicle"
	"encoding/json"
	"net/http"

	"github.com/volatiletech/null/v8"
)

type CreateOrderInput struct {
	ProductName       null.String
	AddPayments    null.Int
	Price             null.Int
	OrderDate         null.Time
	ReturnDate        null.Time
	CustomerID        null.Int64
	Percent           null.Int64
	LicensePlate      null.String
	Status            null.String
	InterestPaidCount null.Int
	Note              null.String
}

type CreateOrderOutput struct {
	ProductName       null.String  `boil:"product_name" json:"product_name,omitempty" toml:"product_name" yaml:"product_name,omitempty"`
	AddPayments       null.Int  `json:"add_payments,omitempty"`
	Price             null.Int     `boil:"price" json:"price,omitempty" toml:"price" yaml:"price,omitempty"`
	OrderDate         null.Time    `boil:"order_date" json:"order_date,omitempty" toml:"order_date" yaml:"order_date,omitempty"`
	CustomerID        null.Int64   `boil:"customer_id" json:"customer_id,omitempty" toml:"customer_id" yaml:"customer_id,omitempty"`
	Percent           null.Int64 `boil:"percent" json:"percent,omitempty" toml:"percent" yaml:"percent,omitempty"`
	LicensePlate      null.String  `boil:"license_plate" json:"license_plate,omitempty" toml:"license_plate" yaml:"license_plate,omitempty"`
	ReturnDate        null.Time    `boil:"return_date" json:"return_date,omitempty" toml:"return_date" yaml:"return_date,omitempty"`
	Status            null.String  `boil:"status" json:"status,omitempty" toml:"status" yaml:"status,omitempty"`
	InterestPaidCount null.Int     `boil:"interest_paid_count" json:"interest_paid_count,omitempty" toml:"interest_paid_count" yaml:"interest_paid_count,omitempty"`
	Note              null.String  `json:"note"`
}

func validateDataInput(inp CreateOrderOutput) (ordervehicle.OrderInput, error) {
	return ordervehicle.OrderInput{Percent: inp.Percent,
		ProductName:       null.String(inp.ProductName),
		AddPayments:    inp.AddPayments,
		Price:             null.Int(inp.Price),
		OrderDate:         null.Time(inp.OrderDate),
		ReturnDate:        null.Time(inp.ReturnDate),
		CustomerID:        inp.CustomerID,
		LicensePlate:      null.String(inp.LicensePlate),
		Note:              null.String(inp.Note),
		Status:            null.String(inp.Status),
		InterestPaidCount: null.Int(inp.InterestPaidCount),
	}, nil
}

func (h Handler) CreateOrderVehicle(w http.ResponseWriter, r *http.Request) {
	var req CreateOrderOutput
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	input, err := validateDataInput(req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	res, err := h.orderController.CreateOrderVehicle(input)
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Send token as response
	json.NewEncoder(w).Encode(res)

}
