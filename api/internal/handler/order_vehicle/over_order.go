package ordervehicle

import (
	"encoding/json"
	"net/http"
)

type InputSearchID struct {
	ID int64 `json:"id"`
}
func (h Handler) GetOrderVehicleOver(w http.ResponseWriter, r *http.Request) {
	res, err := h.orderController.GetOrderVehicleOver()
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	// Send token as response
	json.NewEncoder(w).Encode(res)
}

func (h Handler) GetOrderVehicleHavePaymentsOver(w http.ResponseWriter, r *http.Request) {
	res, err := h.orderController.GetOrderHavePaymentsOver()
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	// Send token as response
	json.NewEncoder(w).Encode(res)
}

func (h Handler) ListSearchOrderVehicleOver(w http.ResponseWriter, r *http.Request) {
	var input InputSearchID
	err := json.NewDecoder(r.Body).Decode(&input)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	res, err := h.orderController.SearchOrderVehicleOverByID(input.ID)
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	// Send token as response
	json.NewEncoder(w).Encode(res)
}
func (h Handler) ListSearchOrderVehicleByPaymentOver(w http.ResponseWriter, r *http.Request) {
	var input InputSearchID
	err := json.NewDecoder(r.Body).Decode(&input)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	paymentDate, err := h.orderController.SearchOrderVehicleOverPaymentByID(input.ID)
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	json.NewEncoder(w).Encode(paymentDate)
}

