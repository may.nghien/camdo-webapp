package ordervehicle

import (
	"encoding/json"
	"net/http"
)

type OverviewStruct struct {
	Month int `json:"month"`
	Year  int `json:"year"`
}
func (h Handler) ExportOverViewVehicle(w http.ResponseWriter, r *http.Request){
	var req OverviewStruct
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	data, err := h.orderController.ExportOverView(req.Month, req.Year)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	
	json.NewEncoder(w).Encode(data)
}
func (h Handler) GetOverView(w http.ResponseWriter, r *http.Request) {
	var req OverviewStruct
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	res, err:= h.orderController.GetOverView(req.Month, req.Year)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	// Send token as response
	json.NewEncoder(w).Encode(res)

}

func (h Handler) GetOrdersVehicleByMonthAndYear(w http.ResponseWriter, r *http.Request) {
	var req OverviewStruct
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	order, err := h.orderController.ListOrderVehicleByMonthAndYear(req.Month, req.Year)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	json.NewEncoder(w).Encode(order)
}

func (h Handler) GetListPaymentsVehicleByMonthAndYear(w http.ResponseWriter, r *http.Request) {
	var req OverviewStruct
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	res, err := h.orderController.ListPaymentByMonthAndYear(req.Month, req.Year)
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Send token as response
	json.NewEncoder(w).Encode(res)
}

func (h Handler) GetListRedeemVehicleByMonthAndYear(w http.ResponseWriter, r *http.Request) {
	var req OverviewStruct
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	res, err := h.orderController.ListRedeemByMonthAndYear(req.Month, req.Year)
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Send token as response
	json.NewEncoder(w).Encode(res)
}
func (h Handler) GetAddPaymentByMonthAndYear(w http.ResponseWriter, r *http.Request) {
	var req OverviewStruct
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	order, err := h.orderController.ListAddPaymentByMonthAndYear(req.Month, req.Year)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	json.NewEncoder(w).Encode(order)
}
