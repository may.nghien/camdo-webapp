package auth

import (
	"Golang/internal/controller/auth"
	"encoding/json"
	"errors"
	"net/http"
)

type Handler struct {
	authController auth.Controller
}

func New(authController auth.Controller) Handler {
	return Handler{authController: authController}
}

// Request body for sign in endpoint
type SignInRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

// Response body for sign in endpoint
type SignInResponse struct {
	Token string `json:"token"`
	Username string `json:"username"`
}

func validateSignInRequest(req SignInRequest) (auth.SignInInput, error) {
	if req.Username == "" {
		return auth.SignInInput{}, errors.New("Username is empty!")
	}
	if req.Password == "" {
		return auth.SignInInput{}, errors.New("Password is empty!")
	}
	return auth.SignInInput{Username: req.Username, Password: req.Password}, nil
}

func (h Handler) SignIn(w http.ResponseWriter, r *http.Request) {
	// Parse request body
	var req SignInRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	input, err := validateSignInRequest(req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	res, err := h.authController.SignIn(input)
	if err != nil {
		// handle controller errgn
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Send token as response
	resp := SignInResponse{Token: res.Token, Username: res.Username}
	json.NewEncoder(w).Encode(resp)

}
