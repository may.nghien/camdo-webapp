package auth

import (
	"Golang/internal/controller/auth"
	"encoding/json"
	"errors"
	"net/http"
	"strings"
)

type SignUpInput struct {
	Name     string
	Username string
	Gender   int
	Avatar   string
	ID       string
	Password string
}
type SignUpRequest struct {
	ID       string `json:"id"`
	Name     string `boil:"name" json:"name" toml:"name" yaml:"name"`
	Username string `boil:"username" json:"username" toml:"username" yaml:"username"`
	Gender   int    `boil:"gender" json:"gender" toml:"gender" yaml:"gender"`
	Avatar   string `boil:"avatar" json:"avatar" toml:"avatar" yaml:"avatar"`
	Password string `boil:"password" json:"password" toml:"password" yaml:"password"`
}

func validateDataSignUp(inp SignUpRequest) (auth.SignUpInput, error) {
	if strings.TrimSpace(inp.Username) == "" {
		return auth.SignUpInput{}, errors.New("Username is empty!")
	}
	if strings.TrimSpace(inp.Password) == "" {
		return auth.SignUpInput{}, errors.New("Password is empty!")
	}
	if strings.TrimSpace(inp.Avatar) == "" {
		return auth.SignUpInput{}, errors.New("Avatar is empty!")
	}
	if strings.TrimSpace(inp.Name) == "" {
		return auth.SignUpInput{}, errors.New("Name is empty!")
	}

	// Check if email already exists
	// existingUser, err := models.Account(qm.Where("username = ?", inp.Username)).One(ctx, db)
	// if err == nil {
	//     return auth.SignUpInput{}, errors.New("UserName is duplicate!")
	// }
	return auth.SignUpInput{Username: inp.Username, Password: inp.Password, Avatar: inp.Avatar, Name: inp.Name, ID: inp.ID, Gender: inp.Gender}, nil
}

func (h Handler) SignUp(w http.ResponseWriter, r *http.Request) {
	// Parse request body
	var req SignUpRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	input, err := validateDataSignUp(req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	res, err := h.authController.SignUp(input)
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Send token as response
	json.NewEncoder(w).Encode(res)

}
