package auth

import (
	"crypto/rand"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"math/big"
	"net/http"
	"net/smtp"
)

type OTPInput struct {
	OTP string `json:"otp"`
}

func generateOTP() string {
	// Sử dụng crypto/rand để tạo một số ngẫu nhiên có 6 chữ số
	num, err := rand.Int(rand.Reader, big.NewInt(9000))
	if err != nil {
		log.Fatal(err)
	}

	// Chuyển đổi số ngẫu nhiên thành chuỗi có 4 chữ số bằng cách thêm 1000 và lấy 4 chữ số cuối cùng
	otp := fmt.Sprintf("%04d", num.Int64()+1000)

	return "hoang"+otp
}
func (h Handler) SendOTP(w http.ResponseWriter, r *http.Request) {
	// Tạo mã OTP ngẫu nhiên
	otp := generateOTP()

	// Email thông tin
	// from := "ntnnga2509@gmail.com" // Email Gmail của bạn
	// password := "epgltfihhhgwgjkc"
	from := "lonelygirl.ngocnga@gmail.com" // Email Gmail của bạn
	password := "mxszuqxfsuuejcez"
	// Mật khẩu của email Gmail của bạn

	to := "ntnnga2509@gmail.com"  // Email người nhận
	subject := "OTP Verification" // Tiêu đề email

	body := fmt.Sprintf("Your OTP is: %s", otp) // Nội dung email

	// Cấu hình kết nối với máy chủ SMTP của Gmail
	auth := smtp.PlainAuth("", from, password, "smtp.gmail.com")

	msg := "From: " + from + "\n" +
		"To: " + to + "\n" +
		"Subject: " + subject + "\n\n" +
		body

	// Gửi email
	err := smtp.SendMail("smtp.gmail.com:587", auth, from, []string{to}, []byte(msg))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	res, err := h.authController.UpdateOTP(otp)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	json.NewEncoder(w).Encode(res)
	// fmt.Println("Email sent successfully!")
}
func (h Handler) CheckOTP(w http.ResponseWriter, r *http.Request) {
	var req OTPInput
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	otpCompare, err := h.authController.GetOTP(req.OTP)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	if req.OTP != otpCompare || req.OTP == "" || req.OTP == "null" {
		http.Error(w, errors.New("otp không đúng").Error(), http.StatusBadRequest)
		return
	}

	json.NewEncoder(w).Encode(otpCompare)

}

func (h Handler) ResetPassword(w http.ResponseWriter, r *http.Request) {
	var req InputUpdatePass
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	res := h.authController.ChangePassword(convertData(req))
	if res != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	_, err = h.authController.UpdateOTPAfterResetPassword()
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	json.NewEncoder(w).Encode(res)
}
