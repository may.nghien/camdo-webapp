package auth

import (
	"Golang/internal/controller/auth"
	"encoding/json"
	"errors"
	"net/http"
)

type InputUpdatePass struct {
	Username    string `json:"username"`
	Password    string `json:"password"`
	NewPassword string `json:"new_password"`
}

func convertData(inp InputUpdatePass) auth.InputUpdatePass {
	return auth.InputUpdatePass{
		Username:    inp.Username,
		Password:    inp.Password,
		NewPassword: inp.NewPassword,
	}
}
func (h Handler) ChangePassword(w http.ResponseWriter, r *http.Request) {
	var req InputUpdatePass
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	user, err := h.authController.GetByUserName(req.Username)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	if user.Password != req.Password {
		http.Error(w, errors.New("mật khẩu cũ không đúng").Error(), http.StatusBadRequest)
		return
	}

	res := h.authController.ChangePassword(convertData(req))
	if res != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	json.NewEncoder(w).Encode(res)
}
