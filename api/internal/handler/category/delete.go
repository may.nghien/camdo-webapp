package category

import (
	"net/http"
	"strconv"

	"github.com/go-chi/chi"
)

func (h Handler) DeleteCategory(w http.ResponseWriter, r *http.Request) {
	idStr := chi.URLParam(r, "id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		http.Error(w, "Invalid orderID", http.StatusBadRequest)
		return
	}

	err = h.categoryController.DeleteCategory(int(id))
	if err != nil {
		http.Error(w, "Lỗi trong quá trình xóa", http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Đã xóa thành công"))
}

func (h Handler) DeleteCategoryGoal(w http.ResponseWriter, r *http.Request) {
	idStr := chi.URLParam(r, "id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		http.Error(w, "Invalid orderID", http.StatusBadRequest)
		return
	}

	err = h.categoryController.DeleteCategoryGoal(int(id))
	if err != nil {
		http.Error(w, "Lỗi trong quá trình xóa", http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Đã xóa thành công"))
}

func (h Handler) DeleteCategoryPercent(w http.ResponseWriter, r *http.Request) {
	idStr := chi.URLParam(r, "id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		http.Error(w, "Invalid orderID", http.StatusBadRequest)
		return
	}

	err = h.categoryController.DeleteCategoryPercent(int(id))
	if err != nil {
		http.Error(w, "Lỗi trong quá trình xóa", http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Đã xóa thành công"))
}
