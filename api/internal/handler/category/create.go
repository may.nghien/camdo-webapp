package category

import (
	"Golang/internal/controller/category"
	"github.com/volatiletech/null/v8"
	"encoding/json"
	"net/http"
)

type CreateCategoryInput struct {
	CategoryName null.String
}

type CreateCategoryOutput struct {
	CategoryName null.String `boil:"category_name" json:"category_name,omitempty" toml:"category_name" yaml:"category_name,omitempty"`
}
type CreateCategoryPercentInput struct {
	Percent null.Float64
}

type CreateCategoryPercentOutput struct {
	Percent   null.Float64 `boil:"percent" json:"percent,omitempty" toml:"percent" yaml:"percent,omitempty"`
}


func validateDataInput(inp CreateCategoryOutput) (category.CategoryInput, error) {
	return category.CategoryInput{ CategoryName: null.String(inp.CategoryName)}, nil
}


func validateDataInputPercent(inp CreateCategoryPercentOutput) (category.CategoryPercentInput, error) {
	return category.CategoryPercentInput{ Percent: null.Float64(inp.Percent)}, nil
}

func (h Handler) CreateCategory(w http.ResponseWriter, r *http.Request) {
	var req CreateCategoryOutput
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	input, err := validateDataInput(req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	res, err := h.categoryController.CreateCategory(input)
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Send token as response
	json.NewEncoder(w).Encode(res)
}

func (h Handler) CreateCategoryGoal(w http.ResponseWriter, r *http.Request) {
	var req CreateCategoryOutput
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	input, err := validateDataInput(req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	res, err := h.categoryController.CreateCategoryGoal(input)
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Send token as response
	json.NewEncoder(w).Encode(res)
}

func (h Handler) CreateCategoryPercent(w http.ResponseWriter, r *http.Request) {
	var req CreateCategoryPercentOutput
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	input, err := validateDataInputPercent(req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	res, err := h.categoryController.CreateCategoryPercent(input)
	if err != nil {
		// handle controller err
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Send token as response
	json.NewEncoder(w).Encode(res)
}