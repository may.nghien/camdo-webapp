package order_goal

import (
	models "Golang/internal/repository/dbmodel"
	"context"
	"time"

	"github.com/volatiletech/sqlboiler/v4/queries/qm"
)

func (rep Repository) GetOverGold() ([]models.OrderGoal, error) {
	overdueOrders, err := models.OrderGoals(
		qm.Where("return_date < ?", time.Now().Format(time.RFC3339)),
		qm.And("status = ?", "0"),
		qm.And("interest_paid_count = ?", 0),
		qm.OrderBy("id DESC"),
	).All(context.Background(), rep.db)
	if err != nil {
		return nil, err
	}
	var orders []models.OrderGoal
	for _, o := range overdueOrders {
		order := models.OrderGoal{
			ID:                o.ID,
			CustomerID:        o.CustomerID,
			Price:             o.Price,
			PriceOld:          o.PriceOld,
			ProductName:       o.ProductName,
			AddPayments:       o.AddPayments,
			Percent:           o.Percent,
			ReturnDate:        o.ReturnDate,
			Status:            o.Status,
			OrderDate:         o.OrderDate,
			Note:              o.Note,
			Miss:              o.Miss,
			InterestPaidCount: o.InterestPaidCount,
		}
		orders = append(orders, order)
	}

	return orders, nil
}

func (rep Repository) GetListOrderHavePaymentOver() ([]models.OrderGoal, error) {
	currentTime := time.Now()

	// Lấy danh sách các đơn hàng có lần thanh toán gần nhất trễ hạn
	orders, err := models.OrderGoals(
		qm.InnerJoin("interest_payments_goal ON interest_payments_goal.order_id = order_goal.id"),
		qm.Where("interest_payments_goal.next_payment_date < ?", currentTime),
		qm.And("status = ?", "0"),
		qm.GroupBy("order_goal.id"),
		qm.Load(models.OrderGoalRels.OrderInterestPaymentsGoals, qm.OrderBy("interest_payments_goal.next_payment_date DESC")),
	).All(context.Background(), rep.db)
	if err != nil {
		return nil, err
	}
	// Lọc danh sách các đơn hàng chỉ lấy đơn hàng có lần thanh toán gần nhất trễ hạn
	overdueOrders := make([]models.OrderGoal, 0)
	for _, order := range orders {
		if order.R.OrderInterestPaymentsGoals != nil && len(order.R.OrderInterestPaymentsGoals) > 0 {
			latestPayment := order.R.OrderInterestPaymentsGoals[0]
			if latestPayment.NextPaymentDate.Valid && latestPayment.NextPaymentDate.Time.Before(currentTime) {
				overdueOrders = append(overdueOrders, *order)
			}
		}
	}

	return overdueOrders, nil
}
func (rep Repository) SearchOrderOverGoalByID(id int64) ([]models.OrderGoal, error) {
	overdueOrders, err := models.OrderGoals(
		qm.Where("return_date < ?", time.Now().Format(time.RFC3339)),
		qm.And("status = ?", "0"),
		qm.And("id = ?", id),
		qm.And("interest_paid_count = ?", 0),
	).All(context.Background(), rep.db)
	if err != nil {
		return nil, err
	}
	var orders []models.OrderGoal
	for _, o := range overdueOrders {
		order := models.OrderGoal{
			ID:                o.ID,
			CustomerID:        o.CustomerID,
			Price:             o.Price,
			PriceOld:          o.PriceOld,
			ProductName:       o.ProductName,
			AddPayments:       o.AddPayments,
			Percent:           o.Percent,
			ReturnDate:        o.ReturnDate,
			Status:            o.Status,
			OrderDate:         o.OrderDate,
			Note:              o.Note,
			Miss:              o.Miss,
			InterestPaidCount: o.InterestPaidCount,
		}
		orders = append(orders, order)
	}

	return orders, nil
}

func (rep Repository) SearchOrderOverPaymentGoalByID(id int64) ([]models.OrderGoal, error) {
	currentTime := time.Now()

	// Lấy danh sách các đơn hàng có lần thanh toán gần nhất trễ hạn
	orders, err := models.OrderGoals(
		qm.InnerJoin("interest_payments_goal ON interest_payments_goal.order_id = order_goal.id"),
		qm.Where("interest_payments_goal.next_payment_date < ?", currentTime),
		qm.And("status = ?", "0"),
		qm.And("order_goal.id = ?", id),
		qm.GroupBy("order_goal.id"),
		qm.Load(models.OrderGoalRels.OrderInterestPaymentsGoals, qm.OrderBy("interest_payments_goal.next_payment_date DESC")),
	).All(context.Background(), rep.db)
	if err != nil {
		return nil, err
	}

	// Lọc danh sách các đơn hàng chỉ lấy đơn hàng có lần thanh toán gần nhất trễ hạn
	overdueOrders := make([]models.OrderGoal, 0)
	for _, order := range orders {
		if order.R.OrderInterestPaymentsGoals != nil && len(order.R.OrderInterestPaymentsGoals) > 0 {
			latestPayment := order.R.OrderInterestPaymentsGoals[0]
			if latestPayment.NextPaymentDate.Valid && latestPayment.NextPaymentDate.Time.Before(currentTime) {
				overdueOrders = append(overdueOrders, *order)
			}
		}
	}

	return overdueOrders, nil
}
