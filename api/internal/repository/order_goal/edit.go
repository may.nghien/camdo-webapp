package order_goal

import (
	models "Golang/internal/repository/dbmodel"
	"context"

	"github.com/volatiletech/null/v8"
	"github.com/volatiletech/sqlboiler/v4/boil"
)

func (rep *Repository) EditOrder(orderID int64, update models.OrderGoal) (models.OrderGoal, error) {
	order, err := models.FindOrderGoal(context.Background(), rep.db, orderID)
	if err != nil {
		return models.OrderGoal{}, err
	}

	// Cập nhật thông tin order
	order.ProductName = update.ProductName
	order.AddPayments = update.AddPayments
	order.Price = update.Price
	order.OrderDate = update.OrderDate
	order.CustomerID = update.CustomerID
	order.Percent = update.Percent
	order.ReturnDate = update.ReturnDate
	order.Status = update.Status
	//order.InterestPaidCount = update.InterestPaidCount

	// Lưu các thay đổi vào cơ sở dữ liệu
	_, err = order.Update(context.Background(), rep.db, boil.Infer())
	if err != nil {
		return models.OrderGoal{}, err
	}

	return *order, nil
}

func (rep Repository) UpdateNote(note null.String,miss null.Int, orderID int64) (models.OrderGoal, error) {
	order, err := models.FindOrderGoal(context.Background(), rep.db, orderID)
	if err != nil {
		return models.OrderGoal{}, err
	}
	order.Note = note
	order.Miss = miss
	// Lưu các thay đổi vào cơ sở dữ liệu
	_, err = order.Update(context.Background(), rep.db, boil.Infer())
	if err != nil {
		return models.OrderGoal{}, err
	}

	return *order, nil
}
