package order_goal

import (
	"github.com/volatiletech/sqlboiler/v4/boil"
)

type Repository struct {
	//db *sql.DB
	db boil.ContextExecutor
}

func New(db boil.ContextExecutor) Repository {
	return Repository{db: db}
}
