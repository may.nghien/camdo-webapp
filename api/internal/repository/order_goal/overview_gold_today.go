package order_goal

import (
	models "Golang/internal/repository/dbmodel"
	"context"
	"time"

	"github.com/volatiletech/sqlboiler/v4/queries/qm"
)

func (rep Repository) GetListPaymentsGoldToday() ([]models.InterestPaymentsGoal, error) {
	var pawnOrders []models.InterestPaymentsGoal
	today := time.Now().UTC()
	startDate := time.Date(today.Year(), today.Month(), today.Day(), 0, 0, 0, 0, time.UTC)
	endDate := startDate.AddDate(0, 0, 1).Add(-time.Nanosecond)

	// Query the database using SQLBoiler's query builder
	err := models.InterestPaymentsGoals(
		qm.Where("payment_date >= ?", startDate),
		qm.Where("payment_date <= ?", endDate),
	).Bind(context.Background(), rep.db, &pawnOrders)
	if err != nil {
		return nil, err
	}

	return pawnOrders, nil
}

func (rep Repository) ListOrderGoldToday() ([]models.OrderGoal, error) {
	var pawnOrders []models.OrderGoal
	today := time.Now().UTC()
	startDate := time.Date(today.Year(), today.Month(), today.Day(), 0, 0, 0, 0, time.UTC)
	endDate := startDate.AddDate(0, 0, 1).Add(-time.Nanosecond)

    // Query the database using SQLBoiler's query builder
    err := models.OrderGoals(
        qm.Where("order_date >= ?", startDate),
        qm.Where("order_date <= ?", endDate),
    ).Bind(context.Background(), rep.db, &pawnOrders)
    if err != nil {
        return nil, err
    }
    
    return pawnOrders, nil
}

func (rep Repository) GetListRedeemGoldToday() ([]models.RedeemOrderGoal, error) {
	var pawnOrders []models.RedeemOrderGoal
	today := time.Now().UTC()
	startDate := time.Date(today.Year(), today.Month(), today.Day(), 0, 0, 0, 0, time.UTC)
	endDate := startDate.AddDate(0, 0, 1).Add(-time.Nanosecond)

	// Query the database using SQLBoiler's query builder
	err := models.RedeemOrderGoals(
		qm.Where("date_redeem >= ?", startDate),
		qm.Where("date_redeem <= ?", endDate),
	).Bind(context.Background(), rep.db, &pawnOrders)
	if err != nil {
		return nil, err
	}

	return pawnOrders, nil
}

func (rep Repository) GetListAddPaymentGoldToday() ([]models.AddPaymentsGoal, error) {
	var pawnOrders []models.AddPaymentsGoal
	today := time.Now().UTC()
	startDate := time.Date(today.Year(), today.Month(), today.Day(), 0, 0, 0, 0, time.UTC)
	endDate := startDate.AddDate(0, 0, 1).Add(-time.Nanosecond)

	// Query the database using SQLBoiler's query builder
	err := models.AddPaymentsGoals(
		qm.Where("date_add_payment >= ?", startDate),
		qm.Where("date_add_payment <= ?", endDate),
	).Bind(context.Background(), rep.db, &pawnOrders)
	if err != nil {
		return nil, err
	}

	return pawnOrders, nil
}