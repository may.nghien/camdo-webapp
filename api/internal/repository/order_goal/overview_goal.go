package order_goal

import (
	models "Golang/internal/repository/dbmodel"
	"context"
	"time"

	"github.com/volatiletech/sqlboiler/v4/queries/qm"
)

func (rep Repository) GetSpendGoal(month int, year int) (int64, error) {
	amount, err := models.SpendGoals(qm.Where("month = ?", month),
		qm.And("year = ?", year)).One(context.Background(), rep.db)
	if err != nil {
		return 0, err
	}
	return amount.Amount.Int64, nil
}

func (rep Repository) GetRevenueGoal(month int, year int) (int64, error) {
	amount, err := models.RevenueGoals(qm.Where("month = ?", month),
		qm.And("year = ?", year)).One(context.Background(), rep.db)
	if err != nil {
		return 0, err
	}
	return amount.Amount.Int64, nil
}

func (rep Repository) GetCollectGoal(month int, year int) (int64, error) {
	amount, err := models.CollectGoals(qm.Where("month = ?", month),
		qm.And("year = ?", year)).One(context.Background(), rep.db)
	if err != nil {
		return 0, err
	}
	return amount.Amount.Int64, nil
}

func (rep Repository) GetListPaymentsGoalByMonthAndYear(month int, year int) ([]models.InterestPaymentsGoal, error) {
	var pawnOrders []models.InterestPaymentsGoal
	startDate := time.Date(year, time.Month(month), 1, 0, 0, 0, 0, time.UTC)
	endDate := startDate.AddDate(0, 1, 0).Add(-time.Nanosecond)

	// Query the database using SQLBoiler's query builder
	err := models.InterestPaymentsGoals(
		qm.Where("payment_date >= ?", startDate),
		qm.Where("payment_date <= ?", endDate),
	).Bind(context.Background(), rep.db, &pawnOrders)
	if err != nil {
		return nil, err
	}

	return pawnOrders, nil
}

func (rep Repository) ListOrderGoalByMonthAndYear(month int, year int) ([]models.OrderGoal, error) {
	var pawnOrders []models.OrderGoal
    startDate := time.Date(year, time.Month(month), 1, 0, 0, 0, 0, time.UTC)
    endDate := startDate.AddDate(0, 1, 0).Add(-time.Nanosecond)

    // Query the database using SQLBoiler's query builder
    err := models.OrderGoals(
        qm.Where("order_date >= ?", startDate),
        qm.Where("order_date <= ?", endDate),
    ).Bind(context.Background(), rep.db, &pawnOrders)
    if err != nil {
        return nil, err
    }
	if err != nil {
        return nil, err
    }
    
    return pawnOrders, nil
}
func (rep Repository) ListAddPaymentByMonthAndYear(month int, year int) ([]models.AddPaymentsGoal, error) {
	var pawnOrders []models.AddPaymentsGoal
    startDate := time.Date(year, time.Month(month), 1, 0, 0, 0, 0, time.UTC)
    endDate := startDate.AddDate(0, 1, 0).Add(-time.Nanosecond)

    // Query the database using SQLBoiler's query builder
    err := models.AddPaymentsGoals(
        qm.Where("date_add_payment >= ?", startDate),
        qm.Where("date_add_payment <= ?", endDate),
    ).Bind(context.Background(), rep.db, &pawnOrders)
    if err != nil {
        return nil, err
    }
    
    return pawnOrders, nil
}

func (rep Repository) GetListRedeemGoalByMonthAndYear(month int, year int) ([]models.RedeemOrderGoal, error) {
	var pawnOrders []models.RedeemOrderGoal
	startDate := time.Date(year, time.Month(month), 1, 0, 0, 0, 0, time.UTC)
	endDate := startDate.AddDate(0, 1, 0).Add(-time.Nanosecond)

	// Query the database using SQLBoiler's query builder
	err := models.RedeemOrderGoals(
		qm.Where("date_redeem >= ?", startDate),
		qm.Where("date_redeem <= ?", endDate),
	).Bind(context.Background(), rep.db, &pawnOrders)
	if err != nil {
		return nil, err
	}

	return pawnOrders, nil
}