package order_goal

import (
	models "Golang/internal/repository/dbmodel"
	"context"

	// "math/rand"
	// "strconv"
	// "time"

	"github.com/volatiletech/null/v8"
	"github.com/volatiletech/sqlboiler/v4/boil"
)

func (rep Repository) CreateOrder(m models.OrderGoal) (models.OrderGoal, error) {
	user := &models.OrderGoal{
		ProductName:       m.ProductName,
		AddPayments:    m.AddPayments,
		PriceOld:          m.Price,
		Price:             m.Price,
		OrderDate:         m.OrderDate,
		CustomerID:        m.CustomerID,
		Percent:           m.Percent,
		ReturnDate:        m.ReturnDate,
		Status:            m.Status,
		InterestPaidCount: m.InterestPaidCount,
		Note:              m.Note,
	}

	if err := user.Insert(context.Background(), rep.db, boil.Infer()); err != nil {
		return models.OrderGoal{}, err
	}

	// Lấy ID của đơn hàng đã tạo
	orderID := user.ID

	return models.OrderGoal{
		ID:                orderID, // Trả về ID của đơn hàng
		ProductName:       user.ProductName,
		AddPayments:    user.AddPayments,
		PriceOld:          user.PriceOld,
		Price:             user.Price,
		OrderDate:         user.OrderDate,
		CustomerID:        user.CustomerID,
		Percent:           user.Percent,
		ReturnDate:        user.ReturnDate,
		Status:            user.Status,
		InterestPaidCount: user.InterestPaidCount,
	}, nil
}

func (rep Repository) CreateProduct(id int64, m models.ProductGold) (models.ProductGold, error) {
	product := &models.ProductGold{
		IDGoal:         null.Int64From(id),
		CategoryGoalID: m.CategoryGoalID,
		WeightGoal:     m.WeightGoal,
	}

	if err := product.Insert(context.Background(), rep.db, boil.Infer()); err != nil {
		return models.ProductGold{}, err
	}
	IDProduct := product.ID
	return models.ProductGold{
		ID:             IDProduct,
		IDGoal:         product.IDGoal,
		CategoryGoalID: product.CategoryGoalID,
		WeightGoal:     product.WeightGoal,
	}, nil
}

func (rep Repository) CreateProductDetail(id int64, m models.DetailProductGold) (models.DetailProductGold, error) {
	product := &models.DetailProductGold{
		DetailProductID: null.Int64From(id),
		CategoryID:      m.CategoryID,
		Count:           m.Count,
	}

	if err := product.Insert(context.Background(), rep.db, boil.Infer()); err != nil {
		return models.DetailProductGold{}, err
	}
	IDProduct := product.ID
	return models.DetailProductGold{
		ID:              IDProduct,
		DetailProductID: product.DetailProductID,
		CategoryID:      product.CategoryID,
	}, nil
}
