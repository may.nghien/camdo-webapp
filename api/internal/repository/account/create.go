package account

import (
	models "Golang/internal/repository/dbmodel"
	"context"
	"fmt"
	"math/rand"
	"strconv"
	"time"

	"github.com/volatiletech/sqlboiler/v4/boil"
	"github.com/volatiletech/sqlboiler/v4/queries/qm"
)

func (rep Repository) CreateAccount(m models.Account) (models.Account, error) {
	// Tạo số nguyên ngẫu nhiên từ 0 đến 999999999
	rand.Seed(time.Now().UnixNano())
	randomInt := rand.Intn(100000)

	// Chuyển đổi số nguyên thành chuỗi và thêm thời gian hiện tại
	// dưới dạng UnixNano để đảm bảo tính duy nhất của ID
	id := strconv.Itoa(randomInt) + "-" + strconv.FormatInt(time.Now().UnixNano(), 10)
	user := &models.Account{
		ID:       id,
		Name:     m.Name,
		Username: m.Username,
		Gender:   m.Gender,
		Avatar:   m.Avatar,
		Password: m.Password,
	}
	if err := user.Insert(context.Background(), rep.db, boil.Infer()); err != nil {
		return models.Account{}, err
	}

	return m,nil
}


func (rep Repository) CheckUsername(m models.Account) (models.Account, error) {
	// Check if email already exists
	existingUser, err := models.Accounts(qm.Where("username = ?", m.Username)).One(context.Background(), rep.db)
	if err == nil {
		return models.Account{}, err
	}
	fmt.Println(existingUser)
	return m, nil
}