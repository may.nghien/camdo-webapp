package account

import (
	"Golang/internal/repository/dbmodel"
	"Golang/pkg/db"
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
	"strconv"
	"time"

	"github.com/volatiletech/sqlboiler/v4/boil"
)

func CreateAccount(w http.ResponseWriter, r *http.Request) {
	var account models.Account
	err := json.NewDecoder(r.Body).Decode(&account)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	// Tạo số nguyên ngẫu nhiên từ 0 đến 999999999
    rand.Seed(time.Now().UnixNano())
    randomInt := rand.Intn(100000)

    // Chuyển đổi số nguyên thành chuỗi và thêm thời gian hiện tại
    // dưới dạng UnixNano để đảm bảo tính duy nhất của ID
    id := strconv.Itoa(randomInt) + "-" + strconv.FormatInt(time.Now().UnixNano(), 10)

	 // Create new user
	 user := &models.Account{
		ID: id,
		Name:account.Name,
		Username:account.Username,
		Gender:  account.Gender ,
		Avatar:  account.Avatar,
		Password: account.Password,
    }
    if err := user.Insert(r.Context(),db.Database.Db, boil.Infer()); err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
        return
    }
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
	w.Header().Set("Content-Type", "application/json")
    // Return success response
    w.WriteHeader(http.StatusCreated)
    fmt.Fprint(w, "User created successfully")
}