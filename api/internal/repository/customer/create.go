package customer

import (
	models "Golang/internal/repository/dbmodel"
	"github.com/volatiletech/sqlboiler/v4/boil"
	"context"
	"fmt"
	"hash/fnv"
	"strconv"
	"time"
)

func generateID() string {
	// Tạo một chuỗi dữ liệu duy nhất từ thời gian hiện tại
	data := strconv.FormatInt(time.Now().UnixNano(), 10)

	// Tạo một đối tượng hash FNV
	h := fnv.New32a()

	// Đưa chuỗi dữ liệu vào đối tượng hash
	h.Write([]byte(data))

	// Lấy giá trị băm dưới dạng uint32
	hashValue := h.Sum32()

	// Chuyển đổi giá trị băm thành chuỗi và trả về
	return strconv.FormatUint(uint64(hashValue), 10)
}
func (rep Repository) CreateCustomer(m models.Customer) (models.Customer, error) {
	// Tạo số nguyên ngẫu nhiên từ 0 đến 999999999
	// rand.Seed(time.Now().UnixNano())
	// randomInt := rand.Intn(1000)

	// // Chuyển đổi số nguyên thành chuỗi và thêm thời gian hiện tại
	// // dưới dạng UnixNano để đảm bảo tính duy nhất của ID
	// id := strconv.Itoa(randomInt) + strconv.FormatInt(time.Now().UnixNano(), 10)
	idStr := generateID()
	id, err := strconv.Atoi(idStr)
	if err != nil {
		// Xử lý lỗi nếu có
		fmt.Println("Lỗi chuyển đổi chuỗi thành số nguyên:", err)
	}

	user := &models.Customer{
		CustomerID:   int64(id),
		CustomerName: m.CustomerName,
		Address:      m.Address,
		Gender:       m.Gender,
		CCCD:         m.CCCD,
		BirthDate:    m.BirthDate,
		Phone:        m.Phone,
		AddressCCCD: m.AddressCCCD,
		DayCCCD:	m.DayCCCD,
	}
	if err := user.Insert(context.Background(), rep.db, boil.Infer()); err != nil {
		return models.Customer{}, err
	}

	return m, nil
}
