package customer

import (
	models "Golang/internal/repository/dbmodel"
	"context"

	//"github.com/volatiletech/sqlboiler/v4/boil"

	"github.com/volatiletech/null/v8"
	"github.com/volatiletech/sqlboiler/v4/queries/qm"
)

func (rep Repository) SearchCustomerByName(inp string) ([]models.Customer, error) {
	//boil.DebugMode = true
	users, err := models.Customers(qm.Where("customer_name ILIKE ?", "%"+inp+"%")).All(context.Background(), rep.db)

	if err != nil {
		return nil, err
	}

	var result = make([]models.Customer, len(users))
	for ind, customer := range users {
		result[ind] = models.Customer{CustomerID: int64(customer.CustomerID),
			CustomerName: customer.CustomerName,
			Address:      null.String(customer.Address),
			Gender:       null.String(customer.Gender),
			CCCD:         null.String(customer.CCCD),
			BirthDate:    null.Time(customer.BirthDate),
			Phone:        null.String(customer.Phone),
			AddressCCCD:  null.String(customer.AddressCCCD),
			DayCCCD:      null.Time(customer.DayCCCD),
		}
	}

	return result, nil
}

func (rep Repository) SearchCustomerByPhone(inp string) ([]models.Customer, error) {
	//boil.DebugMode = true
	users, err := models.Customers(qm.Where("phone ILIKE ?", "%"+inp+"%")).All(context.Background(), rep.db)

	if err != nil {
		return nil, err
	}

	var result = make([]models.Customer, len(users))
	for ind, customer := range users {
		result[ind] = models.Customer{CustomerID: int64(customer.CustomerID),
			CustomerName: customer.CustomerName,
			Address:      null.String(customer.Address),
			Gender:       null.String(customer.Gender),
			CCCD:         null.String(customer.CCCD),
			BirthDate:    null.Time(customer.BirthDate),
			Phone:        null.String(customer.Phone),
			AddressCCCD:  null.String(customer.AddressCCCD),
			DayCCCD:      null.Time(customer.DayCCCD),
		}
	}

	return result, nil
}

func (rep Repository) SearchCustomerByCCCD(inp string) ([]models.Customer, error) {
	//boil.DebugMode = true
	users, err := models.Customers(qm.Where("cccd ILIKE ?", "%"+inp+"%")).All(context.Background(), rep.db)

	if err != nil {
		return nil, err
	}

	var result = make([]models.Customer, len(users))
	for ind, customer := range users {
		result[ind] = models.Customer{CustomerID: int64(customer.CustomerID),
			CustomerName: customer.CustomerName,
			Address:      null.String(customer.Address),
			Gender:       null.String(customer.Gender),
			CCCD:         null.String(customer.CCCD),
			BirthDate:    null.Time(customer.BirthDate),
			Phone:        null.String(customer.Phone),
			AddressCCCD:  null.String(customer.AddressCCCD),
			DayCCCD:      null.Time(customer.DayCCCD),
		}
	}

	return result, nil
}
