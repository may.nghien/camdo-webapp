package customer

import (
	models "Golang/internal/repository/dbmodel"
	"context"

	"github.com/volatiletech/sqlboiler/v4/queries/qm"
)

func (rep Repository) DeleteCustomer(id int64) error {
	_, err := models.Customers(qm.Where("customer_id=?", id)).DeleteAll(context.Background(), rep.db)
	return err
}
