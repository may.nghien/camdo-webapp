// Code generated by SQLBoiler 4.14.2 (https://github.com/volatiletech/sqlboiler). DO NOT EDIT.
// This file is meant to be re-generated in place and/or deleted at any time.

package models

import (
	"context"
	"database/sql"
	"fmt"
	"reflect"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/friendsofgo/errors"
	"github.com/volatiletech/null/v8"
	"github.com/volatiletech/sqlboiler/v4/boil"
	"github.com/volatiletech/sqlboiler/v4/queries"
	"github.com/volatiletech/sqlboiler/v4/queries/qm"
	"github.com/volatiletech/sqlboiler/v4/queries/qmhelper"
	"github.com/volatiletech/strmangle"
)

// RedeemOrderVehicle is an object representing the database table.
type RedeemOrderVehicle struct {
	ID         int64      `boil:"id" json:"id" toml:"id" yaml:"id"`
	Amount     null.Int   `boil:"amount" json:"amount,omitempty" toml:"amount" yaml:"amount,omitempty"`
	DateRedeem null.Time  `boil:"date_redeem" json:"date_redeem,omitempty" toml:"date_redeem" yaml:"date_redeem,omitempty"`
	OrderID    null.Int64 `boil:"order_id" json:"order_id,omitempty" toml:"order_id" yaml:"order_id,omitempty"`
	CreatedAt  time.Time  `boil:"created_at" json:"created_at" toml:"created_at" yaml:"created_at"`
	UpdatedAt  time.Time  `boil:"updated_at" json:"updated_at" toml:"updated_at" yaml:"updated_at"`

	R *redeemOrderVehicleR `boil:"-" json:"-" toml:"-" yaml:"-"`
	L redeemOrderVehicleL  `boil:"-" json:"-" toml:"-" yaml:"-"`
}

var RedeemOrderVehicleColumns = struct {
	ID         string
	Amount     string
	DateRedeem string
	OrderID    string
	CreatedAt  string
	UpdatedAt  string
}{
	ID:         "id",
	Amount:     "amount",
	DateRedeem: "date_redeem",
	OrderID:    "order_id",
	CreatedAt:  "created_at",
	UpdatedAt:  "updated_at",
}

var RedeemOrderVehicleTableColumns = struct {
	ID         string
	Amount     string
	DateRedeem string
	OrderID    string
	CreatedAt  string
	UpdatedAt  string
}{
	ID:         "redeem_order_vehicle.id",
	Amount:     "redeem_order_vehicle.amount",
	DateRedeem: "redeem_order_vehicle.date_redeem",
	OrderID:    "redeem_order_vehicle.order_id",
	CreatedAt:  "redeem_order_vehicle.created_at",
	UpdatedAt:  "redeem_order_vehicle.updated_at",
}

// Generated where

var RedeemOrderVehicleWhere = struct {
	ID         whereHelperint64
	Amount     whereHelpernull_Int
	DateRedeem whereHelpernull_Time
	OrderID    whereHelpernull_Int64
	CreatedAt  whereHelpertime_Time
	UpdatedAt  whereHelpertime_Time
}{
	ID:         whereHelperint64{field: "\"redeem_order_vehicle\".\"id\""},
	Amount:     whereHelpernull_Int{field: "\"redeem_order_vehicle\".\"amount\""},
	DateRedeem: whereHelpernull_Time{field: "\"redeem_order_vehicle\".\"date_redeem\""},
	OrderID:    whereHelpernull_Int64{field: "\"redeem_order_vehicle\".\"order_id\""},
	CreatedAt:  whereHelpertime_Time{field: "\"redeem_order_vehicle\".\"created_at\""},
	UpdatedAt:  whereHelpertime_Time{field: "\"redeem_order_vehicle\".\"updated_at\""},
}

// RedeemOrderVehicleRels is where relationship names are stored.
var RedeemOrderVehicleRels = struct {
	Order string
}{
	Order: "Order",
}

// redeemOrderVehicleR is where relationships are stored.
type redeemOrderVehicleR struct {
	Order *OrderVehicle `boil:"Order" json:"Order" toml:"Order" yaml:"Order"`
}

// NewStruct creates a new relationship struct
func (*redeemOrderVehicleR) NewStruct() *redeemOrderVehicleR {
	return &redeemOrderVehicleR{}
}

func (r *redeemOrderVehicleR) GetOrder() *OrderVehicle {
	if r == nil {
		return nil
	}
	return r.Order
}

// redeemOrderVehicleL is where Load methods for each relationship are stored.
type redeemOrderVehicleL struct{}

var (
	redeemOrderVehicleAllColumns            = []string{"id", "amount", "date_redeem", "order_id", "created_at", "updated_at"}
	redeemOrderVehicleColumnsWithoutDefault = []string{}
	redeemOrderVehicleColumnsWithDefault    = []string{"id", "amount", "date_redeem", "order_id", "created_at", "updated_at"}
	redeemOrderVehiclePrimaryKeyColumns     = []string{"id"}
	redeemOrderVehicleGeneratedColumns      = []string{}
)

type (
	// RedeemOrderVehicleSlice is an alias for a slice of pointers to RedeemOrderVehicle.
	// This should almost always be used instead of []RedeemOrderVehicle.
	RedeemOrderVehicleSlice []*RedeemOrderVehicle
	// RedeemOrderVehicleHook is the signature for custom RedeemOrderVehicle hook methods
	RedeemOrderVehicleHook func(context.Context, boil.ContextExecutor, *RedeemOrderVehicle) error

	redeemOrderVehicleQuery struct {
		*queries.Query
	}
)

// Cache for insert, update and upsert
var (
	redeemOrderVehicleType                 = reflect.TypeOf(&RedeemOrderVehicle{})
	redeemOrderVehicleMapping              = queries.MakeStructMapping(redeemOrderVehicleType)
	redeemOrderVehiclePrimaryKeyMapping, _ = queries.BindMapping(redeemOrderVehicleType, redeemOrderVehicleMapping, redeemOrderVehiclePrimaryKeyColumns)
	redeemOrderVehicleInsertCacheMut       sync.RWMutex
	redeemOrderVehicleInsertCache          = make(map[string]insertCache)
	redeemOrderVehicleUpdateCacheMut       sync.RWMutex
	redeemOrderVehicleUpdateCache          = make(map[string]updateCache)
	redeemOrderVehicleUpsertCacheMut       sync.RWMutex
	redeemOrderVehicleUpsertCache          = make(map[string]insertCache)
)

var (
	// Force time package dependency for automated UpdatedAt/CreatedAt.
	_ = time.Second
	// Force qmhelper dependency for where clause generation (which doesn't
	// always happen)
	_ = qmhelper.Where
)

var redeemOrderVehicleAfterSelectHooks []RedeemOrderVehicleHook

var redeemOrderVehicleBeforeInsertHooks []RedeemOrderVehicleHook
var redeemOrderVehicleAfterInsertHooks []RedeemOrderVehicleHook

var redeemOrderVehicleBeforeUpdateHooks []RedeemOrderVehicleHook
var redeemOrderVehicleAfterUpdateHooks []RedeemOrderVehicleHook

var redeemOrderVehicleBeforeDeleteHooks []RedeemOrderVehicleHook
var redeemOrderVehicleAfterDeleteHooks []RedeemOrderVehicleHook

var redeemOrderVehicleBeforeUpsertHooks []RedeemOrderVehicleHook
var redeemOrderVehicleAfterUpsertHooks []RedeemOrderVehicleHook

// doAfterSelectHooks executes all "after Select" hooks.
func (o *RedeemOrderVehicle) doAfterSelectHooks(ctx context.Context, exec boil.ContextExecutor) (err error) {
	if boil.HooksAreSkipped(ctx) {
		return nil
	}

	for _, hook := range redeemOrderVehicleAfterSelectHooks {
		if err := hook(ctx, exec, o); err != nil {
			return err
		}
	}

	return nil
}

// doBeforeInsertHooks executes all "before insert" hooks.
func (o *RedeemOrderVehicle) doBeforeInsertHooks(ctx context.Context, exec boil.ContextExecutor) (err error) {
	if boil.HooksAreSkipped(ctx) {
		return nil
	}

	for _, hook := range redeemOrderVehicleBeforeInsertHooks {
		if err := hook(ctx, exec, o); err != nil {
			return err
		}
	}

	return nil
}

// doAfterInsertHooks executes all "after Insert" hooks.
func (o *RedeemOrderVehicle) doAfterInsertHooks(ctx context.Context, exec boil.ContextExecutor) (err error) {
	if boil.HooksAreSkipped(ctx) {
		return nil
	}

	for _, hook := range redeemOrderVehicleAfterInsertHooks {
		if err := hook(ctx, exec, o); err != nil {
			return err
		}
	}

	return nil
}

// doBeforeUpdateHooks executes all "before Update" hooks.
func (o *RedeemOrderVehicle) doBeforeUpdateHooks(ctx context.Context, exec boil.ContextExecutor) (err error) {
	if boil.HooksAreSkipped(ctx) {
		return nil
	}

	for _, hook := range redeemOrderVehicleBeforeUpdateHooks {
		if err := hook(ctx, exec, o); err != nil {
			return err
		}
	}

	return nil
}

// doAfterUpdateHooks executes all "after Update" hooks.
func (o *RedeemOrderVehicle) doAfterUpdateHooks(ctx context.Context, exec boil.ContextExecutor) (err error) {
	if boil.HooksAreSkipped(ctx) {
		return nil
	}

	for _, hook := range redeemOrderVehicleAfterUpdateHooks {
		if err := hook(ctx, exec, o); err != nil {
			return err
		}
	}

	return nil
}

// doBeforeDeleteHooks executes all "before Delete" hooks.
func (o *RedeemOrderVehicle) doBeforeDeleteHooks(ctx context.Context, exec boil.ContextExecutor) (err error) {
	if boil.HooksAreSkipped(ctx) {
		return nil
	}

	for _, hook := range redeemOrderVehicleBeforeDeleteHooks {
		if err := hook(ctx, exec, o); err != nil {
			return err
		}
	}

	return nil
}

// doAfterDeleteHooks executes all "after Delete" hooks.
func (o *RedeemOrderVehicle) doAfterDeleteHooks(ctx context.Context, exec boil.ContextExecutor) (err error) {
	if boil.HooksAreSkipped(ctx) {
		return nil
	}

	for _, hook := range redeemOrderVehicleAfterDeleteHooks {
		if err := hook(ctx, exec, o); err != nil {
			return err
		}
	}

	return nil
}

// doBeforeUpsertHooks executes all "before Upsert" hooks.
func (o *RedeemOrderVehicle) doBeforeUpsertHooks(ctx context.Context, exec boil.ContextExecutor) (err error) {
	if boil.HooksAreSkipped(ctx) {
		return nil
	}

	for _, hook := range redeemOrderVehicleBeforeUpsertHooks {
		if err := hook(ctx, exec, o); err != nil {
			return err
		}
	}

	return nil
}

// doAfterUpsertHooks executes all "after Upsert" hooks.
func (o *RedeemOrderVehicle) doAfterUpsertHooks(ctx context.Context, exec boil.ContextExecutor) (err error) {
	if boil.HooksAreSkipped(ctx) {
		return nil
	}

	for _, hook := range redeemOrderVehicleAfterUpsertHooks {
		if err := hook(ctx, exec, o); err != nil {
			return err
		}
	}

	return nil
}

// AddRedeemOrderVehicleHook registers your hook function for all future operations.
func AddRedeemOrderVehicleHook(hookPoint boil.HookPoint, redeemOrderVehicleHook RedeemOrderVehicleHook) {
	switch hookPoint {
	case boil.AfterSelectHook:
		redeemOrderVehicleAfterSelectHooks = append(redeemOrderVehicleAfterSelectHooks, redeemOrderVehicleHook)
	case boil.BeforeInsertHook:
		redeemOrderVehicleBeforeInsertHooks = append(redeemOrderVehicleBeforeInsertHooks, redeemOrderVehicleHook)
	case boil.AfterInsertHook:
		redeemOrderVehicleAfterInsertHooks = append(redeemOrderVehicleAfterInsertHooks, redeemOrderVehicleHook)
	case boil.BeforeUpdateHook:
		redeemOrderVehicleBeforeUpdateHooks = append(redeemOrderVehicleBeforeUpdateHooks, redeemOrderVehicleHook)
	case boil.AfterUpdateHook:
		redeemOrderVehicleAfterUpdateHooks = append(redeemOrderVehicleAfterUpdateHooks, redeemOrderVehicleHook)
	case boil.BeforeDeleteHook:
		redeemOrderVehicleBeforeDeleteHooks = append(redeemOrderVehicleBeforeDeleteHooks, redeemOrderVehicleHook)
	case boil.AfterDeleteHook:
		redeemOrderVehicleAfterDeleteHooks = append(redeemOrderVehicleAfterDeleteHooks, redeemOrderVehicleHook)
	case boil.BeforeUpsertHook:
		redeemOrderVehicleBeforeUpsertHooks = append(redeemOrderVehicleBeforeUpsertHooks, redeemOrderVehicleHook)
	case boil.AfterUpsertHook:
		redeemOrderVehicleAfterUpsertHooks = append(redeemOrderVehicleAfterUpsertHooks, redeemOrderVehicleHook)
	}
}

// One returns a single redeemOrderVehicle record from the query.
func (q redeemOrderVehicleQuery) One(ctx context.Context, exec boil.ContextExecutor) (*RedeemOrderVehicle, error) {
	o := &RedeemOrderVehicle{}

	queries.SetLimit(q.Query, 1)

	err := q.Bind(ctx, exec, o)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, sql.ErrNoRows
		}
		return nil, errors.Wrap(err, "models: failed to execute a one query for redeem_order_vehicle")
	}

	if err := o.doAfterSelectHooks(ctx, exec); err != nil {
		return o, err
	}

	return o, nil
}

// All returns all RedeemOrderVehicle records from the query.
func (q redeemOrderVehicleQuery) All(ctx context.Context, exec boil.ContextExecutor) (RedeemOrderVehicleSlice, error) {
	var o []*RedeemOrderVehicle

	err := q.Bind(ctx, exec, &o)
	if err != nil {
		return nil, errors.Wrap(err, "models: failed to assign all query results to RedeemOrderVehicle slice")
	}

	if len(redeemOrderVehicleAfterSelectHooks) != 0 {
		for _, obj := range o {
			if err := obj.doAfterSelectHooks(ctx, exec); err != nil {
				return o, err
			}
		}
	}

	return o, nil
}

// Count returns the count of all RedeemOrderVehicle records in the query.
func (q redeemOrderVehicleQuery) Count(ctx context.Context, exec boil.ContextExecutor) (int64, error) {
	var count int64

	queries.SetSelect(q.Query, nil)
	queries.SetCount(q.Query)

	err := q.Query.QueryRowContext(ctx, exec).Scan(&count)
	if err != nil {
		return 0, errors.Wrap(err, "models: failed to count redeem_order_vehicle rows")
	}

	return count, nil
}

// Exists checks if the row exists in the table.
func (q redeemOrderVehicleQuery) Exists(ctx context.Context, exec boil.ContextExecutor) (bool, error) {
	var count int64

	queries.SetSelect(q.Query, nil)
	queries.SetCount(q.Query)
	queries.SetLimit(q.Query, 1)

	err := q.Query.QueryRowContext(ctx, exec).Scan(&count)
	if err != nil {
		return false, errors.Wrap(err, "models: failed to check if redeem_order_vehicle exists")
	}

	return count > 0, nil
}

// Order pointed to by the foreign key.
func (o *RedeemOrderVehicle) Order(mods ...qm.QueryMod) orderVehicleQuery {
	queryMods := []qm.QueryMod{
		qm.Where("\"id\" = ?", o.OrderID),
	}

	queryMods = append(queryMods, mods...)

	return OrderVehicles(queryMods...)
}

// LoadOrder allows an eager lookup of values, cached into the
// loaded structs of the objects. This is for an N-1 relationship.
func (redeemOrderVehicleL) LoadOrder(ctx context.Context, e boil.ContextExecutor, singular bool, maybeRedeemOrderVehicle interface{}, mods queries.Applicator) error {
	var slice []*RedeemOrderVehicle
	var object *RedeemOrderVehicle

	if singular {
		var ok bool
		object, ok = maybeRedeemOrderVehicle.(*RedeemOrderVehicle)
		if !ok {
			object = new(RedeemOrderVehicle)
			ok = queries.SetFromEmbeddedStruct(&object, &maybeRedeemOrderVehicle)
			if !ok {
				return errors.New(fmt.Sprintf("failed to set %T from embedded struct %T", object, maybeRedeemOrderVehicle))
			}
		}
	} else {
		s, ok := maybeRedeemOrderVehicle.(*[]*RedeemOrderVehicle)
		if ok {
			slice = *s
		} else {
			ok = queries.SetFromEmbeddedStruct(&slice, maybeRedeemOrderVehicle)
			if !ok {
				return errors.New(fmt.Sprintf("failed to set %T from embedded struct %T", slice, maybeRedeemOrderVehicle))
			}
		}
	}

	args := make([]interface{}, 0, 1)
	if singular {
		if object.R == nil {
			object.R = &redeemOrderVehicleR{}
		}
		if !queries.IsNil(object.OrderID) {
			args = append(args, object.OrderID)
		}

	} else {
	Outer:
		for _, obj := range slice {
			if obj.R == nil {
				obj.R = &redeemOrderVehicleR{}
			}

			for _, a := range args {
				if queries.Equal(a, obj.OrderID) {
					continue Outer
				}
			}

			if !queries.IsNil(obj.OrderID) {
				args = append(args, obj.OrderID)
			}

		}
	}

	if len(args) == 0 {
		return nil
	}

	query := NewQuery(
		qm.From(`order_vehicle`),
		qm.WhereIn(`order_vehicle.id in ?`, args...),
	)
	if mods != nil {
		mods.Apply(query)
	}

	results, err := query.QueryContext(ctx, e)
	if err != nil {
		return errors.Wrap(err, "failed to eager load OrderVehicle")
	}

	var resultSlice []*OrderVehicle
	if err = queries.Bind(results, &resultSlice); err != nil {
		return errors.Wrap(err, "failed to bind eager loaded slice OrderVehicle")
	}

	if err = results.Close(); err != nil {
		return errors.Wrap(err, "failed to close results of eager load for order_vehicle")
	}
	if err = results.Err(); err != nil {
		return errors.Wrap(err, "error occurred during iteration of eager loaded relations for order_vehicle")
	}

	if len(orderVehicleAfterSelectHooks) != 0 {
		for _, obj := range resultSlice {
			if err := obj.doAfterSelectHooks(ctx, e); err != nil {
				return err
			}
		}
	}

	if len(resultSlice) == 0 {
		return nil
	}

	if singular {
		foreign := resultSlice[0]
		object.R.Order = foreign
		if foreign.R == nil {
			foreign.R = &orderVehicleR{}
		}
		foreign.R.OrderRedeemOrderVehicles = append(foreign.R.OrderRedeemOrderVehicles, object)
		return nil
	}

	for _, local := range slice {
		for _, foreign := range resultSlice {
			if queries.Equal(local.OrderID, foreign.ID) {
				local.R.Order = foreign
				if foreign.R == nil {
					foreign.R = &orderVehicleR{}
				}
				foreign.R.OrderRedeemOrderVehicles = append(foreign.R.OrderRedeemOrderVehicles, local)
				break
			}
		}
	}

	return nil
}

// SetOrder of the redeemOrderVehicle to the related item.
// Sets o.R.Order to related.
// Adds o to related.R.OrderRedeemOrderVehicles.
func (o *RedeemOrderVehicle) SetOrder(ctx context.Context, exec boil.ContextExecutor, insert bool, related *OrderVehicle) error {
	var err error
	if insert {
		if err = related.Insert(ctx, exec, boil.Infer()); err != nil {
			return errors.Wrap(err, "failed to insert into foreign table")
		}
	}

	updateQuery := fmt.Sprintf(
		"UPDATE \"redeem_order_vehicle\" SET %s WHERE %s",
		strmangle.SetParamNames("\"", "\"", 1, []string{"order_id"}),
		strmangle.WhereClause("\"", "\"", 2, redeemOrderVehiclePrimaryKeyColumns),
	)
	values := []interface{}{related.ID, o.ID}

	if boil.IsDebug(ctx) {
		writer := boil.DebugWriterFrom(ctx)
		fmt.Fprintln(writer, updateQuery)
		fmt.Fprintln(writer, values)
	}
	if _, err = exec.ExecContext(ctx, updateQuery, values...); err != nil {
		return errors.Wrap(err, "failed to update local table")
	}

	queries.Assign(&o.OrderID, related.ID)
	if o.R == nil {
		o.R = &redeemOrderVehicleR{
			Order: related,
		}
	} else {
		o.R.Order = related
	}

	if related.R == nil {
		related.R = &orderVehicleR{
			OrderRedeemOrderVehicles: RedeemOrderVehicleSlice{o},
		}
	} else {
		related.R.OrderRedeemOrderVehicles = append(related.R.OrderRedeemOrderVehicles, o)
	}

	return nil
}

// RemoveOrder relationship.
// Sets o.R.Order to nil.
// Removes o from all passed in related items' relationships struct.
func (o *RedeemOrderVehicle) RemoveOrder(ctx context.Context, exec boil.ContextExecutor, related *OrderVehicle) error {
	var err error

	queries.SetScanner(&o.OrderID, nil)
	if _, err = o.Update(ctx, exec, boil.Whitelist("order_id")); err != nil {
		return errors.Wrap(err, "failed to update local table")
	}

	if o.R != nil {
		o.R.Order = nil
	}
	if related == nil || related.R == nil {
		return nil
	}

	for i, ri := range related.R.OrderRedeemOrderVehicles {
		if queries.Equal(o.OrderID, ri.OrderID) {
			continue
		}

		ln := len(related.R.OrderRedeemOrderVehicles)
		if ln > 1 && i < ln-1 {
			related.R.OrderRedeemOrderVehicles[i] = related.R.OrderRedeemOrderVehicles[ln-1]
		}
		related.R.OrderRedeemOrderVehicles = related.R.OrderRedeemOrderVehicles[:ln-1]
		break
	}
	return nil
}

// RedeemOrderVehicles retrieves all the records using an executor.
func RedeemOrderVehicles(mods ...qm.QueryMod) redeemOrderVehicleQuery {
	mods = append(mods, qm.From("\"redeem_order_vehicle\""))
	q := NewQuery(mods...)
	if len(queries.GetSelect(q)) == 0 {
		queries.SetSelect(q, []string{"\"redeem_order_vehicle\".*"})
	}

	return redeemOrderVehicleQuery{q}
}

// FindRedeemOrderVehicle retrieves a single record by ID with an executor.
// If selectCols is empty Find will return all columns.
func FindRedeemOrderVehicle(ctx context.Context, exec boil.ContextExecutor, iD int64, selectCols ...string) (*RedeemOrderVehicle, error) {
	redeemOrderVehicleObj := &RedeemOrderVehicle{}

	sel := "*"
	if len(selectCols) > 0 {
		sel = strings.Join(strmangle.IdentQuoteSlice(dialect.LQ, dialect.RQ, selectCols), ",")
	}
	query := fmt.Sprintf(
		"select %s from \"redeem_order_vehicle\" where \"id\"=$1", sel,
	)

	q := queries.Raw(query, iD)

	err := q.Bind(ctx, exec, redeemOrderVehicleObj)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, sql.ErrNoRows
		}
		return nil, errors.Wrap(err, "models: unable to select from redeem_order_vehicle")
	}

	if err = redeemOrderVehicleObj.doAfterSelectHooks(ctx, exec); err != nil {
		return redeemOrderVehicleObj, err
	}

	return redeemOrderVehicleObj, nil
}

// Insert a single record using an executor.
// See boil.Columns.InsertColumnSet documentation to understand column list inference for inserts.
func (o *RedeemOrderVehicle) Insert(ctx context.Context, exec boil.ContextExecutor, columns boil.Columns) error {
	if o == nil {
		return errors.New("models: no redeem_order_vehicle provided for insertion")
	}

	var err error
	if !boil.TimestampsAreSkipped(ctx) {
		currTime := time.Now().In(boil.GetLocation())

		if o.CreatedAt.IsZero() {
			o.CreatedAt = currTime
		}
		if o.UpdatedAt.IsZero() {
			o.UpdatedAt = currTime
		}
	}

	if err := o.doBeforeInsertHooks(ctx, exec); err != nil {
		return err
	}

	nzDefaults := queries.NonZeroDefaultSet(redeemOrderVehicleColumnsWithDefault, o)

	key := makeCacheKey(columns, nzDefaults)
	redeemOrderVehicleInsertCacheMut.RLock()
	cache, cached := redeemOrderVehicleInsertCache[key]
	redeemOrderVehicleInsertCacheMut.RUnlock()

	if !cached {
		wl, returnColumns := columns.InsertColumnSet(
			redeemOrderVehicleAllColumns,
			redeemOrderVehicleColumnsWithDefault,
			redeemOrderVehicleColumnsWithoutDefault,
			nzDefaults,
		)

		cache.valueMapping, err = queries.BindMapping(redeemOrderVehicleType, redeemOrderVehicleMapping, wl)
		if err != nil {
			return err
		}
		cache.retMapping, err = queries.BindMapping(redeemOrderVehicleType, redeemOrderVehicleMapping, returnColumns)
		if err != nil {
			return err
		}
		if len(wl) != 0 {
			cache.query = fmt.Sprintf("INSERT INTO \"redeem_order_vehicle\" (\"%s\") %%sVALUES (%s)%%s", strings.Join(wl, "\",\""), strmangle.Placeholders(dialect.UseIndexPlaceholders, len(wl), 1, 1))
		} else {
			cache.query = "INSERT INTO \"redeem_order_vehicle\" %sDEFAULT VALUES%s"
		}

		var queryOutput, queryReturning string

		if len(cache.retMapping) != 0 {
			queryReturning = fmt.Sprintf(" RETURNING \"%s\"", strings.Join(returnColumns, "\",\""))
		}

		cache.query = fmt.Sprintf(cache.query, queryOutput, queryReturning)
	}

	value := reflect.Indirect(reflect.ValueOf(o))
	vals := queries.ValuesFromMapping(value, cache.valueMapping)

	if boil.IsDebug(ctx) {
		writer := boil.DebugWriterFrom(ctx)
		fmt.Fprintln(writer, cache.query)
		fmt.Fprintln(writer, vals)
	}

	if len(cache.retMapping) != 0 {
		err = exec.QueryRowContext(ctx, cache.query, vals...).Scan(queries.PtrsFromMapping(value, cache.retMapping)...)
	} else {
		_, err = exec.ExecContext(ctx, cache.query, vals...)
	}

	if err != nil {
		return errors.Wrap(err, "models: unable to insert into redeem_order_vehicle")
	}

	if !cached {
		redeemOrderVehicleInsertCacheMut.Lock()
		redeemOrderVehicleInsertCache[key] = cache
		redeemOrderVehicleInsertCacheMut.Unlock()
	}

	return o.doAfterInsertHooks(ctx, exec)
}

// Update uses an executor to update the RedeemOrderVehicle.
// See boil.Columns.UpdateColumnSet documentation to understand column list inference for updates.
// Update does not automatically update the record in case of default values. Use .Reload() to refresh the records.
func (o *RedeemOrderVehicle) Update(ctx context.Context, exec boil.ContextExecutor, columns boil.Columns) (int64, error) {
	if !boil.TimestampsAreSkipped(ctx) {
		currTime := time.Now().In(boil.GetLocation())

		o.UpdatedAt = currTime
	}

	var err error
	if err = o.doBeforeUpdateHooks(ctx, exec); err != nil {
		return 0, err
	}
	key := makeCacheKey(columns, nil)
	redeemOrderVehicleUpdateCacheMut.RLock()
	cache, cached := redeemOrderVehicleUpdateCache[key]
	redeemOrderVehicleUpdateCacheMut.RUnlock()

	if !cached {
		wl := columns.UpdateColumnSet(
			redeemOrderVehicleAllColumns,
			redeemOrderVehiclePrimaryKeyColumns,
		)

		if !columns.IsWhitelist() {
			wl = strmangle.SetComplement(wl, []string{"created_at"})
		}
		if len(wl) == 0 {
			return 0, errors.New("models: unable to update redeem_order_vehicle, could not build whitelist")
		}

		cache.query = fmt.Sprintf("UPDATE \"redeem_order_vehicle\" SET %s WHERE %s",
			strmangle.SetParamNames("\"", "\"", 1, wl),
			strmangle.WhereClause("\"", "\"", len(wl)+1, redeemOrderVehiclePrimaryKeyColumns),
		)
		cache.valueMapping, err = queries.BindMapping(redeemOrderVehicleType, redeemOrderVehicleMapping, append(wl, redeemOrderVehiclePrimaryKeyColumns...))
		if err != nil {
			return 0, err
		}
	}

	values := queries.ValuesFromMapping(reflect.Indirect(reflect.ValueOf(o)), cache.valueMapping)

	if boil.IsDebug(ctx) {
		writer := boil.DebugWriterFrom(ctx)
		fmt.Fprintln(writer, cache.query)
		fmt.Fprintln(writer, values)
	}
	var result sql.Result
	result, err = exec.ExecContext(ctx, cache.query, values...)
	if err != nil {
		return 0, errors.Wrap(err, "models: unable to update redeem_order_vehicle row")
	}

	rowsAff, err := result.RowsAffected()
	if err != nil {
		return 0, errors.Wrap(err, "models: failed to get rows affected by update for redeem_order_vehicle")
	}

	if !cached {
		redeemOrderVehicleUpdateCacheMut.Lock()
		redeemOrderVehicleUpdateCache[key] = cache
		redeemOrderVehicleUpdateCacheMut.Unlock()
	}

	return rowsAff, o.doAfterUpdateHooks(ctx, exec)
}

// UpdateAll updates all rows with the specified column values.
func (q redeemOrderVehicleQuery) UpdateAll(ctx context.Context, exec boil.ContextExecutor, cols M) (int64, error) {
	queries.SetUpdate(q.Query, cols)

	result, err := q.Query.ExecContext(ctx, exec)
	if err != nil {
		return 0, errors.Wrap(err, "models: unable to update all for redeem_order_vehicle")
	}

	rowsAff, err := result.RowsAffected()
	if err != nil {
		return 0, errors.Wrap(err, "models: unable to retrieve rows affected for redeem_order_vehicle")
	}

	return rowsAff, nil
}

// UpdateAll updates all rows with the specified column values, using an executor.
func (o RedeemOrderVehicleSlice) UpdateAll(ctx context.Context, exec boil.ContextExecutor, cols M) (int64, error) {
	ln := int64(len(o))
	if ln == 0 {
		return 0, nil
	}

	if len(cols) == 0 {
		return 0, errors.New("models: update all requires at least one column argument")
	}

	colNames := make([]string, len(cols))
	args := make([]interface{}, len(cols))

	i := 0
	for name, value := range cols {
		colNames[i] = name
		args[i] = value
		i++
	}

	// Append all of the primary key values for each column
	for _, obj := range o {
		pkeyArgs := queries.ValuesFromMapping(reflect.Indirect(reflect.ValueOf(obj)), redeemOrderVehiclePrimaryKeyMapping)
		args = append(args, pkeyArgs...)
	}

	sql := fmt.Sprintf("UPDATE \"redeem_order_vehicle\" SET %s WHERE %s",
		strmangle.SetParamNames("\"", "\"", 1, colNames),
		strmangle.WhereClauseRepeated(string(dialect.LQ), string(dialect.RQ), len(colNames)+1, redeemOrderVehiclePrimaryKeyColumns, len(o)))

	if boil.IsDebug(ctx) {
		writer := boil.DebugWriterFrom(ctx)
		fmt.Fprintln(writer, sql)
		fmt.Fprintln(writer, args...)
	}
	result, err := exec.ExecContext(ctx, sql, args...)
	if err != nil {
		return 0, errors.Wrap(err, "models: unable to update all in redeemOrderVehicle slice")
	}

	rowsAff, err := result.RowsAffected()
	if err != nil {
		return 0, errors.Wrap(err, "models: unable to retrieve rows affected all in update all redeemOrderVehicle")
	}
	return rowsAff, nil
}

// Upsert attempts an insert using an executor, and does an update or ignore on conflict.
// See boil.Columns documentation for how to properly use updateColumns and insertColumns.
func (o *RedeemOrderVehicle) Upsert(ctx context.Context, exec boil.ContextExecutor, updateOnConflict bool, conflictColumns []string, updateColumns, insertColumns boil.Columns) error {
	if o == nil {
		return errors.New("models: no redeem_order_vehicle provided for upsert")
	}
	if !boil.TimestampsAreSkipped(ctx) {
		currTime := time.Now().In(boil.GetLocation())

		if o.CreatedAt.IsZero() {
			o.CreatedAt = currTime
		}
		o.UpdatedAt = currTime
	}

	if err := o.doBeforeUpsertHooks(ctx, exec); err != nil {
		return err
	}

	nzDefaults := queries.NonZeroDefaultSet(redeemOrderVehicleColumnsWithDefault, o)

	// Build cache key in-line uglily - mysql vs psql problems
	buf := strmangle.GetBuffer()
	if updateOnConflict {
		buf.WriteByte('t')
	} else {
		buf.WriteByte('f')
	}
	buf.WriteByte('.')
	for _, c := range conflictColumns {
		buf.WriteString(c)
	}
	buf.WriteByte('.')
	buf.WriteString(strconv.Itoa(updateColumns.Kind))
	for _, c := range updateColumns.Cols {
		buf.WriteString(c)
	}
	buf.WriteByte('.')
	buf.WriteString(strconv.Itoa(insertColumns.Kind))
	for _, c := range insertColumns.Cols {
		buf.WriteString(c)
	}
	buf.WriteByte('.')
	for _, c := range nzDefaults {
		buf.WriteString(c)
	}
	key := buf.String()
	strmangle.PutBuffer(buf)

	redeemOrderVehicleUpsertCacheMut.RLock()
	cache, cached := redeemOrderVehicleUpsertCache[key]
	redeemOrderVehicleUpsertCacheMut.RUnlock()

	var err error

	if !cached {
		insert, ret := insertColumns.InsertColumnSet(
			redeemOrderVehicleAllColumns,
			redeemOrderVehicleColumnsWithDefault,
			redeemOrderVehicleColumnsWithoutDefault,
			nzDefaults,
		)

		update := updateColumns.UpdateColumnSet(
			redeemOrderVehicleAllColumns,
			redeemOrderVehiclePrimaryKeyColumns,
		)

		if updateOnConflict && len(update) == 0 {
			return errors.New("models: unable to upsert redeem_order_vehicle, could not build update column list")
		}

		conflict := conflictColumns
		if len(conflict) == 0 {
			conflict = make([]string, len(redeemOrderVehiclePrimaryKeyColumns))
			copy(conflict, redeemOrderVehiclePrimaryKeyColumns)
		}
		cache.query = buildUpsertQueryPostgres(dialect, "\"redeem_order_vehicle\"", updateOnConflict, ret, update, conflict, insert)

		cache.valueMapping, err = queries.BindMapping(redeemOrderVehicleType, redeemOrderVehicleMapping, insert)
		if err != nil {
			return err
		}
		if len(ret) != 0 {
			cache.retMapping, err = queries.BindMapping(redeemOrderVehicleType, redeemOrderVehicleMapping, ret)
			if err != nil {
				return err
			}
		}
	}

	value := reflect.Indirect(reflect.ValueOf(o))
	vals := queries.ValuesFromMapping(value, cache.valueMapping)
	var returns []interface{}
	if len(cache.retMapping) != 0 {
		returns = queries.PtrsFromMapping(value, cache.retMapping)
	}

	if boil.IsDebug(ctx) {
		writer := boil.DebugWriterFrom(ctx)
		fmt.Fprintln(writer, cache.query)
		fmt.Fprintln(writer, vals)
	}
	if len(cache.retMapping) != 0 {
		err = exec.QueryRowContext(ctx, cache.query, vals...).Scan(returns...)
		if errors.Is(err, sql.ErrNoRows) {
			err = nil // Postgres doesn't return anything when there's no update
		}
	} else {
		_, err = exec.ExecContext(ctx, cache.query, vals...)
	}
	if err != nil {
		return errors.Wrap(err, "models: unable to upsert redeem_order_vehicle")
	}

	if !cached {
		redeemOrderVehicleUpsertCacheMut.Lock()
		redeemOrderVehicleUpsertCache[key] = cache
		redeemOrderVehicleUpsertCacheMut.Unlock()
	}

	return o.doAfterUpsertHooks(ctx, exec)
}

// Delete deletes a single RedeemOrderVehicle record with an executor.
// Delete will match against the primary key column to find the record to delete.
func (o *RedeemOrderVehicle) Delete(ctx context.Context, exec boil.ContextExecutor) (int64, error) {
	if o == nil {
		return 0, errors.New("models: no RedeemOrderVehicle provided for delete")
	}

	if err := o.doBeforeDeleteHooks(ctx, exec); err != nil {
		return 0, err
	}

	args := queries.ValuesFromMapping(reflect.Indirect(reflect.ValueOf(o)), redeemOrderVehiclePrimaryKeyMapping)
	sql := "DELETE FROM \"redeem_order_vehicle\" WHERE \"id\"=$1"

	if boil.IsDebug(ctx) {
		writer := boil.DebugWriterFrom(ctx)
		fmt.Fprintln(writer, sql)
		fmt.Fprintln(writer, args...)
	}
	result, err := exec.ExecContext(ctx, sql, args...)
	if err != nil {
		return 0, errors.Wrap(err, "models: unable to delete from redeem_order_vehicle")
	}

	rowsAff, err := result.RowsAffected()
	if err != nil {
		return 0, errors.Wrap(err, "models: failed to get rows affected by delete for redeem_order_vehicle")
	}

	if err := o.doAfterDeleteHooks(ctx, exec); err != nil {
		return 0, err
	}

	return rowsAff, nil
}

// DeleteAll deletes all matching rows.
func (q redeemOrderVehicleQuery) DeleteAll(ctx context.Context, exec boil.ContextExecutor) (int64, error) {
	if q.Query == nil {
		return 0, errors.New("models: no redeemOrderVehicleQuery provided for delete all")
	}

	queries.SetDelete(q.Query)

	result, err := q.Query.ExecContext(ctx, exec)
	if err != nil {
		return 0, errors.Wrap(err, "models: unable to delete all from redeem_order_vehicle")
	}

	rowsAff, err := result.RowsAffected()
	if err != nil {
		return 0, errors.Wrap(err, "models: failed to get rows affected by deleteall for redeem_order_vehicle")
	}

	return rowsAff, nil
}

// DeleteAll deletes all rows in the slice, using an executor.
func (o RedeemOrderVehicleSlice) DeleteAll(ctx context.Context, exec boil.ContextExecutor) (int64, error) {
	if len(o) == 0 {
		return 0, nil
	}

	if len(redeemOrderVehicleBeforeDeleteHooks) != 0 {
		for _, obj := range o {
			if err := obj.doBeforeDeleteHooks(ctx, exec); err != nil {
				return 0, err
			}
		}
	}

	var args []interface{}
	for _, obj := range o {
		pkeyArgs := queries.ValuesFromMapping(reflect.Indirect(reflect.ValueOf(obj)), redeemOrderVehiclePrimaryKeyMapping)
		args = append(args, pkeyArgs...)
	}

	sql := "DELETE FROM \"redeem_order_vehicle\" WHERE " +
		strmangle.WhereClauseRepeated(string(dialect.LQ), string(dialect.RQ), 1, redeemOrderVehiclePrimaryKeyColumns, len(o))

	if boil.IsDebug(ctx) {
		writer := boil.DebugWriterFrom(ctx)
		fmt.Fprintln(writer, sql)
		fmt.Fprintln(writer, args)
	}
	result, err := exec.ExecContext(ctx, sql, args...)
	if err != nil {
		return 0, errors.Wrap(err, "models: unable to delete all from redeemOrderVehicle slice")
	}

	rowsAff, err := result.RowsAffected()
	if err != nil {
		return 0, errors.Wrap(err, "models: failed to get rows affected by deleteall for redeem_order_vehicle")
	}

	if len(redeemOrderVehicleAfterDeleteHooks) != 0 {
		for _, obj := range o {
			if err := obj.doAfterDeleteHooks(ctx, exec); err != nil {
				return 0, err
			}
		}
	}

	return rowsAff, nil
}

// Reload refetches the object from the database
// using the primary keys with an executor.
func (o *RedeemOrderVehicle) Reload(ctx context.Context, exec boil.ContextExecutor) error {
	ret, err := FindRedeemOrderVehicle(ctx, exec, o.ID)
	if err != nil {
		return err
	}

	*o = *ret
	return nil
}

// ReloadAll refetches every row with matching primary key column values
// and overwrites the original object slice with the newly updated slice.
func (o *RedeemOrderVehicleSlice) ReloadAll(ctx context.Context, exec boil.ContextExecutor) error {
	if o == nil || len(*o) == 0 {
		return nil
	}

	slice := RedeemOrderVehicleSlice{}
	var args []interface{}
	for _, obj := range *o {
		pkeyArgs := queries.ValuesFromMapping(reflect.Indirect(reflect.ValueOf(obj)), redeemOrderVehiclePrimaryKeyMapping)
		args = append(args, pkeyArgs...)
	}

	sql := "SELECT \"redeem_order_vehicle\".* FROM \"redeem_order_vehicle\" WHERE " +
		strmangle.WhereClauseRepeated(string(dialect.LQ), string(dialect.RQ), 1, redeemOrderVehiclePrimaryKeyColumns, len(*o))

	q := queries.Raw(sql, args...)

	err := q.Bind(ctx, exec, &slice)
	if err != nil {
		return errors.Wrap(err, "models: unable to reload all in RedeemOrderVehicleSlice")
	}

	*o = slice

	return nil
}

// RedeemOrderVehicleExists checks if the RedeemOrderVehicle row exists.
func RedeemOrderVehicleExists(ctx context.Context, exec boil.ContextExecutor, iD int64) (bool, error) {
	var exists bool
	sql := "select exists(select 1 from \"redeem_order_vehicle\" where \"id\"=$1 limit 1)"

	if boil.IsDebug(ctx) {
		writer := boil.DebugWriterFrom(ctx)
		fmt.Fprintln(writer, sql)
		fmt.Fprintln(writer, iD)
	}
	row := exec.QueryRowContext(ctx, sql, iD)

	err := row.Scan(&exists)
	if err != nil {
		return false, errors.Wrap(err, "models: unable to check if redeem_order_vehicle exists")
	}

	return exists, nil
}

// Exists checks if the RedeemOrderVehicle row exists.
func (o *RedeemOrderVehicle) Exists(ctx context.Context, exec boil.ContextExecutor) (bool, error) {
	return RedeemOrderVehicleExists(ctx, exec, o.ID)
}
