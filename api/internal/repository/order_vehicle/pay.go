package ordervehicle

import (
	models "Golang/internal/repository/dbmodel"
	"context"
	"time"

	"github.com/volatiletech/sqlboiler/v4/boil"
	"github.com/volatiletech/sqlboiler/v4/queries/qm"
)

func (rep Repository) CreatePayVehicle(m models.InterestPaymentsVehicle) (models.InterestPaymentsVehicle, error) {
	pay := &models.InterestPaymentsVehicle{
		OrderID:     m.OrderID,
		Amount:      m.Amount,
		PaymentDate: m.PaymentDate,
		NextPaymentDate: m.NextPaymentDate,
	}
	if err := pay.Insert(context.Background(), rep.db, boil.Infer()); err != nil {
		return models.InterestPaymentsVehicle{}, err
	}
	return m, nil
}

func (rep Repository) ListDayPaymentNewest(id int64) (time.Time, error) {
	payment, err := models.InterestPaymentsVehicles(qm.Where("order_id = ?", id),
		qm.OrderBy("created_at DESC"), qm.Limit(1)).One(context.Background(), rep.db)
	if err != nil {
		return time.Time{}, err
	}
	return payment.PaymentDate.Time, nil
}

func (rep Repository) CalculateInterest(amount int, interest float64, startDate, endDate time.Time) float64 {
	duration := endDate.Sub(startDate)
	days := int(duration.Hours() / 24)
	interestPerDay := float64(amount) * (interest / 100) / 30
	interestAmount := interestPerDay * float64(days)

	return interestAmount
}
func (rep Repository) ListOrderVehicleByPaymentOver() ([]*models.OrderVehicle, []*models.InterestPaymentsVehicle, error) {
	currentTime := time.Now()

	// Lấy danh sách các đơn hàng có lần thanh toán gần nhất trễ hạn
	orders, err := models.OrderVehicles(
		qm.InnerJoin("interest_payments_vehicle ON interest_payments_vehicle.order_id = order_vehicle.id"),
		qm.Where("interest_payments_vehicle.next_payment_date < ?", currentTime),
		qm.And("status = ?", "0"),
		qm.GroupBy("order_vehicle.id"),
		qm.Load(models.OrderVehicleRels.OrderInterestPaymentsVehicles, qm.OrderBy("interest_payments_vehicle.next_payment_date DESC")),
	).All(context.Background(), rep.db)
	if err != nil {
		return nil,nil, err
	}

	// Lọc danh sách các đơn hàng chỉ lấy đơn hàng có lần thanh toán gần nhất trễ hạn
	overdueOrders := make([]*models.OrderVehicle, 0)
	overduePayments := make([]*models.InterestPaymentsVehicle, 0)
	for _, order := range orders {
		if order.R.OrderInterestPaymentsVehicles != nil && len(order.R.OrderInterestPaymentsVehicles) > 0 {
			latestPayment := order.R.OrderInterestPaymentsVehicles[0]
			if latestPayment.NextPaymentDate.Valid && latestPayment.NextPaymentDate.Time.Before(currentTime) {
				overdueOrders = append(overdueOrders, order)
				overduePayments = append(overduePayments, latestPayment)
			}
		}
	}

	return overdueOrders,overduePayments, nil
}