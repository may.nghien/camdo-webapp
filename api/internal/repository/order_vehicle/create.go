package ordervehicle

import (
	models "Golang/internal/repository/dbmodel"
	"context"

	"github.com/volatiletech/sqlboiler/v4/boil"
)

func (rep Repository) CreateOrderVehicle(m models.OrderVehicle) (models.OrderVehicle, error) {
	order := &models.OrderVehicle{
		ProductName:       m.ProductName,
		AddPayments:       m.AddPayments,
		Price:             m.Price,
		PriceOld:          m.Price,
		LicensePlate:      m.LicensePlate,
		OrderDate:         m.OrderDate,
		CustomerID:        m.CustomerID,
		Percent:           m.Percent,
		ReturnDate:        m.ReturnDate,
		Note:              m.Note,
		Status:            m.Status,
		InterestPaidCount: m.InterestPaidCount,
	}
	if err := order.Insert(context.Background(), rep.db, boil.Infer()); err != nil {
		return models.OrderVehicle{}, err
	}

	return m, nil
}
