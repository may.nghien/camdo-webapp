package ordervehicle

import (
	models "Golang/internal/repository/dbmodel"
	"context"

	"github.com/volatiletech/sqlboiler/v4/queries/qm"
)

func (rep Repository) SearchOrderVehicleByID(id int64) ([]models.OrderVehicle, error) {
	order, err := models.OrderVehicles(qm.Where("id = ?", id)).All(context.Background(), rep.db)

	if err != nil {
		return nil, err
	}

	var result = make([]models.OrderVehicle, len(order))
	for ind, o := range order {
		result[ind] = models.OrderVehicle{
			ID:                o.ID,
			CustomerID:        o.CustomerID,
			Price:             o.Price,
			PriceOld:          o.PriceOld,
			ProductName:       o.ProductName,
			AddPayments:       o.AddPayments,
			LicensePlate:      o.LicensePlate,
			Percent:           o.Percent,
			ReturnDate:        o.ReturnDate,
			Status:            o.Status,
			OrderDate:         o.OrderDate,
			Note:              o.Note,
			Miss:              o.Miss,
			InterestPaidCount: o.InterestPaidCount,
		}
	}

	return result, nil
}
