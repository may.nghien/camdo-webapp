package ordervehicle

import (
	models "Golang/internal/repository/dbmodel"
	"context"

	"github.com/volatiletech/null/v8"
	"github.com/volatiletech/sqlboiler/v4/boil"
	"github.com/volatiletech/sqlboiler/v4/queries/qm"
)

func (rep Repository) UpdateStatus(id int64, status null.String) (models.OrderVehicle, error) {
	order, err := models.OrderVehicles(
		qm.Where("id = ?", id),
		qm.Limit(1),
	).One(context.Background(), rep.db)
	if err != nil {
		return models.OrderVehicle{}, err
	}

	order.Status = status
	_, err = order.Update(context.Background(), rep.db, boil.Infer())
	if err != nil {
		return models.OrderVehicle{}, err
	}

	return *order, nil

}
