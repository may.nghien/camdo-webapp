package ordervehicle

import (
	models "Golang/internal/repository/dbmodel"
	"context"
	"time"

	"github.com/volatiletech/sqlboiler/v4/queries/qm"
)

func (rep Repository) ListOrdersVehicleOver() ([]models.OrderVehicle, error) {
	overdueOrders, err := models.OrderVehicles(
		qm.Where("return_date < ?", time.Now().Format(time.RFC3339)),
		qm.And("status = ?", "0"),
		qm.And("interest_paid_count = ?", 0),
		qm.OrderBy("id DESC"),
	).All(context.Background(), rep.db)
	if err != nil {
		return nil, err
	}
	var orders []models.OrderVehicle
	for _, o := range overdueOrders {
		order := models.OrderVehicle{
			ID:                o.ID,
			CustomerID:        o.CustomerID,
			Price:             o.Price,
			PriceOld:          o.PriceOld,
			ProductName:       o.ProductName,
			AddPayments:       o.AddPayments,
			Percent:           o.Percent,
			LicensePlate:      o.LicensePlate,
			ReturnDate:        o.ReturnDate,
			Status:            o.Status,
			Note:              o.Note,
			Miss:              o.Miss,
			OrderDate:         o.OrderDate,
			InterestPaidCount: o.InterestPaidCount,
		}
		orders = append(orders, order)
	}

	return orders, nil
}

func (rep Repository) ListOrderVehicleHavePaymentOver() ([]models.OrderVehicle, error) {
	currentTime := time.Now()

	// Lấy danh sách các đơn hàng có lần thanh toán gần nhất trễ hạn
	orders, err := models.OrderVehicles(
		qm.InnerJoin("interest_payments_vehicle ON interest_payments_vehicle.order_id = order_vehicle.id"),
		qm.Where("interest_payments_vehicle.next_payment_date < ?", currentTime),
		qm.And("status = ?", "0"),
		qm.GroupBy("order_vehicle.id"),
		qm.Load(models.OrderVehicleRels.OrderInterestPaymentsVehicles, qm.OrderBy("interest_payments_vehicle.next_payment_date DESC")),
	).All(context.Background(), rep.db)
	if err != nil {
		return nil, err
	}

	// Lọc danh sách các đơn hàng chỉ lấy đơn hàng có lần thanh toán gần nhất trễ hạn
	overdueOrders := make([]models.OrderVehicle, 0)
	for _, order := range orders {
		if order.R.OrderInterestPaymentsVehicles != nil && len(order.R.OrderInterestPaymentsVehicles) > 0 {
			latestPayment := order.R.OrderInterestPaymentsVehicles[0]
			if latestPayment.NextPaymentDate.Valid && latestPayment.NextPaymentDate.Time.Before(currentTime) {
				overdueOrders = append(overdueOrders, *order)
			}
		}
	}

	return overdueOrders, nil
}

func (rep Repository) SearchOrderOverVehicleByID(id int64) ([]models.OrderVehicle, error) {
	overdueOrders, err := models.OrderVehicles(
		qm.Where("return_date < ?", time.Now().Format(time.RFC3339)),
		qm.And("status = ?", "0"),
		qm.And("id = ?", id),
		qm.And("interest_paid_count = ?", 0),
	).All(context.Background(), rep.db)
	if err != nil {
		return nil, err
	}

	if err != nil {
		return nil, err
	}

	var orders []models.OrderVehicle
	for _, o := range overdueOrders {
		order := models.OrderVehicle{
			ID:                o.ID,
			CustomerID:        o.CustomerID,
			Price:             o.Price,
			PriceOld:          o.PriceOld,
			ProductName:       o.ProductName,
			AddPayments:    o.AddPayments,
			Percent:           o.Percent,
			LicensePlate:      o.LicensePlate,
			ReturnDate:        o.ReturnDate,
			Status:            o.Status,
			Note:              o.Note,
			Miss:              o.Miss,
			OrderDate:         o.OrderDate,
			InterestPaidCount: o.InterestPaidCount,
		}
		orders = append(orders, order)
	}

	return orders, nil
}
func (rep Repository) SearchOrderOverPaymentVehicleByID(id int64) ([]*models.OrderVehicle, error) {
	currentTime := time.Now()

	// Lấy danh sách các đơn hàng có lần thanh toán gần nhất trễ hạn
	orders, err := models.OrderVehicles(
		qm.InnerJoin("interest_payments_vehicle ON interest_payments_vehicle.order_id = order_vehicle.id"),
		qm.Where("interest_payments_vehicle.next_payment_date < ?", currentTime),
		qm.And("status = ?", "0"),
		qm.And("id = ?", id),
		qm.GroupBy("order_vehicle.id"),
		qm.Load(models.OrderVehicleRels.OrderInterestPaymentsVehicles, qm.OrderBy("interest_payments_vehicle.next_payment_date DESC")),
	).All(context.Background(), rep.db)
	if err != nil {
		return nil, err
	}

	// Lọc danh sách các đơn hàng chỉ lấy đơn hàng có lần thanh toán gần nhất trễ hạn
	overdueOrders := make([]*models.OrderVehicle, 0)
	for _, order := range orders {
		if order.R.OrderInterestPaymentsVehicles != nil && len(order.R.OrderInterestPaymentsVehicles) > 0 {
			latestPayment := order.R.OrderInterestPaymentsVehicles[0]
			if latestPayment.NextPaymentDate.Valid && latestPayment.NextPaymentDate.Time.Before(currentTime) {
				overdueOrders = append(overdueOrders, order)
			}
		}
	}

	return overdueOrders, nil
}
