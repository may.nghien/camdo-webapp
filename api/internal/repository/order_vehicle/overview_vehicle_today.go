package ordervehicle

import (
	models "Golang/internal/repository/dbmodel"
	"context"
	"time"

	"github.com/volatiletech/sqlboiler/v4/queries/qm"
)

func (rep Repository) GetListPaymentsVehicleToday() ([]models.InterestPaymentsVehicle, error) {
	var pawnOrders []models.InterestPaymentsVehicle
	today := time.Now().UTC()
	startDate := time.Date(today.Year(), today.Month(), today.Day(), 0, 0, 0, 0, time.UTC)
	endDate := startDate.AddDate(0, 0, 1).Add(-time.Nanosecond)

	// Query the database using SQLBoiler's query builder
	err := models.InterestPaymentsVehicles(
		qm.Where("payment_date >= ?", startDate),
		qm.Where("payment_date <= ?", endDate),
	).Bind(context.Background(), rep.db, &pawnOrders)
	if err != nil {
		return nil, err
	}

	return pawnOrders, nil
}

func (rep Repository) ListOrderVehicleToday() ([]models.OrderVehicle, error) {
	var pawnOrders []models.OrderVehicle
	today := time.Now().UTC()
	startDate := time.Date(today.Year(), today.Month(), today.Day(), 0, 0, 0, 0, time.UTC)
	endDate := startDate.AddDate(0, 0, 1).Add(-time.Nanosecond)

    // Query the database using SQLBoiler's query builder
    err := models.OrderVehicles(
        qm.Where("order_date >= ?", startDate),
        qm.Where("order_date <= ?", endDate),
    ).Bind(context.Background(), rep.db, &pawnOrders)
    if err != nil {
        return nil, err
    }
    
    return pawnOrders, nil
}

func (rep Repository) GetListRedeemVehicleToday() ([]models.RedeemOrderVehicle, error) {
	var pawnOrders []models.RedeemOrderVehicle
	today := time.Now().UTC()
	startDate := time.Date(today.Year(), today.Month(), today.Day(), 0, 0, 0, 0, time.UTC)
	endDate := startDate.AddDate(0, 0, 1).Add(-time.Nanosecond)

	// Query the database using SQLBoiler's query builder
	err := models.RedeemOrderVehicles(
		qm.Where("date_redeem >= ?", startDate),
		qm.Where("date_redeem <= ?", endDate),
	).Bind(context.Background(), rep.db, &pawnOrders)
	if err != nil {
		return nil, err
	}

	return pawnOrders, nil
}

func (rep Repository) GetListAddPaymentVehicleToday() ([]models.AddPaymentsVehicle, error) {
	var pawnOrders []models.AddPaymentsVehicle
	today := time.Now().UTC()
	startDate := time.Date(today.Year(), today.Month(), today.Day(), 0, 0, 0, 0, time.UTC)
	endDate := startDate.AddDate(0, 0, 1).Add(-time.Nanosecond)

	// Query the database using SQLBoiler's query builder
	err := models.AddPaymentsVehicles(
		qm.Where("date_add_payment >= ?", startDate),
		qm.Where("date_add_payment <= ?", endDate),
	).Bind(context.Background(), rep.db, &pawnOrders)
	if err != nil {
		return nil, err
	}

	return pawnOrders, nil
}