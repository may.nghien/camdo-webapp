package ordervehicle

import (
	models "Golang/internal/repository/dbmodel"
	"context"

	"github.com/volatiletech/sqlboiler/v4/queries/qm"
)


func (rep Repository) GetListPaymentByOrderID(id int64) ([]models.InterestPaymentsVehicle, error) {
	payments, err := models.InterestPaymentsVehicles(qm.Where("order_id=?", id)).All(context.Background(), rep.db)
	if err != nil {
		return nil, err
	}
	var result = make([]models.InterestPaymentsVehicle, len(payments))
	for ind, inp := range payments {
		result[ind] = models.InterestPaymentsVehicle{Amount:inp.Amount,PaymentID: inp.PaymentID, PaymentDate: inp.PaymentDate, NextPaymentDate: inp.NextPaymentDate }
	}

	return result, nil
}

func (rep Repository) GetListPaymentsVehicle() ([]models.InterestPaymentsVehicle, error) {
	payments, err := models.InterestPaymentsVehicles().All(context.Background(), rep.db)
	if err != nil {
		return nil, err
	}
	var result = make([]models.InterestPaymentsVehicle, len(payments))
	for ind, inp := range payments {
		result[ind] = models.InterestPaymentsVehicle{NextPaymentDate: inp.NextPaymentDate,OrderID: inp.OrderID,Amount:inp.Amount,PaymentID: inp.PaymentID, PaymentDate: inp.PaymentDate }
	}

	return result, nil
}
