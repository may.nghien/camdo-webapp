package category

import (
	models "Golang/internal/repository/dbmodel"
	"context"

	"github.com/volatiletech/sqlboiler/v4/boil"
)

// func generateID() string {
// 	// Tạo một chuỗi dữ liệu duy nhất từ thời gian hiện tại
// 	data := strconv.FormatInt(time.Now().UnixNano(), 10)

// 	// Tạo một đối tượng hash FNV
// 	h := fnv.New32a()

// 	// Đưa chuỗi dữ liệu vào đối tượng hash
// 	h.Write([]byte(data))

// 	// Lấy giá trị băm dưới dạng uint32
// 	hashValue := h.Sum32()

// 	// Chuyển đổi giá trị băm thành chuỗi và trả về
// 	return strconv.FormatUint(uint64(hashValue), 10)
// }
func (rep Repository) CreateCategory(m models.Category) (models.Category, error) {
	// Tạo số nguyên ngẫu nhiên từ 0 đến 999999999
	// rand.Seed(time.Now().UnixNano())
	// randomInt := rand.Intn(1000)

	// // Chuyển đổi số nguyên thành chuỗi và thêm thời gian hiện tại
	// // dưới dạng UnixNano để đảm bảo tính duy nhất của ID
	// id := strconv.Itoa(randomInt) + strconv.FormatInt(time.Now().UnixNano(), 10)
	// idStr := generateID()
	// id, err := strconv.Atoi(idStr)
	// if err != nil {
	// 	// Xử lý lỗi nếu có
	// 	fmt.Println("Lỗi chuyển đổi chuỗi thành số nguyên:", err)
	// }

	user := &models.Category{
		CategoryName: m.CategoryName,
	}
	if err := user.Insert(context.Background(), rep.db, boil.Infer()); err != nil {
		return models.Category{}, err
	}

	return m, nil
}

func (rep Repository) CreateCategoryGoal(m models.CategoryGoal) (models.CategoryGoal, error) {
	user := &models.CategoryGoal{
		CategoryName: m.CategoryName,
	}
	if err := user.Insert(context.Background(), rep.db, boil.Infer()); err != nil {
		return models.CategoryGoal{}, err
	}

	return m, nil
}

func (rep Repository) CreateCategoryPercent(m models.CategoryPercent) (models.CategoryPercent, error) {
	user := &models.CategoryPercent{
		Percent: m.Percent,
	}
	if err := user.Insert(context.Background(), rep.db, boil.Infer()); err != nil {
		return models.CategoryPercent{}, err
	}

	return m, nil
}