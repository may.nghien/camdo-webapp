package category

import (
	models "Golang/internal/repository/dbmodel"
	"context"

	"github.com/volatiletech/sqlboiler/v4/queries/qm"
)

func (rep Repository) DeleteCategory(id int) error {
	_, err := models.Categories(qm.Where("category_id=?", id)).DeleteAll(context.Background(), rep.db)
	return err
}

func (rep Repository) DeleteCategoryGoal(id int) error {
	_, err := models.CategoryGoals(qm.Where("category_id=?", id)).DeleteAll(context.Background(), rep.db)
	return err
}

func (rep Repository) DeleteCategoryPercent(id int) error {
	_, err := models.CategoryPercents(qm.Where("percent_id=?", id)).DeleteAll(context.Background(), rep.db)
	return err
}

