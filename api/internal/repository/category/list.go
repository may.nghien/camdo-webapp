package category

import (
	models "Golang/internal/repository/dbmodel"
	"github.com/volatiletech/sqlboiler/v4/queries/qm"
	"context"

	"github.com/volatiletech/null/v8"
)

type CategoryOutput struct {
	CategoryID   int
	CategoryName null.String
}
type CategoryPercentOutput struct {
	PercentID int
	Percent   null.Float64
}

func (rep Repository) GetListCategory() ([]CategoryOutput, error) {
	users, err := models.Categories().All(context.Background(), rep.db)
	if err != nil {
		return nil, err
	}

	var result = make([]CategoryOutput, len(users))
	for ind, cate := range users {
		result[ind] = CategoryOutput{CategoryID: int(cate.CategoryID), CategoryName: cate.CategoryName}
	}

	return result, nil
}
func (rep Repository) GetListCategoryGoal() ([]CategoryOutput, error) {
	users, err := models.CategoryGoals().All(context.Background(), rep.db)
	if err != nil {
		return nil, err
	}

	var result = make([]CategoryOutput, len(users))
	for ind, cate := range users {
		result[ind] = CategoryOutput{CategoryID: int(cate.CategoryID), CategoryName: cate.CategoryName}
	}

	return result, nil
}
func (rep Repository) GetListCategoryByID(id int) (models.Category, error) {
	category, err := models.Categories(qm.Where("category_id=?", id)).One(context.Background(), rep.db)
	if err != nil {
		return models.Category{}, err
	}


	return *category, nil
}
func (rep Repository) GetListCategoryGoalByID(id int) (models.CategoryGoal, error) {
	category, err := models.CategoryGoals(qm.Where("category_id=?", id)).One(context.Background(), rep.db)
	if err != nil {
		return models.CategoryGoal{}, err
	}

	return *category, nil
}
func (rep Repository) GetListCategoryPercent() ([]CategoryPercentOutput, error) {
	users, err := models.CategoryPercents().All(context.Background(), rep.db)
	if err != nil {
		return nil, err
	}

	var result = make([]CategoryPercentOutput, len(users))
	for ind, cate := range users {
		result[ind] = CategoryPercentOutput{PercentID: int(cate.PercentID), Percent: cate.Percent}
	}

	return result, nil
}
func (rep Repository) GetListCategoryPercentByID(id int) (models.CategoryPercent, error) {
	category, err := models.CategoryPercents(qm.Where("percent_id=?", id)).One(context.Background(), rep.db)
	if err != nil {
		return models.CategoryPercent{}, err
	}


	return *category, nil
}