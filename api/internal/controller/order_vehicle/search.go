package ordervehicle

import (
	"github.com/volatiletech/null/v8"
)

func (c Controller) SearchOrderVehicleByID(id int64) ([]OrderProduct, error) {
	orders, err := c.orderRepo.SearchOrderVehicleByID(id)
	if err != nil {
		return nil, err
	}

	var result []OrderProduct
	for _, inp := range orders {
		order := OrderVehicleOutput{
			ID:                int64(inp.ID),
			Percent:           inp.Percent,
			ProductName:       null.String(inp.ProductName),
			AddPayments:       inp.AddPayments,
			Price:             null.Int(inp.Price),
			PriceOld:          inp.PriceOld,
			OrderDate:         null.Time(inp.OrderDate),
			ReturnDate:        null.Time(inp.ReturnDate),
			CustomerID:        inp.CustomerID,
			LicensePlate:      inp.LicensePlate,
			Status:            inp.Status,
			Note:              inp.Note,
			Miss:              inp.Miss,
			InterestPaidCount: inp.InterestPaidCount,
		}
		payments, err := c.orderRepo.GetListPaymentByOrderID(int64(inp.ID))
		if err != nil {
			return nil, err
		}
		addPayments, err := c.orderRepo.GetAddPaymentInfo(int64(inp.ID))
		if err != nil {
			return nil, err
		}
		var convertedPayments []InterestPaidOutput
		for _, payment := range payments {
			convertedPayment := InterestPaidOutput{
				OrderID:         null.Int64From(inp.ID),
				Amount:          payment.Amount,
				PaymentDate:     payment.PaymentDate,
				NextPaymentDate: payment.NextPaymentDate,
			}
			convertedPayments = append(convertedPayments, convertedPayment)
		}
		var convertedAddPayments []AddPaymentOutput
		for _, p := range addPayments {
			convertedAddPayment := AddPaymentOutput{
				ID:             p.ID,
				IDVehicle:      p.IDVehicle,
				Amount:         p.Amount,
				DateAddPayment: p.DateAddPayment,
			}
			convertedAddPayments = append(convertedAddPayments, convertedAddPayment)
		}
		orderProduct := OrderProduct{
			Order:      order,
			Payments:   convertedPayments,
			AddPayment: convertedAddPayments,
		}

		result = append(result, orderProduct)
	}

	return result, nil
}
