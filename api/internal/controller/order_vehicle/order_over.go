package ordervehicle

import (
	"github.com/volatiletech/null/v8"
)

type OrderOverOutput struct {
	Order      OrderVehicleOutput   `json:"order"`
	AddPayment []AddPaymentOutput   `json:"add_payments"`
	Payments   []InterestPaidOutput `json:"payments"`
}

func (c Controller) GetOrderVehicleOver() ([]OrderOverOutput, error) {
	orders, err := c.orderRepo.ListOrdersVehicleOver()
	if err != nil {
		return nil, err
	}
	var result []OrderOverOutput
	for _, inp := range orders {
		order := OrderVehicleOutput{
			ID:                int64(inp.ID),
			LicensePlate:      inp.LicensePlate,
			Percent:           inp.Percent,
			ProductName:       null.String(inp.ProductName),
			AddPayments:       inp.AddPayments,
			Price:             null.Int(inp.Price),
			PriceOld:          null.Int(inp.PriceOld),
			OrderDate:         null.Time(inp.OrderDate),
			CustomerID:        inp.CustomerID,
			ReturnDate:        inp.ReturnDate,
			Status:            inp.Status,
			InterestPaidCount: inp.InterestPaidCount,
			Note:              inp.Note,
			Miss:              inp.Miss,
		}

		addPayments, err := c.orderRepo.GetAddPaymentInfo(int64(inp.ID))
		if err != nil {
			return nil, err
		}

		var convertedAddPayments []AddPaymentOutput
		for _, p := range addPayments {
			convertedAddPayment := AddPaymentOutput{
				ID:             p.ID,
				IDVehicle:      p.IDVehicle,
				Amount:         p.Amount,
				DateAddPayment: p.DateAddPayment,
			}
			convertedAddPayments = append(convertedAddPayments, convertedAddPayment)
		}
		// Tạo đối tượng OrderProduct chứa thông tin của đơn hàng và sản phẩm
		orderProduct := OrderOverOutput{
			Order:      order,
			AddPayment: convertedAddPayments,
		}

		result = append(result, orderProduct)
	}

	return result, nil
}

func (c Controller) GetOrderHavePaymentsOver() ([]OrderOverOutput, error) {
	orders, err := c.orderRepo.ListOrderVehicleHavePaymentOver()
	if err != nil {
		return nil, err
	}
	var result []OrderOverOutput
	for _, inp := range orders {
		order := OrderVehicleOutput{
			ID:                int64(inp.ID),
			Percent:           inp.Percent,
			LicensePlate:      inp.LicensePlate,
			ProductName:       null.String(inp.ProductName),
			AddPayments:       inp.AddPayments,
			Price:             null.Int(inp.Price),
			PriceOld:          null.Int(inp.PriceOld),
			OrderDate:         null.Time(inp.OrderDate),
			CustomerID:        inp.CustomerID,
			ReturnDate:        inp.ReturnDate,
			Status:            inp.Status,
			InterestPaidCount: inp.InterestPaidCount,
			Note:              inp.Note,
			Miss:              inp.Miss,
		}
		payments, err := c.orderRepo.GetListPaymentByOrderID(int64(inp.ID))
		if err != nil {
			return nil, err
		}
		addPayments, err := c.orderRepo.GetAddPaymentInfo(int64(inp.ID))
		if err != nil {
			return nil, err
		}
		var convertedPayments []InterestPaidOutput
		for _, payment := range payments {
			convertedPayment := InterestPaidOutput{
				OrderID:         null.Int64From(inp.ID),
				Amount:          payment.Amount,
				PaymentDate:     payment.PaymentDate,
				NextPaymentDate: payment.NextPaymentDate,
			}
			convertedPayments = append(convertedPayments, convertedPayment)
		}
		var convertedAddPayments []AddPaymentOutput
		for _, p := range addPayments {
			convertedAddPayment := AddPaymentOutput{
				ID:             p.ID,
				IDVehicle:      p.IDVehicle,
				Amount:         p.Amount,
				DateAddPayment: p.DateAddPayment,
			}
			convertedAddPayments = append(convertedAddPayments, convertedAddPayment)
		}
		// Tạo đối tượng OrderProduct chứa thông tin của đơn hàng và sản phẩm
		orderProduct := OrderOverOutput{
			Order:      order,
			AddPayment: convertedAddPayments,
			Payments:   convertedPayments,
		}

		result = append(result, orderProduct)
	}

	return result, nil
}
func (c Controller) SearchOrderVehicleOverByID(id int64) ([]OrderOverOutput, error) {
	orders, err := c.orderRepo.SearchOrderOverVehicleByID(id)
	if err != nil {
		return nil, err
	}
	var result []OrderOverOutput
	for _, inp := range orders {
		order := OrderVehicleOutput{
			ID:                int64(inp.ID),
			LicensePlate:      inp.LicensePlate,
			Percent:           inp.Percent,
			ProductName:       null.String(inp.ProductName),
			AddPayments:       inp.AddPayments,
			Price:             null.Int(inp.Price),
			PriceOld:          null.Int(inp.PriceOld),
			OrderDate:         null.Time(inp.OrderDate),
			CustomerID:        inp.CustomerID,
			ReturnDate:        inp.ReturnDate,
			Status:            inp.Status,
			InterestPaidCount: inp.InterestPaidCount,
			Note:              inp.Note,
			Miss:              inp.Miss,
		}

		addPayments, err := c.orderRepo.GetAddPaymentInfo(int64(inp.ID))
		if err != nil {
			return nil, err
		}

		var convertedAddPayments []AddPaymentOutput
		for _, p := range addPayments {
			convertedAddPayment := AddPaymentOutput{
				ID:             p.ID,
				IDVehicle:      p.IDVehicle,
				Amount:         p.Amount,
				DateAddPayment: p.DateAddPayment,
			}
			convertedAddPayments = append(convertedAddPayments, convertedAddPayment)
		}
		// Tạo đối tượng OrderProduct chứa thông tin của đơn hàng và sản phẩm
		orderProduct := OrderOverOutput{
			Order:      order,
			AddPayment: convertedAddPayments,
		}

		result = append(result, orderProduct)
	}

	return result, nil
}
func (c Controller) SearchOrderVehicleOverPaymentByID(id int64) ([]OrderOverOutput, error) {
	orders, err := c.orderRepo.SearchOrderOverPaymentVehicleByID(id)
	if err != nil {
		return nil, err
	}
	var result []OrderOverOutput
	for _, inp := range orders {
		order := OrderVehicleOutput{
			ID:                int64(inp.ID),
			Percent:           inp.Percent,
			LicensePlate:      inp.LicensePlate,
			ProductName:       null.String(inp.ProductName),
			AddPayments:       inp.AddPayments,
			Price:             null.Int(inp.Price),
			PriceOld:          null.Int(inp.PriceOld),
			OrderDate:         null.Time(inp.OrderDate),
			CustomerID:        inp.CustomerID,
			ReturnDate:        inp.ReturnDate,
			Status:            inp.Status,
			InterestPaidCount: inp.InterestPaidCount,
			Note:              inp.Note,
			Miss:              inp.Miss,
		}
		payments, err := c.orderRepo.GetListPaymentByOrderID(int64(inp.ID))
		if err != nil {
			return nil, err
		}
		addPayments, err := c.orderRepo.GetAddPaymentInfo(int64(inp.ID))
		if err != nil {
			return nil, err
		}
		var convertedPayments []InterestPaidOutput
		for _, payment := range payments {
			convertedPayment := InterestPaidOutput{
				OrderID:         null.Int64From(inp.ID),
				Amount:          payment.Amount,
				PaymentDate:     payment.PaymentDate,
				NextPaymentDate: payment.NextPaymentDate,
			}
			convertedPayments = append(convertedPayments, convertedPayment)
		}
		var convertedAddPayments []AddPaymentOutput
		for _, p := range addPayments {
			convertedAddPayment := AddPaymentOutput{
				ID:             p.ID,
				IDVehicle:      p.IDVehicle,
				Amount:         p.Amount,
				DateAddPayment: p.DateAddPayment,
			}
			convertedAddPayments = append(convertedAddPayments, convertedAddPayment)
		}
		// Tạo đối tượng OrderProduct chứa thông tin của đơn hàng và sản phẩm
		orderProduct := OrderOverOutput{
			Order:      order,
			AddPayment: convertedAddPayments,
			Payments:   convertedPayments,
		}

		result = append(result, orderProduct)
	}

	return result, nil
}
