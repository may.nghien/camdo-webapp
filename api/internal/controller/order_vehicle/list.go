package ordervehicle

import (
	models "Golang/internal/repository/dbmodel"

	"github.com/volatiletech/null/v8"
)

type OrderVehicleOutput struct {
	ID                int64
	ProductName       null.String
	AddPayments       null.Int
	Price             null.Int
	PriceOld          null.Int
	OrderDate         null.Time
	CustomerID        null.Int64
	Percent           null.Int64
	LicensePlate      null.String
	ReturnDate        null.Time
	Status            null.String
	InterestPaidCount null.Int
	Note              null.String
	Miss              null.Int
}
type InterestPaidOutput struct {
	OrderID         null.Int64
	Amount          null.Int
	PaymentDate     null.Time
	NextPaymentDate null.Time
}
type AddPaymentOutput struct {
	ID             int64
	IDVehicle      null.Int64
	Amount         null.Int
	DateAddPayment null.Time
}
type OrderProduct struct {
	Order      OrderVehicleOutput   `json:"order"`
	Payments   []InterestPaidOutput `json:"payments"`
	AddPayment []AddPaymentOutput   `json:"add_payments"`
}

func (c Controller) GetOrderVehicle() ([]OrderProduct, error) {
	orders, err := c.orderRepo.GetOrderVehicle()
	if err != nil {
		return nil, err
	}

	var result []OrderProduct
	for _, inp := range orders {
		order := OrderVehicleOutput{
			ID:                int64(inp.ID),
			Percent:           inp.Percent,
			ProductName:       null.String(inp.ProductName),
			AddPayments:       inp.AddPayments,
			Price:             null.Int(inp.Price),
			PriceOld:          inp.PriceOld,
			OrderDate:         null.Time(inp.OrderDate),
			ReturnDate:        null.Time(inp.ReturnDate),
			CustomerID:        inp.CustomerID,
			LicensePlate:      inp.LicensePlate,
			Status:            inp.Status,
			Miss:              inp.Miss,
			InterestPaidCount: inp.InterestPaidCount,
			Note:              inp.Note,
		}
		payments, err := c.orderRepo.GetListPaymentByOrderID(int64(inp.ID))
		if err != nil {
			return nil, err
		}
		addPayments, err := c.orderRepo.GetAddPaymentInfo(int64(inp.ID))
		if err != nil {
			return nil, err
		}
		var convertedPayments []InterestPaidOutput
		for _, payment := range payments {
			convertedPayment := InterestPaidOutput{
				OrderID:         null.Int64From(inp.ID),
				Amount:          payment.Amount,
				PaymentDate:     payment.PaymentDate,
				NextPaymentDate: payment.NextPaymentDate,
			}
			convertedPayments = append(convertedPayments, convertedPayment)
		}
		var convertedAddPayments []AddPaymentOutput
		for _, p := range addPayments {
			convertedAddPayment := AddPaymentOutput{
				ID:             p.ID,
				IDVehicle:      p.IDVehicle,
				Amount:         p.Amount,
				DateAddPayment: p.DateAddPayment,
			}
			convertedAddPayments = append(convertedAddPayments, convertedAddPayment)
		}
		orderProduct := OrderProduct{
			Order:      order,
			Payments:   convertedPayments,
			AddPayment: convertedAddPayments,
		}

		result = append(result, orderProduct)
	}

	return result, nil
}

func (c Controller) ListOrdersVehicleOver() ([]models.OrderVehicle, error) {
	order, err := c.orderRepo.ListOrdersVehicleOver()
	if err != nil {
		return nil, err
	}

	var result = make([]models.OrderVehicle, len(order))
	for ind, m := range order {
		result[ind] = models.OrderVehicle{
			ID:                m.ID,
			ProductName:       null.String(m.ProductName),
			AddPayments:       m.AddPayments,
			Price:             m.Price,
			PriceOld:          m.PriceOld,
			OrderDate:         m.OrderDate,
			CustomerID:        m.CustomerID,
			Percent:           m.Percent,
			LicensePlate:      m.LicensePlate,
			ReturnDate:        m.ReturnDate,
			Status:            m.Status,
			Miss:              m.Miss,
			Note:              m.Note,
			InterestPaidCount: m.InterestPaidCount,
		}
	}
	return result, nil
}

func (c Controller) ListOrdersVehicleOver60d() ([]models.OrderVehicle, error) {
	order, err := c.orderRepo.ListOrdersVehicleOver60d()
	if err != nil {
		return nil, err
	}

	var result = make([]models.OrderVehicle, len(order))
	for ind, m := range order {
		result[ind] = models.OrderVehicle{
			ID:                m.ID,
			ProductName:       null.String(m.ProductName),
			AddPayments:       m.AddPayments,
			Price:             m.Price,
			PriceOld:          m.PriceOld,
			OrderDate:         m.OrderDate,
			CustomerID:        m.CustomerID,
			Percent:           m.Percent,
			LicensePlate:      m.LicensePlate,
			ReturnDate:        m.ReturnDate,
			Status:            m.Status,
			Miss:              m.Miss,
			Note:              m.Note,
			InterestPaidCount: m.InterestPaidCount,
		}
	}
	return result, nil
}

func (c Controller) ListOrdersVehicleUpComming() ([]models.OrderVehicle, error) {
	order, err := c.orderRepo.ListOrdersVehicleUpComming()
	if err != nil {
		return nil, err
	}

	var result = make([]models.OrderVehicle, len(order))
	for ind, m := range order {
		result[ind] = models.OrderVehicle{
			ID:                m.ID,
			ProductName:       null.String(m.ProductName),
			AddPayments:       m.AddPayments,
			Price:             m.Price,
			PriceOld:          m.PriceOld,
			OrderDate:         m.OrderDate,
			CustomerID:        m.CustomerID,
			Percent:           m.Percent,
			LicensePlate:      m.LicensePlate,
			ReturnDate:        m.ReturnDate,
			Status:            m.Status,
			Miss:              m.Miss,
			Note:              m.Note,
			InterestPaidCount: m.InterestPaidCount,
		}
	}
	return result, nil
}

func (c Controller) ListOrdersVehicleDue() ([]models.OrderVehicle, error) {
	order, err := c.orderRepo.ListOrdersVehicleDue()
	if err != nil {
		return nil, err
	}

	var result = make([]models.OrderVehicle, len(order))
	for ind, m := range order {
		result[ind] = models.OrderVehicle{
			ID:                m.ID,
			ProductName:       null.String(m.ProductName),
			AddPayments:       m.AddPayments,
			Price:             m.Price,
			PriceOld:          m.PriceOld,
			OrderDate:         m.OrderDate,
			CustomerID:        m.CustomerID,
			Percent:           m.Percent,
			LicensePlate:      m.LicensePlate,
			ReturnDate:        m.ReturnDate,
			Status:            m.Status,
			Miss:              m.Miss,
			Note:              m.Note,
			InterestPaidCount: m.InterestPaidCount,
		}
	}
	return result, nil
}

func (c Controller) GetOrderVehicleByID(id int64) (models.OrderVehicle, error) {
	order, err := c.orderRepo.GetOrderVehicleByID(id)
	if err != nil {
		return models.OrderVehicle{}, err
	}

	return models.OrderVehicle{ID: order.ID,
		AddPayments:       order.AddPayments,
		Price:             order.Price,
		PriceOld:          order.PriceOld,
		OrderDate:         order.OrderDate,
		Percent:           order.Percent,
		ProductName:       order.ProductName,
		Note:              order.Note,
		CustomerID:        order.CustomerID,
		ReturnDate:        order.ReturnDate,
		LicensePlate:      order.LicensePlate,
		Status:            order.Status,
		Miss:              order.Miss,
		InterestPaidCount: order.InterestPaidCount,
	}, nil
}

func (c Controller) GetOrderVehicleByCustomerID(id int64) ([]models.OrderVehicle, error) {
	order, err := c.orderRepo.GetOrderVehicleByCustomerID(id)
	if err != nil {
		return nil, err
	}
	var result = make([]models.OrderVehicle, len(order))
	for ind, m := range order {
		result[ind] = models.OrderVehicle{
			ID:                m.ID,
			ProductName:       m.ProductName,
			AddPayments:       m.AddPayments,
			Price:             m.Price,
			PriceOld:          m.PriceOld,
			OrderDate:         m.OrderDate,
			CustomerID:        m.CustomerID,
			Percent:           m.Percent,
			ReturnDate:        m.ReturnDate,
			Status:            m.Status,
			LicensePlate:      m.LicensePlate,
			Miss:              m.Miss,
			Note:              m.Note,
			InterestPaidCount: m.InterestPaidCount}
	}
	return result, nil
}

func (c Controller) GetNewestOrderVehicle() (models.OrderVehicle, error) {
	order, err := c.orderRepo.GetNewestOrderVehicle()
	if err != nil {
		return models.OrderVehicle{}, err
	}

	return models.OrderVehicle{
		ID:                order.ID,
		AddPayments:       order.AddPayments,
		Price:             order.Price,
		PriceOld:          order.PriceOld,
		OrderDate:         order.OrderDate,
		Percent:           order.Percent,
		ProductName:       order.ProductName,
		CustomerID:        order.CustomerID,
		ReturnDate:        order.ReturnDate,
		Status:            order.Status,
		Miss:              order.Miss,
		Note:              order.Note,
		LicensePlate:      order.LicensePlate,
		InterestPaidCount: order.InterestPaidCount,
	}, nil

}

// func (c Controller) ListOrdersVehicleDueLateday() ([]models.OrderVehicle, error) {
// 	// dueDate := time.Now().Add(-3 * 24 * time.Hour)

// 	// orders, err := c.orderRepo.ListOrdersVehicleDue(dueDate)
// 	// if err != nil {
// 	// 	return nil, err
// 	// }

// 	// var result = make([]models.OrderVehicle, len(order))
// 	// for ind, m := range order {
// 	// 	result[ind] = models.OrderVehicle{
// 	// 		ID:                m.ID,
// 	// 		ProductName:       null.String(m.ProductName),
// 	// 		EstimatedPrice:    m.EstimatedPrice,
// 	// 		Price:             m.Price,
// 	// 		OrderDate:         m.OrderDate,
// 	// 		CustomerID:        m.CustomerID,
// 	// 		Percent:           m.Percent,
// 	// 		LicensePlate:      m.LicensePlate,
// 	// 		ReturnDate:        m.ReturnDate,
// 	// 		Status:            m.Status,
// 	// 		InterestPaidCount: m.InterestPaidCount,
// 	// 	}
// 	// }
// 	//Prepare response data
// 	// 	type Notification struct {
// 	// 		OrderID   int       `json:"order_id"`
// 	// 		LateDays  int       `json:"late_days"`
// 	// 		OrderDate time.Time `json:"order_date"`
// 	// 		ReturnDate time.Time `json:"return_date"`
// 	// 	}
// 	// notifications := make([]Notification, len(orders))
// 	// for i, order := range orders {
// 	// 	lateDays := int(time.Since(order.ReturnDate.Time).Hours() / 24)
// 	// 	notification := Notification{
// 	// 		OrderID:    int(order.ID),
// 	// 		LateDays:   lateDays,
// 	// 		OrderDate:  order.OrderDate.Time,
// 	// 		ReturnDate: order.ReturnDate.Time,
// 	// 	}
// 	// 	notifications[i] = notification
// 	// }

// 	// return []models.OrderVehicle{}, nil
// }
