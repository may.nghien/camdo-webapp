package ordervehicle

import (
	models "Golang/internal/repository/dbmodel"
	"Golang/pkg/db"

	"github.com/volatiletech/null/v8"
	"Golang/internal/repository/order_vehicle"

)

type RedeemInput struct {
	ID                int64                       `json:"id"`
	Status            null.String                 `json:"status"`
	ModelInput        models.InterestPaymentsVehicle `json:"model_input"`
	CreateRedeemInput models.RedeemOrderVehicle    `json:"create_redeem"`
}

func (c Controller) RedeemOrderVehicle(inp RedeemInput) (models.OrderVehicle, error) {
	tx, err := db.Database.Db.Begin()
	if err != nil {
		return models.OrderVehicle{}, err
	}

	txOrderRepo := ordervehicle.New(tx)
	if inp.ModelInput.Amount.Valid && inp.ModelInput.Amount.Int > 0 {
		_, err = txOrderRepo.CreatePayVehicle(inp.ModelInput)
		if err != nil {
			tx.Rollback()
			return models.OrderVehicle{}, err
		}
	}

	order, err := txOrderRepo.UpdateStatus(inp.ID, inp.Status)
	if err != nil {
		tx.Rollback()
		return models.OrderVehicle{}, err
	}

	_, err = txOrderRepo.CreateRedeemVehicle(inp.CreateRedeemInput)
	if err != nil {
		tx.Rollback()
		return models.OrderVehicle{}, err
	}

	tx.Commit()

	return models.OrderVehicle{
		ID:                order.ID,
		ProductName:       order.ProductName,
		AddPayments:       order.AddPayments,
		Price:             order.Price,
		OrderDate:         order.OrderDate,
		CustomerID:        order.CustomerID,
		Percent:           order.Percent,
		ReturnDate:        order.ReturnDate,
		Status:            order.Status,
		InterestPaidCount: order.InterestPaidCount,
	}, nil

}

func (c Controller) ListRedeemVehicle(orderID int64) (models.RedeemOrderGoal, error) {
	redeemGoal, err := c.orderRepo.ListRedeemVehicle(orderID)
	if err != nil {
		return models.RedeemOrderGoal{}, err
	}

	return models.RedeemOrderGoal{
		ID:         redeemGoal.ID,
		OrderID:    redeemGoal.OrderID,
		Amount:     redeemGoal.Amount,
		DateRedeem: redeemGoal.DateRedeem,
	}, nil
}
