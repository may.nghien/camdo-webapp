package ordervehicle

import "context"

func (c Controller) DeleteOrderVehicle(ctx context.Context, id int64) error {
	err := c.orderRepo.DeleteOrderVehicle(ctx,id)
	if err != nil {
		return err
	}
	return nil
}