package ordervehicle

import (
	models "Golang/internal/repository/dbmodel"
)

type OverviewTodayOutput struct {
	Revenue int64
	Collect int64
	Spend   int64
}

func (c Controller) ListOrderVehicleToday() ([]models.OrderVehicle, error) {
	orderQuery, err := c.orderRepo.ListOrderVehicleToday()
	if err != nil {
		return nil, err
	}
	var orders []models.OrderVehicle
	for _, o := range orderQuery {
		order := models.OrderVehicle{
			ID:                o.ID,
			CustomerID:        o.CustomerID,
			LicensePlate:      o.LicensePlate,
			Price:             o.Price,
			PriceOld:          o.PriceOld,
			ProductName:       o.ProductName,
			AddPayments:       o.AddPayments,
			Percent:           o.Percent,
			ReturnDate:        o.ReturnDate,
			Status:            o.Status,
			OrderDate:         o.OrderDate,
			InterestPaidCount: o.InterestPaidCount,
		}
		orders = append(orders, order)
	}

	return orders, nil
}

func (c Controller) ListPaymentToday() ([]RevenueOverviewOutput, error) {
	orderQuery, err := c.orderRepo.GetListPaymentsVehicleToday()
	if err != nil {
		return nil, err
	}
	var result []RevenueOverviewOutput
	for _, inp := range orderQuery {
		payments := models.InterestPaymentsVehicle{
			PaymentID:       inp.PaymentID,
			OrderID:         inp.OrderID,
			PaymentDate:     inp.PaymentDate,
			NextPaymentDate: inp.NextPaymentDate,
			Amount:          inp.Amount,
		}
		order, err := c.orderRepo.GetOrderVehicleByID(inp.OrderID.Int64)
		if err != nil {
			return nil, err
		}
		convertedOrder := models.OrderVehicle{
			ID:                order.ID,
			ProductName:       order.ProductName,
			AddPayments:       order.AddPayments,
			Price:             order.Price,
			PriceOld:          order.PriceOld,
			OrderDate:         order.OrderDate,
			CustomerID:        order.CustomerID,
			Percent:           order.Percent,
			LicensePlate:      order.LicensePlate,
			ReturnDate:        order.ReturnDate,
			Status:            order.Status,
			InterestPaidCount: order.InterestPaidCount,
			Note:              order.Note,
		}

		paymentOrder := RevenueOverviewOutput{
			Order:    convertedOrder,
			Payments: payments,
		}
		result = append(result, paymentOrder)

	}
	return result, nil
}
func (c Controller) ListRedeemToday() ([]RedeemOverviewOutput, error) {
	orderQuery, err := c.orderRepo.GetListRedeemVehicleToday()
	if err != nil {
		return nil, err
	}

	var result []RedeemOverviewOutput
	for _, inp := range orderQuery {
		payments := models.RedeemOrderVehicle{
			ID:         inp.ID,
			DateRedeem: inp.DateRedeem,
			OrderID:    inp.OrderID,
			Amount:     inp.Amount,
		}
		order, err := c.orderRepo.GetOrderVehicleByID(inp.OrderID.Int64)
		if err != nil {
			return nil, err
		}
		convertedOrder := models.OrderVehicle{
			ID:                order.ID,
			ProductName:       order.ProductName,
			AddPayments:       order.AddPayments,
			Price:             order.Price,
			PriceOld:          order.PriceOld,
			OrderDate:         order.OrderDate,
			CustomerID:        order.CustomerID,
			Percent:           order.Percent,
			LicensePlate:      order.LicensePlate,
			ReturnDate:        order.ReturnDate,
			Status:            order.Status,
			InterestPaidCount: order.InterestPaidCount,
			Note:              order.Note,
		}

		paymentOrder := RedeemOverviewOutput{
			Order:  convertedOrder,
			Redeem: payments,
		}
		result = append(result, paymentOrder)

	}
	return result, nil
}

func (c Controller) ListAddPaymentToday() ([]AddPaymentOverviewOutput, error) {
	orderQuery, err := c.orderRepo.GetListAddPaymentVehicleToday()
	if err != nil {
		return nil, err
	}

	var result []AddPaymentOverviewOutput
	for _, addPayment := range orderQuery {
		addPaymentCoverted := models.AddPaymentsVehicle{
			ID:             addPayment.ID,
			IDVehicle:      addPayment.IDVehicle,
			Amount:         addPayment.Amount,
			DateAddPayment: addPayment.DateAddPayment,
		}
		order, err := c.orderRepo.GetOrderVehicleByID(addPayment.IDVehicle.Int64)
		if err != nil {
			return nil, err
		}
		convertedOrder := models.OrderVehicle{
			ID:                order.ID,
			ProductName:       order.ProductName,
			AddPayments:       order.AddPayments,
			Price:             order.Price,
			PriceOld:          order.PriceOld,
			OrderDate:         order.OrderDate,
			CustomerID:        order.CustomerID,
			Percent:           order.Percent,
			LicensePlate:      order.LicensePlate,
			ReturnDate:        order.ReturnDate,
			Status:            order.Status,
			InterestPaidCount: order.InterestPaidCount,
			Note:              order.Note,
		}
		paymentOrder := AddPaymentOverviewOutput{
			AddPayment: addPaymentCoverted,
			Order:      convertedOrder,
		}
		result = append(result, paymentOrder)

	}
	return result, nil
}

func (c Controller) GetOverviewToday() (OverviewTodayOutput, error) {
	amountSpend := 0
	amountCollect := 0
	amountRevenue := 0
	//Chi
	orders, err := c.orderRepo.ListOrderVehicleToday()
	if err != nil {
		return OverviewTodayOutput{}, err
	}
	for _, order := range orders {
		amountSpend += order.PriceOld.Int
	}

	addPayments, err := c.orderRepo.GetListAddPaymentVehicleToday()
	if err != nil {
		return OverviewTodayOutput{}, err
	}
	for _, addPayment := range addPayments {
		amountSpend += addPayment.Amount.Int
	}
	//Thu
	payments, err := c.orderRepo.GetListPaymentsVehicleToday()
	if err != nil {
		return OverviewTodayOutput{}, err
	}
	for _, payment := range payments {
		//Loi nhuan
		amountRevenue += payment.Amount.Int
		//Thu
		amountCollect += payment.Amount.Int
	}
	redeems, err := c.orderRepo.GetListRedeemVehicleToday()
	if err != nil {
		return OverviewTodayOutput{}, err
	}
	for _, redeem := range redeems {
		amountCollect += redeem.Amount.Int
	}
	return OverviewTodayOutput{
		Revenue: int64(amountRevenue),
		Collect: int64(amountCollect),
		Spend:   int64(amountSpend),
	}, nil
}
