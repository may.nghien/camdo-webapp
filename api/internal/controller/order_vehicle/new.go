package ordervehicle

import "Golang/internal/repository/order_vehicle"

type Controller struct {
	orderRepo ordervehicle.Repository
}

func New(orderRepo ordervehicle.Repository) Controller {
	return Controller{orderRepo: orderRepo}
}
