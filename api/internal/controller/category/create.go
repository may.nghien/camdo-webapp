package category

import (
	models "Golang/internal/repository/dbmodel"

	"github.com/volatiletech/null/v8"
)

type CategoryInput struct {
	CategoryName null.String
}

type CategoryPercentInput struct {
	Percent null.Float64
}

func (c Controller) CreateCategory(inp CategoryInput) (models.Category, error) {
	return c.categoryRepo.CreateCategory(models.Category{
		CategoryName: inp.CategoryName,
	})
}

func (c Controller) CreateCategoryGoal(inp CategoryInput) (models.CategoryGoal, error) {
	return c.categoryRepo.CreateCategoryGoal(models.CategoryGoal{
		CategoryName: inp.CategoryName,
	})
}

func (c Controller) CreateCategoryPercent(inp CategoryPercentInput) (models.CategoryPercent, error) {
	return c.categoryRepo.CreateCategoryPercent(models.CategoryPercent{
		Percent: inp.Percent,
	})
}
