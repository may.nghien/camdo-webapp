package auth

import models "Golang/internal/repository/dbmodel"

func (c Controller) UpdateOTP(otp string) (models.Account, error) {
	account, err := c.accountRepo.UpdateOTP(otp)
	if err != nil {
		return models.Account{}, err
	}
	return models.Account{
		Username: account.Username,
	}, nil
}
 
func (c Controller) GetOTP(otp string) (string, error) {
	account, err := c.accountRepo.GetOTP(otp)
	if err != nil {
		return "Lỗi khi tìm tài khoản", err
	}
	return account, nil
}
func (c Controller) UpdateOTPAfterResetPassword() (models.Account, error) {
	account, err := c.accountRepo.UpdateOTPAfterResetPassword()
	if err != nil {
		return models.Account{}, err
	}
	return models.Account{
		Username: account.Username,
	}, nil
}