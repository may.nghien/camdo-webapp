package auth

import (
	"Golang/internal/repository/account"
	"github.com/dgrijalva/jwt-go"
	"database/sql"
	"errors"
	"time"
)

type Controller struct {
	accountRepo account.Repository
}

func New(accountRepo account.Repository) Controller {
	return Controller{accountRepo: accountRepo}
}

// Request body for sign in endpoint
type SignInInput struct {
	Username string
	Password string
}
type SignInOutput struct {
	Token string
	Username string
}

const secretKey = "secret"

func (c Controller) SignIn(inp SignInInput) (SignInOutput, error) {
	user, err := c.accountRepo.GetByUserName(inp.Username)
	if err != nil {
		if err == sql.ErrNoRows {
			return SignInOutput{}, errors.New("User not found!")
		}
		return SignInOutput{}, err
	}

	if user.Password != inp.Password {
		return SignInOutput{}, errors.New("Password is incorrect!")
	}

	// Create JWT token
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"username": user.Username,
		"exp":      time.Now().Add(time.Hour * 24).Unix(),
	})
	tokenString, err := token.SignedString([]byte(secretKey))
	if err != nil {
		return SignInOutput{}, err
	}

	return SignInOutput{Token: tokenString, Username: user.Username}, nil
}
