package order_goal

import (
	models "Golang/internal/repository/dbmodel"

	"github.com/volatiletech/null/v8"
)

type OutputOverView struct {
	AmountRevenue int64
	AmountCollect int64
	AmountSpend   int64
}

type Data struct {
	Thang    int
	Nam      int
	LoiNhuan int64
	Thu      int64
	Chi      int64
}
type PaymentOutput struct {
	PaymentID       int64
	OrderID         null.Int64
	PaymentDate     null.Time
	NextPaymentDate null.Time
	Amount          null.Int
}

type RevenueOverviewOutput struct {
	Order    OrderGoalOutput             `json:"order"`
	Payments models.InterestPaymentsGoal `json:"payments"`
	Products []ProductsOutput            `json:"products"`
}
type RedeemOverviewOutput struct {
	Order    OrderGoalOutput        `json:"order"`
	Redeem   models.RedeemOrderGoal `json:"redeem"`
	Products []ProductsOutput       `json:"products"`
}
type OrderOverviewOutput struct {
	Order    OrderGoalOutput  `json:"order"`
	Products []ProductsOutput `json:"products"`
}
type AddPaymentOverviewOutput struct {
	AddPayment models.AddPaymentsGoal `json:"add_payment"`
	Products   []ProductsOutput       `json:"products"`
}

func (c Controller) ExportOverView(month int, year int) (Data, error) {
	amountSpend, err := c.orderRepo.GetSpendGoal(month, year)
	if err != nil {
		amountSpend = 0
	}

	amountRevenue, err := c.orderRepo.GetRevenueGoal(month, year)
	if err != nil {
		amountRevenue = 0
	}

	amountCollect, err := c.orderRepo.GetCollectGoal(month, year)
	if err != nil {
		amountCollect = 0
	}
	return Data{
		Thang:    month,
		Nam:      year,
		LoiNhuan: amountRevenue,
		Thu:      amountCollect,
		Chi:      amountSpend,
	}, nil
}

func (c Controller) GetOverView(month int, year int) (OutputOverView, error) {
	amountSpend, err := c.orderRepo.GetSpendGoal(month, year)
	if err != nil {
		amountSpend = 0
	}

	amountRevenue, err := c.orderRepo.GetRevenueGoal(month, year)
	if err != nil {
		amountRevenue = 0
	}

	amountCollect, err := c.orderRepo.GetCollectGoal(month, year)
	if err != nil {
		amountCollect = 0
	}

	return OutputOverView{
		AmountRevenue: amountRevenue,
		AmountCollect: amountCollect,
		AmountSpend:   amountSpend,
	}, nil
}

func (c Controller) ListOrderGoalByMonthAndYear(month int, year int) ([]OrderOverviewOutput, error) {
	orderQuery, err := c.orderRepo.ListOrderGoalByMonthAndYear(month, year)
	if err != nil {
		return nil, err
	}

	var result []OrderOverviewOutput
	for _, order := range orderQuery {
		convertedOrder := OrderGoalOutput{
			ID:                order.ID,
			ProductName:       order.ProductName,
			AddPayments:       order.AddPayments,
			Price:             order.Price,
			PriceOld:          order.PriceOld,
			OrderDate:         order.OrderDate,
			CustomerID:        order.CustomerID,
			Percent:           order.Percent,
			ReturnDate:        order.ReturnDate,
			Status:            order.Status,
			InterestPaidCount: order.InterestPaidCount,
			Note:              order.Note,
		}
		var details []ProductsOutput
		products, err := c.orderRepo.GetListProductByIDOrder(int64(order.ID))
		if err != nil {
			return nil, err
		}

		for _, o := range products {
			res, err := c.orderRepo.GetListProductDetail(int64(o.ID))
			if err != nil {
				return nil, err
			}
			var convertedDetails []ProductDetailOutput
			for _, d := range res {
				convertedDetail := ProductDetailOutput{
					ID:           d.ID,
					CategoryID:   d.CategoryID,
					CategoryName: d.CategoryName,
					Count:        d.Count,
				}
				convertedDetails = append(convertedDetails, convertedDetail)
			}
			out := ProductsOutput{
				Product: ProductGoldOutput{
					ID:               o.ID,
					WeightGoal:       o.WeightGoal,
					CategoryGoalID:   o.CategoryGoalID,
					CategoryGoldName: o.CategoryGoldName,
				},
				DetailProducts: convertedDetails,
			}
			details = append(details, out)
		}
		paymentOrder := OrderOverviewOutput{
			Order:    convertedOrder,
			Products: details,
		}
		result = append(result, paymentOrder)

	}
	return result, nil

}

func (c Controller) ListAddPaymentByMonthAndYear(month int, year int) ([]AddPaymentOverviewOutput, error) {
	orderQuery, err := c.orderRepo.ListAddPaymentByMonthAndYear(month, year)
	if err != nil {
		return nil, err
	}

	var result []AddPaymentOverviewOutput
	for _, order := range orderQuery {
		convertedOrder := models.AddPaymentsGoal{
			ID:             order.ID,
			IDGold:         order.IDGold,
			Amount:         order.Amount,
			DateAddPayment: order.DateAddPayment,
		}
		var details []ProductsOutput
		products, err := c.orderRepo.GetListProductByIDOrder(int64(order.IDGold.Int64))
		if err != nil {
			return nil, err
		}

		for _, o := range products {
			res, err := c.orderRepo.GetListProductDetail(int64(o.ID))
			if err != nil {
				return nil, err
			}
			var convertedDetails []ProductDetailOutput
			for _, d := range res {
				convertedDetail := ProductDetailOutput{
					ID:           d.ID,
					CategoryID:   d.CategoryID,
					CategoryName: d.CategoryName,
					Count:        d.Count,
				}
				convertedDetails = append(convertedDetails, convertedDetail)
			}
			out := ProductsOutput{
				Product: ProductGoldOutput{
					ID:               o.ID,
					WeightGoal:       o.WeightGoal,
					CategoryGoalID:   o.CategoryGoalID,
					CategoryGoldName: o.CategoryGoldName,
				},
				DetailProducts: convertedDetails,
			}
			details = append(details, out)
		}
		paymentOrder := AddPaymentOverviewOutput{
			AddPayment: convertedOrder,
			Products:   details,
		}
		result = append(result, paymentOrder)

	}
	return result, nil

}

func (c Controller) ListPaymentByMonthAndYear(month int, year int) ([]RevenueOverviewOutput, error) {
	paymentQuery, err := c.orderRepo.GetListPaymentsGoalByMonthAndYear(month, year)
	if err != nil {
		return nil, err
	}
	var result []RevenueOverviewOutput
	for _, inp := range paymentQuery {
		payments := models.InterestPaymentsGoal{
			PaymentID:       inp.PaymentID,
			OrderID:         inp.OrderID,
			PaymentDate:     inp.PaymentDate,
			NextPaymentDate: inp.NextPaymentDate,
			Amount:          inp.Amount,
		}
		order, err := c.orderRepo.GetOrderGoalByID(inp.OrderID.Int64)
		if err != nil {
			return nil, err
		}
		convertedOrder := OrderGoalOutput{
			ID:                order.ID,
			ProductName:       order.ProductName,
			Price:             order.Price,
			PriceOld:          order.PriceOld,
			OrderDate:         order.OrderDate,
			CustomerID:        order.CustomerID,
			Percent:           order.Percent,
			AddPayments:       order.AddPayments,
			ReturnDate:        order.ReturnDate,
			Status:            order.Status,
			InterestPaidCount: order.InterestPaidCount,
			Note:              order.Note,
		}
		var details []ProductsOutput
		products, err := c.orderRepo.GetListProductByIDOrder(int64(order.ID))
		if err != nil {
			return nil, err
		}

		for _, o := range products {
			res, err := c.orderRepo.GetListProductDetail(int64(o.ID))
			if err != nil {
				return nil, err
			}
			var convertedDetails []ProductDetailOutput
			for _, d := range res {
				convertedDetail := ProductDetailOutput{
					ID:           d.ID,
					CategoryID:   d.CategoryID,
					CategoryName: d.CategoryName,
					Count:        d.Count,
				}
				convertedDetails = append(convertedDetails, convertedDetail)
			}
			out := ProductsOutput{
				Product: ProductGoldOutput{
					ID:               o.ID,
					WeightGoal:       o.WeightGoal,
					CategoryGoalID:   o.CategoryGoalID,
					CategoryGoldName: o.CategoryGoldName,
				},
				DetailProducts: convertedDetails,
			}
			details = append(details, out)
		}
		paymentOrder := RevenueOverviewOutput{
			Order:    convertedOrder,
			Payments: payments,
			Products: details,
		}
		result = append(result, paymentOrder)

	}
	return result, nil
}

func (c Controller) ListRedeemByMonthAndYear(month int, year int) ([]RedeemOverviewOutput, error) {
	orderQuery, err := c.orderRepo.GetListRedeemGoalByMonthAndYear(month, year)
	if err != nil {
		return nil, err
	}

	var result []RedeemOverviewOutput
	for _, inp := range orderQuery {
		payments := models.RedeemOrderGoal{
			ID:         inp.ID,
			DateRedeem: inp.DateRedeem,
			OrderID:    inp.OrderID,
			Amount:     inp.Amount,
		}
		order, err := c.orderRepo.GetOrderGoalByID(inp.OrderID.Int64)
		if err != nil {
			return nil, err
		}
		convertedOrder := OrderGoalOutput{
			ID:                order.ID,
			ProductName:       order.ProductName,
			Price:             order.Price,
			PriceOld:          order.PriceOld,
			OrderDate:         order.OrderDate,
			CustomerID:        order.CustomerID,
			Percent:           order.Percent,
			AddPayments:       order.AddPayments,
			ReturnDate:        order.ReturnDate,
			Status:            order.Status,
			InterestPaidCount: order.InterestPaidCount,
			Note:              order.Note,
		}
		var details []ProductsOutput
		products, err := c.orderRepo.GetListProductByIDOrder(int64(inp.OrderID.Int64))
		if err != nil {
			return nil, err
		}

		for _, o := range products {
			res, err := c.orderRepo.GetListProductDetail(int64(o.ID))
			if err != nil {
				return nil, err
			}
			var convertedDetails []ProductDetailOutput
			for _, d := range res {
				convertedDetail := ProductDetailOutput{
					ID:           d.ID,
					CategoryID:   d.CategoryID,
					CategoryName: d.CategoryName,
					Count:        d.Count,
				}
				convertedDetails = append(convertedDetails, convertedDetail)
			}
			out := ProductsOutput{
				Product: ProductGoldOutput{
					ID:               o.ID,
					WeightGoal:       o.WeightGoal,
					CategoryGoalID:   o.CategoryGoalID,
					CategoryGoldName: o.CategoryGoldName,
				},
				DetailProducts: convertedDetails,
			}
			details = append(details, out)
		}
		paymentOrder := RedeemOverviewOutput{
			Order:    convertedOrder,
			Redeem:   payments,
			Products: details,
		}
		result = append(result, paymentOrder)

	}
	return result, nil
}
