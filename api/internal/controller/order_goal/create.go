package order_goal

import (
	models "Golang/internal/repository/dbmodel"
	"Golang/internal/repository/order_goal"
	"Golang/pkg/db"

	"github.com/volatiletech/null/v8"
)

type OrderInput struct {
	ProductName       null.String
	AddPayments       null.Int
	Price             null.Int
	OrderDate         null.Time
	CustomerID        null.Int64
	Percent           null.Int64
	ReturnDate        null.Time
	Status            null.String
	InterestPaidCount null.Int
	Note              null.String
}

type ProductsStruct struct {
	Product        models.ProductGold         `json:"product"`
	DetailProducts []models.DetailProductGold `json:"detail_products"`
}

func (c Controller) CreateOrder(inp models.OrderGoal, products []ProductsStruct) (models.OrderGoal, error) {
	tx, err := db.Database.Db.Begin()
	if err != nil {
		return models.OrderGoal{}, err
	}
	txOrderRepo := order_goal.New(tx)

	createdOrder, err := txOrderRepo.CreateOrder(models.OrderGoal{
		ProductName:       inp.ProductName,
		AddPayments:    inp.AddPayments,
		Price:             inp.Price,
		OrderDate:         inp.OrderDate,
		CustomerID:        inp.CustomerID,
		Percent:           inp.Percent,
		ReturnDate:        inp.ReturnDate,
		Status:            inp.Status,
		InterestPaidCount: inp.InterestPaidCount,
		Note:              inp.Note,
	})
	if err != nil {
		tx.Rollback()
		return models.OrderGoal{}, err
	}

	// Tạo các loại vàng và liên kết loại hàng liên quan
	for _, product := range products {
		pro, err := txOrderRepo.CreateProduct(int64(createdOrder.ID), product.Product)
		if err != nil {
			tx.Rollback()
			return models.OrderGoal{}, err
		}

		for _, p := range product.DetailProducts {
			_, err := txOrderRepo.CreateProductDetail(int64(pro.ID), p)
			if err != nil {
				tx.Rollback()
				return models.OrderGoal{}, err
			}
		}
	}
	tx.Commit()

	return createdOrder, nil
}
