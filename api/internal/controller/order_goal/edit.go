package order_goal

import (
	models "Golang/internal/repository/dbmodel"

	"github.com/volatiletech/null/v8"
)

type OrderInputEdit struct {
	ProductName       null.String
	AddPayments    null.Int
	Price             null.Int
	WeightGoal        null.Float64
	OrderDate         null.Time
	CustomerID        null.Int64
	Percent           null.Int64
	ReturnDate        null.Time
	Status            null.String
	InterestPaidCount null.Int
}


func (c Controller) EditOrder(orderID int64, update models.OrderGoal) (models.OrderGoal, error) {
	// Gọi hàm EditOrder từ repository để xử lý cập nhật order
	updatedOrder, err := c.orderRepo.EditOrder(orderID, update)
	if err != nil {
		return models.OrderGoal{}, err
	}

	return updatedOrder, nil
}

func (c Controller) UpdateNote(note null.String,miss null.Int,orderID int64) (models.OrderGoal, error){
	updatedOrder, err := c.orderRepo.UpdateNote(note,miss, orderID)
	if err != nil {
		return models.OrderGoal{}, err
	}

	return updatedOrder, nil
}