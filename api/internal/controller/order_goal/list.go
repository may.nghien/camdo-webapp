package order_goal

import (
	models "Golang/internal/repository/dbmodel"

	"github.com/volatiletech/null/v8"
)

type OrderGoalOutput struct {
	ID                int64
	ProductName       null.String
	AddPayments       null.Int
	Price             null.Int
	PriceOld          null.Int
	OrderDate         null.Time
	CustomerID        null.Int64
	Percent           null.Int64
	ReturnDate        null.Time
	Status            null.String
	InterestPaidCount null.Int
	Note              null.String
	Miss              null.Int
}

type ProductGoldOutput struct {
	IDGold           int64
	ID               int64
	CategoryGoalID   null.Int64
	CategoryGoldName null.String
	WeightGoal       null.Float64
}
type ProductDetailOutput struct {
	ID           int64
	CategoryID   null.Int64
	CategoryName null.String
	Count        null.Int
}
type ProductsOutput struct {
	Product        ProductGoldOutput
	DetailProducts []ProductDetailOutput
}
type InterestPaidOutput struct {
	OrderID         null.Int64
	Amount          null.Int
	PaymentDate     null.Time
	NextPaymentDate null.Time
}
type AddPaymentOutput struct {
	ID             int64
	IDGold         null.Int64
	Amount         null.Int
	DateAddPayment null.Time
}

//	type OrderProduct struct {
//		Order      OrderGoalOutput      `json:"order"`
//		Products   []ProductGoldOutput  `json:"products"`
//		Payments   []InterestPaidOutput `json:"payments"`
//		AddPayment []AddPaymentOutput   `json:"add_payments"`
//	}
type OrderProduct struct {
	Order      OrderGoalOutput      `json:"order"`
	Products   []ProductsOutput     `json:"products"`
	Payments   []InterestPaidOutput `json:"payments"`
	AddPayment []AddPaymentOutput   `json:"add_payments"`
}
type OrderOverProduct struct {
	Order    OrderGoalOutput  `json:"order"`
	Products []ProductsOutput `json:"products"`
}

func (c Controller) GetOrderGoal() ([]OrderGoalOutput, error) {
	orders, err := c.orderRepo.GetOrderGoal()
	if err != nil {
		return nil, err
	}

	var result = make([]OrderGoalOutput, len(orders))
	for ind, inp := range orders {
		result[ind] = OrderGoalOutput{
			ID:                int64(inp.ID),
			PriceOld:          inp.PriceOld,
			Percent:           inp.Percent,
			ProductName:       null.String(inp.ProductName),
			AddPayments:       inp.AddPayments,
			Price:             null.Int(inp.Price),
			OrderDate:         null.Time(inp.OrderDate),
			CustomerID:        inp.CustomerID,
			ReturnDate:        inp.ReturnDate,
			Status:            inp.Status,
			InterestPaidCount: inp.InterestPaidCount,
			Note:              inp.Note,
			Miss:              null.Int(inp.Miss),
		}
	}

	return result, nil
}

func (c Controller) GetOrderGoalByCustomerID(id int64) ([]OrderOverProduct, error) {
	orders, err := c.orderRepo.GetOrderGoalByCustomerID(id)
	if err != nil {
		return nil, err
	}
	var result []OrderOverProduct
	for _, inp := range orders {
		order := OrderGoalOutput{
			ID:                int64(inp.ID),
			Percent:           inp.Percent,
			ProductName:       null.String(inp.ProductName),
			AddPayments:       inp.AddPayments,
			Price:             null.Int(inp.Price),
			PriceOld:          null.Int(inp.PriceOld),
			OrderDate:         null.Time(inp.OrderDate),
			CustomerID:        inp.CustomerID,
			ReturnDate:        inp.ReturnDate,
			Status:            inp.Status,
			InterestPaidCount: inp.InterestPaidCount,
			Note:              inp.Note,
			Miss:              inp.Miss,
		}
		var details []ProductsOutput
		products, err := c.orderRepo.GetListProductByIDOrder(int64(inp.ID))
		if err != nil {
			return nil, err
		}

		for _, o := range products {
			res, err := c.orderRepo.GetListProductDetail(int64(o.ID))
			if err != nil {
				return nil, err
			}
			var convertedDetails []ProductDetailOutput
			for _, d := range res {
				convertedDetail := ProductDetailOutput{
					ID:           d.ID,
					CategoryID:   d.CategoryID,
					CategoryName: d.CategoryName,
					Count:        d.Count,
				}
				convertedDetails = append(convertedDetails, convertedDetail)
			}
			out := ProductsOutput{
				Product: ProductGoldOutput{
					ID:               o.ID,
					WeightGoal:       o.WeightGoal,
					CategoryGoalID:   o.CategoryGoalID,
					CategoryGoldName: o.CategoryGoldName,
				},
				DetailProducts: convertedDetails,
			}
			details = append(details, out)
		}

		// Tạo đối tượng OrderProduct chứa thông tin của đơn hàng và sản phẩm
		orderProduct := OrderOverProduct{
			Order:    order,
			Products: details,
		}

		result = append(result, orderProduct)
	}

	return result, nil
}

func (c Controller) ListOrderGoalByPaymentOver() (models.OrderGoalSlice, models.InterestPaymentsGoalSlice, error) {
	orderGoal, overduePayments, err := c.orderRepo.ListOrderGoalByPaymentOver()
	if err != nil {
		return models.OrderGoalSlice{}, models.InterestPaymentsGoalSlice{}, err
	}
	return orderGoal, overduePayments, nil
}
func (c Controller) ListOrdersGoalOver() ([]OrderOverProduct, error) {
	orders, err := c.orderRepo.ListOrdersGoalOver()
	if err != nil {
		return nil, err
	}
	var result []OrderOverProduct
	for _, inp := range orders {
		order := OrderGoalOutput{
			ID:                int64(inp.ID),
			AddPayments:       inp.AddPayments,
			Percent:           inp.Percent,
			ProductName:       null.String(inp.ProductName),
			Price:             null.Int(inp.Price),
			PriceOld:          null.Int(inp.PriceOld),
			OrderDate:         null.Time(inp.OrderDate),
			CustomerID:        inp.CustomerID,
			ReturnDate:        inp.ReturnDate,
			Status:            inp.Status,
			InterestPaidCount: inp.InterestPaidCount,
			Note:              inp.Note,
			Miss:              inp.Miss,
		}
		var details []ProductsOutput
		products, err := c.orderRepo.GetListProductByIDOrder(int64(inp.ID))
		if err != nil {
			return nil, err
		}

		for _, o := range products {
			res, err := c.orderRepo.GetListProductDetail(int64(o.ID))
			if err != nil {
				return nil, err
			}
			var convertedDetails []ProductDetailOutput
			for _, d := range res {
				convertedDetail := ProductDetailOutput{
					ID:           d.ID,
					CategoryID:   d.CategoryID,
					CategoryName: d.CategoryName,
					Count:        d.Count,
				}
				convertedDetails = append(convertedDetails, convertedDetail)
			}
			out := ProductsOutput{
				Product: ProductGoldOutput{
					ID:               o.ID,
					WeightGoal:       o.WeightGoal,
					CategoryGoalID:   o.CategoryGoalID,
					CategoryGoldName: o.CategoryGoldName,
				},
				DetailProducts: convertedDetails,
			}
			details = append(details, out)
		}

		// Tạo đối tượng OrderProduct chứa thông tin của đơn hàng và sản phẩm
		orderProduct := OrderOverProduct{
			Order:    order,
			Products: details,
		}

		result = append(result, orderProduct)
	}

	return result, nil
}

func (c Controller) GetProductListByOrderID(id int64) ([]ProductsOutput, error) {
	var details []ProductsOutput
	products, err := c.orderRepo.GetListProductByIDOrder(int64(id))
	if err != nil {
		return nil, err
	}

	for _, o := range products {
		res, err := c.orderRepo.GetListProductDetail(int64(o.ID))
		if err != nil {
			return nil, err
		}
		var convertedDetails []ProductDetailOutput
		for _, d := range res {
			convertedDetail := ProductDetailOutput{
				ID:           d.ID,
				CategoryID:   d.CategoryID,
				CategoryName: d.CategoryName,
				Count:        d.Count,
			}
			convertedDetails = append(convertedDetails, convertedDetail)
		}
		out := ProductsOutput{
			Product: ProductGoldOutput{
				ID:               o.ID,
				WeightGoal:       o.WeightGoal,
				CategoryGoalID:   o.CategoryGoalID,
				CategoryGoldName: o.CategoryGoldName,
			},
			DetailProducts: convertedDetails,
		}
		details = append(details, out)
	}

	// var result = make([]ProductGoldOutput, len(products))
	// for ind, m := range products {
	// 	result[ind] = ProductGoldOutput{
	// 		IDGold:           id,
	// 		ID:               m.ID,
	// 		CategoryGoalID:   m.CategoryGoalID,
	// 		CategoryGoldName: m.CategoryGoldName,
	// 	}
	// }
	return details, nil
}

func (c Controller) GetOrderGoalAndProducts() ([]OrderProduct, error) {
	orders, err := c.orderRepo.GetOrderGoal()
	if err != nil {
		return nil, err
	}

	var result []OrderProduct
	for _, inp := range orders {
		order := OrderGoalOutput{
			ID:                int64(inp.ID),
			AddPayments:       inp.AddPayments,
			Percent:           inp.Percent,
			ProductName:       null.String(inp.ProductName),
			Price:             null.Int(inp.Price),
			PriceOld:          null.Int(inp.PriceOld),
			OrderDate:         null.Time(inp.OrderDate),
			CustomerID:        inp.CustomerID,
			ReturnDate:        inp.ReturnDate,
			Status:            inp.Status,
			InterestPaidCount: inp.InterestPaidCount,
			Note:              inp.Note,
			Miss:              inp.Miss,
		}
		// Lấy danh sách sản phẩm tương ứng với đơn hàng
		var details []ProductsOutput
		products, err := c.orderRepo.GetListProductByIDOrder(int64(inp.ID))
		if err != nil {
			return nil, err
		}

		for _, o := range products {
			res, err := c.orderRepo.GetListProductDetail(int64(o.ID))
			if err != nil {
				return nil, err
			}
			var convertedDetails []ProductDetailOutput
			for _, d := range res {
				convertedDetail := ProductDetailOutput{
					ID:           d.ID,
					CategoryID:   d.CategoryID,
					CategoryName: d.CategoryName,
					Count:        d.Count,
				}
				convertedDetails = append(convertedDetails, convertedDetail)
			}
			out := ProductsOutput{
				Product: ProductGoldOutput{
					ID:               o.ID,
					WeightGoal:       o.WeightGoal,
					CategoryGoalID:   o.CategoryGoalID,
					CategoryGoldName: o.CategoryGoldName,
				},
				DetailProducts: convertedDetails,
			}
			details = append(details, out)
		}
		payments, err := c.orderRepo.GetListPaymentByOrderID(int64(inp.ID))
		if err != nil {
			return nil, err
		}
		addPayments, err := c.orderRepo.GetAddPaymentInfo(int64(inp.ID))
		if err != nil {
			return nil, err
		}
		// Convert the products to the appropriate type
		var convertedProducts []ProductGoldOutput
		for _, product := range products {
			convertedProduct := ProductGoldOutput{
				ID:               product.ID,
				CategoryGoalID:   product.CategoryGoalID,
				CategoryGoldName: product.CategoryGoldName,
			}
			convertedProducts = append(convertedProducts, convertedProduct)
		}
		var convertedPayments []InterestPaidOutput
		for _, payment := range payments {
			convertedPayment := InterestPaidOutput{
				OrderID:         null.Int64From(inp.ID),
				Amount:          payment.Amount,
				PaymentDate:     payment.PaymentDate,
				NextPaymentDate: payment.NextPaymentDate,
			}
			convertedPayments = append(convertedPayments, convertedPayment)
		}
		var convertedAddPayments []AddPaymentOutput
		for _, p := range addPayments {
			convertedAddPayment := AddPaymentOutput{
				ID:             p.ID,
				IDGold:         p.IDGold,
				Amount:         p.Amount,
				DateAddPayment: p.DateAddPayment,
			}
			convertedAddPayments = append(convertedAddPayments, convertedAddPayment)
		}
		// Tạo đối tượng OrderProduct chứa thông tin của đơn hàng và sản phẩm
		orderProduct := OrderProduct{
			Order:      order,
			Products:   details,
			Payments:   convertedPayments,
			AddPayment: convertedAddPayments,
		}

		result = append(result, orderProduct)
	}

	return result, nil
}

func (c Controller) GetNewestOrderGoal() (OrderProduct, error) {
	order, err := c.orderRepo.GetNewestOrderGoal()
	if err != nil {
		return OrderProduct{}, err
	}
	// Lấy danh sách sản phẩm tương ứng với đơn hàng
	var details []ProductsOutput
	products, err := c.orderRepo.GetListProductByIDOrder(int64(order.ID))
	if err != nil {
		return OrderProduct{}, err
	}

	for _, o := range products {
		res, err := c.orderRepo.GetListProductDetail(int64(o.ID))
		if err != nil {
			return OrderProduct{}, err
		}
		var convertedDetails []ProductDetailOutput
		for _, d := range res {
			convertedDetail := ProductDetailOutput{
				ID:           d.ID,
				CategoryID:   d.CategoryID,
				CategoryName: d.CategoryName,
				Count:        d.Count,
			}
			convertedDetails = append(convertedDetails, convertedDetail)
		}
		out := ProductsOutput{
			Product: ProductGoldOutput{
				ID:               o.ID,
				WeightGoal:       o.WeightGoal,
				CategoryGoalID:   o.CategoryGoalID,
				CategoryGoldName: o.CategoryGoldName,
			},
			DetailProducts: convertedDetails,
		}
		details = append(details, out)
	}
	convertOrder := OrderGoalOutput{
		ID:                order.ID,
		ProductName:       order.ProductName,
		Price:             order.Price,
		PriceOld:          order.PriceOld,
		OrderDate:         order.OrderDate,
		CustomerID:        order.CustomerID,
		Percent:           order.Percent,
		AddPayments:       order.AddPayments,
		Note:              order.Note,
		ReturnDate:        order.ReturnDate,
		Status:            order.Status,
		Miss:              order.Miss,
		InterestPaidCount: order.InterestPaidCount,
	}
	// Convert the products to the appropriate type
	var convertedProducts []ProductGoldOutput
	for _, product := range products {
		convertedProduct := ProductGoldOutput{
			ID:               product.ID,
			CategoryGoalID:   product.CategoryGoalID,
			CategoryGoldName: product.CategoryGoldName,
		}
		convertedProducts = append(convertedProducts, convertedProduct)
	}

	// Tạo đối tượng OrderProduct chứa thông tin của đơn hàng và sản phẩm
	orderProduct := OrderProduct{
		Order:    convertOrder,
		Products: details,
	}

	return orderProduct, nil
}

func (c Controller) GetOrderGoalByID(id int64) (OrderProduct, error) {
	order, err := c.orderRepo.GetOrderGoalByID(id)
	if err != nil {
		return OrderProduct{}, err
	}
	// Lấy danh sách sản phẩm tương ứng với đơn hàng
	var details []ProductsOutput
	products, err := c.orderRepo.GetListProductByIDOrder(int64(order.ID))
	if err != nil {
		return OrderProduct{}, err
	}

	for _, o := range products {
		res, err := c.orderRepo.GetListProductDetail(int64(o.ID))
		if err != nil {
			return OrderProduct{}, err
		}
		var convertedDetails []ProductDetailOutput
		for _, d := range res {
			convertedDetail := ProductDetailOutput{
				ID:           d.ID,
				CategoryID:   d.CategoryID,
				CategoryName: d.CategoryName,
				Count:        d.Count,
			}
			convertedDetails = append(convertedDetails, convertedDetail)
		}
		out := ProductsOutput{
			Product: ProductGoldOutput{
				ID:               o.ID,
				WeightGoal:       o.WeightGoal,
				CategoryGoalID:   o.CategoryGoalID,
				CategoryGoldName: o.CategoryGoldName,
			},
			DetailProducts: convertedDetails,
		}
		details = append(details, out)
	}
	convertOrder := OrderGoalOutput{
		ID:                order.ID,
		ProductName:       order.ProductName,
		Price:             order.Price,
		PriceOld:          order.PriceOld,
		OrderDate:         order.OrderDate,
		CustomerID:        order.CustomerID,
		Percent:           order.Percent,
		AddPayments:       order.AddPayments,
		ReturnDate:        order.ReturnDate,
		Status:            order.Status,
		InterestPaidCount: order.InterestPaidCount,
		Note:              order.Note,
		Miss:              order.Miss,
	}
	// Convert the products to the appropriate type
	var convertedProducts []ProductGoldOutput
	for _, product := range products {
		convertedProduct := ProductGoldOutput{
			ID:               product.ID,
			CategoryGoalID:   product.CategoryGoalID,
			CategoryGoldName: product.CategoryGoldName,
		}
		convertedProducts = append(convertedProducts, convertedProduct)
	}

	// Tạo đối tượng OrderProduct chứa thông tin của đơn hàng và sản phẩm
	orderProduct := OrderProduct{
		Order:    convertOrder,
		Products: details,
	}

	return orderProduct, nil
}
