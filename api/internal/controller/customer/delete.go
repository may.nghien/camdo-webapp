package customer

func (c Controller) DeleteCustomer(id int64) error{
	err:= c.customerRepo.DeleteCustomer(id)
	if err!=nil{
		return err
	}

	return nil
}