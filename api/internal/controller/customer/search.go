package customer

import (
	models "Golang/internal/repository/dbmodel"

	"github.com/volatiletech/null/v8"
)

func (c Controller) SearchCustomerByName(inp string) ([]models.Customer, error) {
	users, err := c.customerRepo.SearchCustomerByName(inp)

	if err != nil {
		return nil, err
	}

	var result = make([]models.Customer, len(users))
	for ind, customer := range users {
		result[ind] = models.Customer{CustomerID: int64(customer.CustomerID),
			CustomerName: customer.CustomerName,
			Address:      null.String(customer.Address),
			Gender:       null.String(customer.Gender),
			CCCD:         null.String(customer.CCCD),
			BirthDate:    null.Time(customer.BirthDate),
			Phone:        null.String(customer.Phone),
			AddressCCCD:  null.String(customer.AddressCCCD),
			DayCCCD:      null.Time(customer.DayCCCD),
		}
	}

	return result, nil
}

func (c Controller) SearchCustomerByPhone(inp string) ([]models.Customer, error) {
	users, err := c.customerRepo.SearchCustomerByPhone(inp)

	if err != nil {
		return nil, err
	}

	var result = make([]models.Customer, len(users))
	for ind, customer := range users {
		result[ind] = models.Customer{CustomerID: int64(customer.CustomerID),
			CustomerName: customer.CustomerName,
			Address:      null.String(customer.Address),
			Gender:       null.String(customer.Gender),
			CCCD:         null.String(customer.CCCD),
			BirthDate:    null.Time(customer.BirthDate),
			Phone:        null.String(customer.Phone),
			AddressCCCD:  null.String(customer.AddressCCCD),
			DayCCCD:      null.Time(customer.DayCCCD),
		}
	}

	return result, nil
}

func (c Controller) SearchCustomerByCCCD(inp string) ([]models.Customer, error) {
	users, err := c.customerRepo.SearchCustomerByCCCD(inp)

	if err != nil {
		return nil, err
	}

	var result = make([]models.Customer, len(users))
	for ind, customer := range users {
		result[ind] = models.Customer{CustomerID: int64(customer.CustomerID),
			CustomerName: customer.CustomerName,
			Address:      null.String(customer.Address),
			Gender:       null.String(customer.Gender),
			CCCD:         null.String(customer.CCCD),
			BirthDate:    null.Time(customer.BirthDate),
			Phone:        null.String(customer.Phone),
			AddressCCCD:  null.String(customer.AddressCCCD),
			DayCCCD:      null.Time(customer.DayCCCD),
		}
	}

	return result, nil
}

