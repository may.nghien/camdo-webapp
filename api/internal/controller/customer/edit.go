package customer

import (
	models "Golang/internal/repository/dbmodel"

	"github.com/volatiletech/null/v8"
)

type EditCustomerInput struct {
	CustomerID   int64      
	CustomerName string      
	CCCD         null.String 
	DayCCCD      null.Time   
	AddressCCCD  null.String 
	Gender       null.String 
	Address      null.String 
	BirthDate    null.Time   
	Phone        null.String 
}

func (c Controller) EditCustomer(customerId int64, m EditCustomerInput) (models.Customer, error) {
	return c.customerRepo.EditCustomer(customerId, models.Customer{
		CustomerID:   customerId,
		CustomerName: m.CustomerName,
		AddressCCCD:  m.AddressCCCD,
		CCCD:         m.CCCD,
		Gender:       m.Gender,
		DayCCCD:      m.DayCCCD,
		Phone:        m.Phone,
		BirthDate:    m.BirthDate,
		Address:      m.Address,
	})

}
