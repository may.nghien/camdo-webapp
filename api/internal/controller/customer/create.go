package customer

import (
	models "Golang/internal/repository/dbmodel"
	"github.com/volatiletech/null/v8"
)

type CustomerInput struct {
	CustomerID int
	CustomerName string
	Address     null.String
	Gender      null.String
	CCCD        null.String
	BirthDate    null.Time
	Phone       null.String
	AddressCCCD null.String
	DayCCCD		null.Time
}
func (c Controller) CreateCustomer(inp CustomerInput) (models.Customer, error) {
	return c.customerRepo.CreateCustomer(models.Customer{
		CustomerID:   int64(inp.CustomerID),
		CustomerName: inp.CustomerName,
		Address:      inp.Address,
		Gender:       inp.Gender,
		CCCD:         inp.CCCD,
		BirthDate:    inp.BirthDate,
		Phone:        inp.Phone,
		AddressCCCD: inp.AddressCCCD,
		DayCCCD:	inp.DayCCCD,
	})
}
