package db

import (
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"
)
type DbInstance struct {
	Db *sql.DB
}

var Database DbInstance

func Connect() (*sql.DB, error){
	// `database` host is container which already defined in docker compose
	db, err := sql.Open("postgres", "host=localhost user=postgres password=25092000 dbname=scm port=5432 sslmode=disable")
	if err != nil {
		panic(err.Error())
	}
	fmt.Println("Database Done !")
	Database = DbInstance{Db: db}

	return db,nil
}
