

CREATE TABLE IF NOT EXISTS public.category_percent (
  percent_id        SERIAL PRIMARY KEY,
  percent     FLOAT,
  created_at      TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
  updated_at      TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
);

CREATE TABLE IF NOT EXISTS public.category (
  category_id SERIAL PRIMARY KEY,
  category_name VARCHAR(255),
  created_at      TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
  updated_at      TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
);
CREATE TABLE IF NOT EXISTS public.category_goal (
  category_id SERIAL PRIMARY KEY,
  category_name VARCHAR(255),
  created_at      TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
  updated_at      TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
);