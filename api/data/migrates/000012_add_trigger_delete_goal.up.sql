-- Trigger cho bảng interest_payments_goal
CREATE OR REPLACE FUNCTION delete_revenue_goal_function() RETURNS TRIGGER AS $$
DECLARE
    month_val INT;
    year_val INT;
    paymentAmount INT;
BEGIN
    -- Lấy thông tin tháng và năm từ khoản thanh toán bị xóa
    month_val := EXTRACT(MONTH FROM OLD.payment_date);
    year_val := EXTRACT(YEAR FROM OLD.payment_date);
    paymentAmount := OLD.amount;

    -- Kiểm tra xem đã có bản ghi cho tháng và năm đó trong bảng "revenue_goal" chưa
    IF EXISTS (SELECT * FROM revenue_goal WHERE month = month_val AND year = year_val) THEN
        -- Đã có bản ghi, cập nhật số tiền đóng lãi
        UPDATE revenue_goal
        SET amount = amount - paymentAmount
        WHERE month = month_val AND year = year_val;
    END IF;

    RETURN OLD;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER delete_revenue_goal
AFTER DELETE ON interest_payments_goal
FOR EACH ROW
EXECUTE FUNCTION delete_revenue_goal_function();


-- Trigger cho bảng interest_payments_goal
CREATE OR REPLACE FUNCTION delete_collect_goal_function() RETURNS TRIGGER AS $$
DECLARE
    month_val INT;
    year_val INT;
    paymentAmount INT;
BEGIN
    -- Lấy thông tin tháng và năm từ khoản thanh toán bị xóa
    month_val := EXTRACT(MONTH FROM OLD.payment_date);
    year_val := EXTRACT(YEAR FROM OLD.payment_date);
    paymentAmount := OLD.amount;

    -- Kiểm tra xem đã có bản ghi cho tháng và năm đó trong bảng "collect_goal" chưa
    IF EXISTS (SELECT * FROM collect_goal WHERE month = month_val AND year = year_val) THEN
        -- Đã có bản ghi, cập nhật số tiền đóng lãi
        UPDATE collect_goal
        SET amount = amount - paymentAmount
        WHERE month = month_val AND year = year_val;
    END IF;

    RETURN OLD;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER delete_collect_goal
AFTER DELETE ON interest_payments_goal
FOR EACH ROW
EXECUTE FUNCTION delete_collect_goal_function();


-- Trigger cho bảng redeem_order_goal
CREATE OR REPLACE FUNCTION delete_collect_redeem_goal_function() RETURNS TRIGGER AS $$
DECLARE
    month_val INT;
    year_val INT;
    paymentAmount INT;
BEGIN
    -- Lấy thông tin tháng và năm từ đơn đổi bị xóa
    month_val := EXTRACT(MONTH FROM OLD.date_redeem);
    year_val := EXTRACT(YEAR FROM OLD.date_redeem);
    paymentAmount := OLD.amount;

    -- Kiểm tra xem đã có bản ghi cho tháng và năm đó trong bảng "collect_goal" chưa
    IF EXISTS (SELECT * FROM collect_goal WHERE month = month_val AND year = year_val) THEN
        -- Đã có bản ghi, cập nhật số tiền đóng lãi
        UPDATE collect_goal
        SET amount = amount - paymentAmount
        WHERE month = month_val AND year = year_val;
    END IF;

    RETURN OLD;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER delete_collect_redeem_goal
AFTER DELETE ON redeem_order_goal
FOR EACH ROW
EXECUTE FUNCTION delete_collect_redeem_goal_function();


-- Trigger cho bảng order_goal
CREATE OR REPLACE FUNCTION delete_spend_goal_function() RETURNS TRIGGER AS $$
DECLARE
    month_val INT;
    year_val INT;
    paymentAmount INT;
BEGIN
    -- Lấy thông tin tháng và năm từ đơn hàng bị xóa
    month_val := EXTRACT(MONTH FROM OLD.order_date);
    year_val := EXTRACT(YEAR FROM OLD.order_date);
    paymentAmount := OLD.price;

    -- Kiểm tra xem đã có bản ghi cho tháng và năm đó trong bảng "spend_goal" chưa
    IF EXISTS (SELECT * FROM spend_goal WHERE month = month_val AND year = year_val) THEN
        -- Đã có bản ghi, cập nhật số tiền đóng lãi
        UPDATE spend_goal
        SET amount = amount - paymentAmount
        WHERE month = month_val AND year = year_val;
    END IF;

    RETURN OLD;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER delete_spend_goal
AFTER DELETE ON order_goal
FOR EACH ROW
EXECUTE FUNCTION delete_spend_goal_function();
