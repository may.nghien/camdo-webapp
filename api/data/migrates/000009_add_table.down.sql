DROP TABLE IF EXISTS public.spend_goal;
DROP TABLE IF EXISTS public.revenue_goal;
DROP TABLE IF EXISTS public.collect_goal;
DROP TABLE IF EXISTS public.spend_vehicle;
DROP TABLE IF EXISTS public.revenue_vehicle;
DROP TABLE IF EXISTS public.collect_vehicle;