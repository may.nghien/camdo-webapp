
CREATE TABLE public.interest_payments_goal (
  payment_id BIGSERIAL PRIMARY KEY,
  order_id BIGINT REFERENCES order_goal(id) ,
  payment_date DATE,
  next_payment_date DATE,
  amount INT,
  created_at      TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
  updated_at      TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
);

CREATE TABLE public.interest_payments_vehicle (
  payment_id BIGSERIAL PRIMARY KEY,
  order_id     BIGINT REFERENCES order_vehicle(id) ,
  payment_date DATE,
  next_payment_date DATE,
  amount INT,
    created_at      TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
  updated_at      TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
);

