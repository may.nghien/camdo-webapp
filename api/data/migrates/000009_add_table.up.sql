
CREATE TABLE public.revenue_goal (
    id BIGSERIAL PRIMARY KEY,
    month INT,
    year INT,
    amount BIGINT,
    created_at      TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
    updated_at      TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
);

CREATE TABLE public.spend_goal (
    id BIGSERIAL PRIMARY KEY,
    month INT,
    year INT,
    amount BIGINT,
    created_at      TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
    updated_at      TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
);

CREATE TABLE public.collect_goal (
    id BIGSERIAL PRIMARY KEY,
    month INT,
    year INT,
    amount BIGINT,
    created_at      TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
    updated_at      TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
);


CREATE TABLE public.revenue_vehicle (
    id BIGSERIAL PRIMARY KEY,
    month INT,
    year INT,
    amount BIGINT,
    created_at      TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
    updated_at      TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
);

CREATE TABLE public.spend_vehicle (
    id BIGSERIAL PRIMARY KEY,
    month INT,
    year INT,
    amount BIGINT,
    created_at      TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
    updated_at      TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
);

CREATE TABLE public.collect_vehicle (
    id BIGSERIAL PRIMARY KEY,
    month INT,
    year INT,
    amount BIGINT,
    created_at      TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
    updated_at      TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
);