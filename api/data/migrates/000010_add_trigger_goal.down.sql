DROP TRIGGER IF EXISTS increase_revenue_goal ON interest_payments_goal;
DROP TRIGGER IF EXISTS increase_collect_goal ON interest_payments_goal;
DROP TRIGGER IF EXISTS increase_collect_redeem_goal ON redeem_order_goal;
DROP TRIGGER IF EXISTS increase_spend_goal ON order_goal;