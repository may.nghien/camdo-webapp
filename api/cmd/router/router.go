package router

import (
	authCtrlPkg "Golang/internal/controller/auth"
	orderCtrlPkg "Golang/internal/controller/order_goal"
	authHandlerPkg "Golang/internal/handler/auth"
	orderHandlerPkg "Golang/internal/handler/order_goal"
	accountRepoPkg "Golang/internal/repository/account"
	orderRepoPkg "Golang/internal/repository/order_goal"

	customerCtrlPkg "Golang/internal/controller/customer"
	customerHandlerPkg "Golang/internal/handler/customer"
	customerRepoPkg "Golang/internal/repository/customer"

	categoryCtrlPkg "Golang/internal/controller/category"
	categoryHandlerPkg "Golang/internal/handler/category"
	categoryRepoPkg "Golang/internal/repository/category"

	orderVehicleCtrlPkg "Golang/internal/controller/order_vehicle"
	orderVehicleHandlerPkg "Golang/internal/handler/order_vehicle"
	orderVehicleRepoPkg "Golang/internal/repository/order_vehicle"

	"database/sql"

	"github.com/go-chi/chi"
	"github.com/go-chi/jwtauth/v5"
)

func Init(db *sql.DB) *chi.Mux {
	r := chi.NewRouter()
	//Account
	authRepoImpl := accountRepoPkg.New(db)
	authController := authCtrlPkg.New(authRepoImpl)
	authHandler := authHandlerPkg.New(authController)
	tokenAuth := jwtauth.New("HS256", []byte("secret"), nil)
	//OrderGoal
	orderRepoImpl := orderRepoPkg.New(db)
	orderController := orderCtrlPkg.New(orderRepoImpl)
	orderHandler := orderHandlerPkg.New(orderController)
	//OrderVehicle
	orderVehicleRepoImpl := orderVehicleRepoPkg.New(db)
	orderVehicleController := orderVehicleCtrlPkg.New(orderVehicleRepoImpl)
	orderVehicleHandler := orderVehicleHandlerPkg.New(orderVehicleController)
	//Customer
	customerRepo := customerRepoPkg.New(db)
	customerController := customerCtrlPkg.New(customerRepo)
	customerHandler := customerHandlerPkg.New(customerController)
	//Category
	categoryRepo := categoryRepoPkg.New(db)
	categoryController := categoryCtrlPkg.New(categoryRepo)
	categoryHandler := categoryHandlerPkg.New(categoryController)

	r.Post("/api/sign-in", authHandler.SignIn)
	r.Get("/api/send-otp", authHandler.SendOTP)
	r.Post("/api/check-otp", authHandler.CheckOTP)
	r.Post("/api/reset-password", authHandler.ResetPassword)

	r.Group(func(r chi.Router) {
		r.Use(jwtauth.Verifier(tokenAuth), JWTRouterContext(tokenAuth))
		//Change Password
		r.Post("/api/change-password", authHandler.ChangePassword)
		//Create Order
		r.Post("/api/create-order", orderHandler.CreateOrder)
		//Get
		// r.Get("/api/test/{orderID}", orderHandler.Test)
		r.Get("/api/get-order-goal/{orderID}", orderHandler.GetOrderGoalByID)
		r.Get("/api/get-order-goal-add-products", orderHandler.GetOrderGoldAndProducts)
		r.Get("/api/get-products-by-order-id/{orderID}", orderHandler.GetProductsByOrderID)
		r.Get("/api/get-order-goal/customer-id/{customerID}", orderHandler.GetOrderGoalByCustomerID)
		r.Get("/api/get-payments-goal/{orderID}", orderHandler.GetListPaymentsByOrderID)
		r.Get("/api/get-payments-goal", orderHandler.GetListPaymentsGoal)
		r.Get("/api/get-newest-order-goal", orderHandler.GetNewestOrderGoal)
		r.Get("/api/get-order-goal", orderHandler.GetOrderGoal)
		r.Get("/api/get-order-goal-over", orderHandler.GetOrderGoalOver)
		r.Get("/api/get-payment-day-goal/{orderID}", orderHandler.ListDayPaymentGoalNewest)
		r.Get("/api/payment/get-order-goal-over", orderHandler.ListOrderGoalByPaymentOver)
		r.Get("/api/payment/list-redeem-order-goal/{orderID}", orderHandler.GetListRedeemGoalByOrderID)
		r.Get("/api/payment/list-redeem-order-goal", orderHandler.GetListRedeemGoal)
		r.Get("/api/payment/get-count-order-goal-over", orderHandler.GetCountOrderGoalOver)
		r.Get("/api/payment/get-add-payment-goal/{orderID}", orderHandler.GetAddPaymentInfo)
		//Over Gold
		r.Get("/api/over/get-order-goal", orderHandler.GetOrderGoldOver)
		r.Get("/api/over/get-order-goal-have-payments", orderHandler.GetOrderGoldHavePaymentsOver)
		r.Post("/api/payment/search-order-goal-over", orderHandler.ListSearchOrderGoalByPaymentOver)
		r.Post("/api/search-order-goal-over", orderHandler.ListSearchOrderGoalOver)
		//Create Pay Gold
		r.Post("/api/create-pay-goal", orderHandler.CreatePayGoal)

		//Edit Order Gold
		r.Put("/api/edit-order-goal/{orderID}", orderHandler.EditOrder)
		r.Put("/api/update-note-gold", orderHandler.UpdateStatus)
		r.Put("/api/payment/update-status-order-goal", orderHandler.UpdateStatusOrderGoal)

		//Delete Order Gold
		r.Delete("/api/delete-order-goal/{orderID}", orderHandler.DeleteOrderGoalByID)
		//Search
		r.Post("/api/search-order-goal", orderHandler.SearchOrderGoalByID)

		//Tinh lai
		r.Post("/api/interest-order-goal", orderHandler.CalculateInterest)
		//Create Redeem Gold
		r.Post("/api/payment/redeem-order-goal", orderHandler.RedeemOrderGoal)
		//Cam them
		r.Post("/api/payment/add-payment-goal", orderHandler.UpdateOrderGoalAddPayment)
		//Overview
		r.Post("/api/overview-goal", orderHandler.GetOverView)
		r.Post("/api/get-order-goal-by-month-and-year", orderHandler.GetOrdersByMonthAndYear)
		r.Post("/api/payment/get-by-month-and-year", orderHandler.GetListPaymentsGoalByMonthAndYear)
		r.Post("/api/redeem/get-by-month-and-year", orderHandler.GetListRedeemGoalByMonthAndYear)
		r.Post("/api/add-payment/get-by-month-and-year", orderHandler.GetAddPaymentByMonthAndYear)

		//Get Overview Gold Today
		r.Get("/api/overview-gold-today", orderHandler.GetOverviewGoldToday)
		r.Get("/api/overview/payments-gold-today", orderHandler.GetListPaymentsGoldToday)
		r.Get("/api/overview/redeem-gold-today", orderHandler.GetListRedeemGoldToday)
		r.Get("/api/overview/order-gold-today", orderHandler.GetOrdersGoldToday)
		r.Get("/api/overview/add-payment-gold-today", orderHandler.GetListAddPaymentGoldToday)

		//Export
		r.Post("/api/export/overview-goal", orderHandler.ExportOverViewGoal)

		//Create Order Vehicle
		r.Post("/api/create-order-vehicle", orderVehicleHandler.CreateOrderVehicle)
		//Get
		r.Get("/api/get-newest-order-vehicle", orderVehicleHandler.GetNewestOrderVehicle)
		r.Get("/api/get-order-vehicle", orderVehicleHandler.GetOrderVehicle)
		r.Get("/api/get-order-vehicle/customer-id/{customerID}", orderVehicleHandler.GetOrderVehicleByCustomerID)
		r.Get("/api/get-order-vehicle/{orderID}", orderVehicleHandler.GetOrderVehicleByID)
		r.Get("/api/get-payments-vehicle/{orderID}", orderVehicleHandler.GetListPaymentsByOrderID)
		r.Get("/api/get-order-vehicle-over", orderVehicleHandler.GetOrderVehicleOver)
		r.Get("/api/get-order-vehicle-over-60d", orderVehicleHandler.GetOrderVehicleOver60d)
		r.Get("/api/get-order-vehicle-due", orderVehicleHandler.GetOrderVehicleDue)
		r.Get("/api/get-order-vehicle-upcomming", orderVehicleHandler.GetOrderVehicleUpComming)
		r.Get("/api/get-payment-day-vehicle/{orderID}", orderVehicleHandler.ListDayPaymentVehicleNewest)
		r.Get("/api/get-payments-vehicle", orderVehicleHandler.GetListPaymentsVehicle)
		r.Get("/api/payment/get-add-payment-vehicle/{orderID}", orderVehicleHandler.GetAddPaymentInfo)
		r.Get("/api/payment/list-redeem-order-vehicle/{orderID}", orderVehicleHandler.GetListRedeemVehicle)
		r.Get("/api/payment/get-order-vehicle-over", orderVehicleHandler.ListOrderVehicleByPaymentOver)
		r.Get("/api/payment/get-count-order-vehicle-over", orderVehicleHandler.GetCountOrderVehicleOver)
		//Over Vehicle
		r.Get("/api/over/get-order-vehicle", orderVehicleHandler.GetOrderVehicleOver)
		r.Get("/api/over/get-order-vehicle-have-payments", orderVehicleHandler.GetOrderVehicleHavePaymentsOver)
		r.Post("/api/payment/search-order-vehicle-over", orderVehicleHandler.ListSearchOrderVehicleOver)
		r.Post("/api/search-order-vehicle-over", orderVehicleHandler.ListSearchOrderVehicleByPaymentOver)
		//Dong lai
		r.Post("/api/create-pay-vehicle", orderVehicleHandler.CreatePayVehicle)
		//Delete Order Vehicle
		r.Delete("/api/delete-order-vehicle/{orderID}", orderVehicleHandler.DeleteOrderVehicleByID)
		//Search
		r.Post("/api/search-order-vehicle", orderVehicleHandler.SearchOrderVehicleByID)
		// r.Post("/api/payment/search-order-vehicle-over", orderVehicleHandler.ListSearchOrderVehicleByPaymentOver)
		// r.Post("/api/search-order-vehicle-over", orderVehicleHandler.ListSearchOrderVehicleOver)
		//Tinh lai
		r.Post("/api/interest-order-vehicle", orderVehicleHandler.CalculateInterest)
		//Edit
		r.Put("/api/payment/update-status-order-vehicle", orderVehicleHandler.UpdateStatusOrderVehicle)
		r.Put("/api/update-note-vehicle", orderVehicleHandler.UpdateStatus)
		r.Put("/api/edit-order-vehicle/{orderID}", orderVehicleHandler.EditOrder)
		//Create Redeem Vehicle
		r.Post("/api/payment/redeem-order-vehicle", orderVehicleHandler.RedeemOrderVehicle)
		//Overview
		r.Post("/api/overview-vehicle", orderVehicleHandler.GetOverView)
		r.Post("/api/get-order-vehicle-by-month-and-year", orderVehicleHandler.GetOrdersVehicleByMonthAndYear)
		r.Post("/api/payment/get-vehicle-by-month-and-year", orderVehicleHandler.GetListPaymentsVehicleByMonthAndYear)
		r.Post("/api/redeem/get-vehicle-by-month-and-year", orderVehicleHandler.GetListRedeemVehicleByMonthAndYear)
		r.Post("/api/add-payment/get-vehicle-by-month-and-year", orderVehicleHandler.GetAddPaymentByMonthAndYear)
		//Get Overview Vehicle Today
		r.Get("/api/overview-vehicle-today", orderVehicleHandler.GetOverviewVehicleToday)
		r.Get("/api/overview/payments-vehicle-today", orderVehicleHandler.GetListPaymentsVehicleToday)
		r.Get("/api/overview/redeem-vehicle-today", orderVehicleHandler.GetListRedeemVehicleToday)
		r.Get("/api/overview/order-vehicle-today", orderVehicleHandler.GetOrdersVehicleToday)
		r.Get("/api/overview/add-payment-vehicle-today", orderVehicleHandler.GetListAddPaymentVehicleToday)
		//Export
		r.Post("/api/export/overview-vehicle", orderVehicleHandler.ExportOverViewVehicle)
		//Add Payment
		r.Post("/api/payment/add-payment-vehicle", orderVehicleHandler.UpdateOrderVehicleAddPayment)

		//Create Customer
		r.Post("/api/create-customer", customerHandler.CreateCustomer)
		r.Put("/api/edit-customer/{id}", customerHandler.EditCustomer)
		r.Post("/api/search-customer-by-name", customerHandler.SearchByCustomerName)
		r.Post("/api/search-customer-by-phone", customerHandler.SearchByCustomerPhone)
		r.Post("/api/search-customer-by-cccd", customerHandler.SearchByCustomerCCCD)

		//List Customer
		r.Get("/api/list-customer", customerHandler.GetListCustomer)
		r.Get("/api/list-customer/{id}", customerHandler.GetCustomerByID)
		r.Get("/api/get-newest-customer", customerHandler.GetNewestCustomer)
		r.Delete("/api/delete-customer/{id}", customerHandler.DeleteCustomer)

		//Category

		//Category
		r.Get("/api/list-category", categoryHandler.GetListCategory)
		r.Get("/api/list-category/{id}", categoryHandler.GetListCategoryByID)
		r.Post("/api/create-category", categoryHandler.CreateCategory)
		r.Delete("/api/delete-category/{id}", categoryHandler.DeleteCategory)

		//Category Goal
		r.Post("/api/create-category-goal", categoryHandler.CreateCategoryGoal)
		r.Get("/api/list-category-goal", categoryHandler.GetListCategoryGoal)
		r.Get("/api/list-category-goal/{id}", categoryHandler.GetListCategoryGoalByID)
		r.Delete("/api/delete-category-goal/{id}", categoryHandler.DeleteCategoryGoal)

		//Category Percent
		r.Post("/api/create-category-percent", categoryHandler.CreateCategoryPercent)
		r.Get("/api/list-category-percent", categoryHandler.GetListCategoryPercent)
		r.Get("/api/list-category-percent/{id}", categoryHandler.GetListCategoryPercentByID)
		r.Delete("/api/delete-category-percent/{id}", categoryHandler.DeleteCategoryPercent)

	})

	return r
}
