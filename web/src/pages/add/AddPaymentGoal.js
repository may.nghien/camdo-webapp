import { Button, ConfigProvider, DatePicker, Form, Input, Modal } from "antd";
import BaseURL from "../../api/baseURL";
import moment from "moment";
import { Spin } from "antd";
import { useEffect, useState } from "react";
import locale from 'antd/es/date-picker/locale/vi_VN';
import viVN from "antd/lib/locale/vi_VN";
import 'dayjs/locale/vi';
import dayjs from "dayjs";
import {
  FormatAmountFloatToInt,
  FormatAmountIntToFloat,
  FormatNumberFloat,
} from "../../helpers/Help";

const AddPaymentGoal = ({ isOpen, setIsOpen, orderID, resetFlag, setResetFlag }) => {
  const [loading, setLoading] = useState(true);
  const [dataInterest, setDataInterest] = useState("");
  const [selectedDateEnd, setSelectedDateEnd] = useState(null);
  const [countPayments, setCountPayments] = useState(0);
  const [percentID, setPercentID] = useState("");
  const [percent, setPercent] = useState(0);
  const [dayPayment, setDayPayment] = useState("");
  const [dataOrder, setDataOrder] = useState([]);

  const [amountPayment, setAmountPayment] = useState("");

  const handleCancel = () => {
    setIsOpen(false);
  };

  const formatNumber = (number) => {
    if (typeof number === "number") {
      const formattedNumber = number
        .toString()
        .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      return formattedNumber;
    } else {
      return 0;
    }
  };

  useEffect(() => {
    if (countPayments != 0) {
      BaseURL
        .get(`get-payment-day-goal/${orderID}`)
        .then((response) => {
          setDayPayment(response.data);
        })
        .catch((error) => console.log(error));
    }
  }, [countPayments]);

  useEffect(() => {
    const fetchPercentID = async () => {
      try {
        const response = await BaseURL.get(
          `get-order-goal/${orderID}`
        );
        const fetchedPercentID = response.data.order.Percent;
        setDataOrder(response.data.order);
        setPercentID(fetchedPercentID);
        setCountPayments(response.data.order.InterestPaidCount);
      } catch (error) {
        console.log("Lỗi khi gọi API lấy percentID:", error);
      }
    };

    fetchPercentID();
  }, [orderID, dayPayment]);

  useEffect(() => {
    const fetchPercent = async () => {
      try {
        const response = await BaseURL.get(
          `list-category-percent/${percentID}`
        );
        const percentValue = response.data.Percent;
        setPercent(percentValue);
      } catch (error) {
        console.log("Lỗi khi gọi API lấy percent:", error);
      }
    };

    percentID != "" && fetchPercent();
  }, [percentID]);

  useEffect(() => {
    setLoading(true);
    const params = {
      start_date: countPayments != 0 ? dayPayment : dataOrder.OrderDate,
      order_id: orderID,
      end_date:
        selectedDateEnd == null ? formData.payment_date : selectedDateEnd,
      percent: percent,
    };
    BaseURL
      .post("interest-order-goal", params)
      .then((response) => {
        if (response.status === 200) {
          setDataInterest(response.data);
          setLoading(false);
        }
      }).catch((err) => console.log(err));
  }, [orderID, selectedDateEnd, percent, dayPayment]);

  useEffect(() => {
    setFormData((formData) => ({
      ...formData,
      amount: formatNumber(dataInterest),
    }));
  }, [dataInterest]);

  const [formData, setFormData] = useState({
    amount: "",
    amount_payment: "",
    payment_date: moment(),
    next_payment_date: moment().add(1, "month"),
  });
  const convertNextMonth = (day) => {
    const date = dayjs(day);
    // Lấy ngày sau 1 tháng
    const nextMonth = date.add(1, "month");

    // Lấy thông tin ngày, tháng, năm từ ngày sau 1 tháng
    const nextMonthDay = nextMonth.date();
    const nextMonthMonth = nextMonth.month() + 1; // Tháng trong Day.js bắt đầu từ 0, nên cần cộng thêm 1
    const nextMonthYear = nextMonth.year();
    // Định dạng lại thành dd/mm/yyyy
    const nextMonthFormatted = `${nextMonthDay < 10 ? "0" + nextMonthDay : nextMonthDay
      }/${nextMonthMonth < 10 ? "0" + nextMonthMonth : nextMonthMonth
      }/${nextMonthYear}`;

    const dateS = dayjs(nextMonthFormatted, "DD/MM/YYYY");

    const isoString = dateS.toISOString();

    return isoString;
  };

  const handlePayGoal = () => {
    const params = {
      order_id: orderID,
      amount: parseInt(FormatAmountFloatToInt(formData.amount)),
      payment_date:
        selectedDateEnd == null ? formData.payment_date : selectedDateEnd,
      next_payment_date:
        selectedDateEnd == null
          ? formData.next_payment_date
          : convertNextMonth(selectedDateEnd),
    };
    BaseURL
      .post("create-pay-goal", params)
      .then((response) => {
        if (response.status === 200) {
          setResetFlag(resetFlag + 1);
          setDataInterest(0);
          setDayPayment(response.data.payment_date);
        }
      })
      .catch((err) => console.log(err));
  };

  const handleOk = () => {
    const params = {
      id: orderID,
      amount_payment: parseInt(FormatAmountFloatToInt(formData.amount_payment)),
      date_payment:
        selectedDateEnd == null ? formData.payment_date : selectedDateEnd,
      next_date_payment:
        selectedDateEnd == null
          ? formData.next_payment_date
          : convertNextMonth(selectedDateEnd),
    };
    BaseURL
      .post("payment/add-payment-goal", params)
      .then((response) => {
        if (response.status === 200) {
          setIsOpen(false);
          setResetFlag(resetFlag + 1);
         
        }
      })
      .catch((err) => console.log(err));
  };
  const originalDate = new Date();
  // Lấy các thông tin ngày, tháng, năm từ ngày ban đầu
  const day = originalDate.getDate();
  const month = originalDate.getMonth() + 1; // Tháng trong JavaScript bắt đầu từ 0, nên cần cộng thêm 1
  const year = originalDate.getFullYear();

  // Định dạng lại thành dd/mm/yyyy
  const formattedDate = `${year}-${month < 10 ? "0" + month : month}-${day < 10 ? "0" + day : day
    }`;
  const handleAmountPaymentChange = (event) => {
    const value = event.target.value;
    const formattedValue = FormatAmountIntToFloat(value);
    setAmountPayment(formattedValue);
    setFormData({ ...formData, amount_payment: value });
  };
  const handleChangeDateEnd = (date, dateString) => {
    if (date != null) {
      const dateS = dayjs(date);
      const nextDay = dateS.add(1, "day");
      setSelectedDateEnd(nextDay);
      setFormData({ ...formData, payment_date: dateS });
    }
  };

  return (
    <Modal open={isOpen} onCancel={handleCancel} footer={[
      <>
        <Button onClick={handleCancel}>Hủy</Button>
        <Button key="submit" type="primary" onClick={handleOk} disabled={dataInterest > 0}>
          Cầm thêm
        </Button>
      </>,
    ]}

    >

      <Form className="form-add-pay" autoSave="off" autoComplete="off">
        <Form.Item className="form-add-pay-item" label="Mã đơn">
          <Input className="form-input-pay" value={orderID} disabled />
        </Form.Item>
        {loading ? (
          <Spin size="large" /> // Hiển thị loading khi loading = true
        ) : (<>
            {dataInterest > 0 &&
              <>
                <Form.Item className="form-add-pay-item" label="Phải đóng lãi">
                  <Input
                    className="form-input-pay-disabled"
                    name="amount"
                    value={FormatNumberFloat(formData.amount)}
                    suffix="VNĐ"
                  />
                </Form.Item>
                <div className="btn-submit-add-payment">
                  <Button key="submit" type="primary" onClick={handlePayGoal}>
                    Đóng lãi
                  </Button>
                </div>
              </>
            }
          </>)}

        <Form.Item className="form-add-pay-item" label="Số tiền cầm thêm">
          <Input
            className="form-input-pay"
            name="amountPayment"
            value={amountPayment}
            suffix="VNĐ"
            onChange={handleAmountPaymentChange}
          />
        </Form.Item>
        <Form.Item
          label="Ngày cầm thêm"
          name="payment_date"
          className="form-add-pay-item"
        >
          <ConfigProvider locale={viVN}>
            <DatePicker
              className="form-input-pay"
              format="DD/MM/YYYY"
              defaultValue={dayjs(formattedDate, "YYYY-MM-DD")}
              name="payment_date"
              onChange={handleChangeDateEnd}
              locale={locale}
            />
          </ConfigProvider>
        </Form.Item>
      </Form>
    </Modal>
  );
};
export default AddPaymentGoal;
