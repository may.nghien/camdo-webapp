import { useState } from "react";
import { Button, Form, Input, Pagination, Table } from "antd";
import BaseURL from "../../api/baseURL";
import { ToastContainer, toast } from "react-toastify";
import { ModalDeleteCate } from "../modal/ModalDelete";

const CategoryProduct = ({
  setCategoryName,
  handleSubmit,
  dataSource,
  setResetFlag,
  resetFlag,
}) => {
  const [isOpen, setIsOpen] = useState(false);
  const [isModalDelete, setIsModalDelete] = useState(false);
  const [id, setId] = useState("");
  const [currentPage, setCurrentPage] = useState(1); // Trang hiện tại
  const pageSize = 3; // Kích thước trang
  // Số lượng tổng cộng các mục dữ liệu
  const totalItems = dataSource.length;
  const handlePageChange = (page) => {
    setCurrentPage(page);
  };
  // Lấy dữ liệu trang hiện tại
  const currentPageData = dataSource.slice(
    (currentPage - 1) * pageSize,
    currentPage * pageSize
  );
  const handleDeleteCategory = (id) => {
    BaseURL
      .delete(`delete-category/${id}`)
      .then((response) => {
        if (response.status === 200) {
          setIsModalDelete(false)
          setResetFlag(resetFlag + 1);
        }
      }).catch((err) => toast("Danh mục này đang được sử dụng, không thể xóa được!"));
  };
  const columns = [
    {
      title: "Mã danh mục",
      dataIndex: "CategoryID",
      key: "CategoryID",
    },
    {
      title: "Tên danh mục",
      dataIndex: "CategoryName",
      key: "CategoryName",
    },
    {
      title: "",
      dataIndex: "CategoryID",
      key: "CategoryID",
      render: (id) => (
        <div className="button-action">
          <Button
            className="button-action-delete"
            onClick={() => {
              setId(id);
              setIsModalDelete(true);
            }}
          >
            Xóa
          </Button>
        </div>
      ),
    },
  ];
  return (
    <div className="category-table">
      <div className="category-table-goal">
        DANH MỤC HÀNG
        <Button
          className="button-order-category"
          onClick={() => setIsOpen(!isOpen)}
        >
          {!isOpen ? (
            <i class="nc-icon nc-simple-add"></i>
          ) : (
            <i class="nc-icon nc-simple-delete"></i>
          )}
        </Button>
      </div>
      {isOpen && (
        <div className="category-form">
          <Form autoComplete="off" autoSave="off">
            <Form.Item className="form-add-pawn-item" label="Tên danh mục ">
              <Input
                className="form-input-pawn"
                onChange={(e) => setCategoryName(e.target.value)}
              />
            </Form.Item>
            <div className="btn-add-cate">
              <Button
                className="category-form-button"
                onClick={() => handleSubmit()}
              >
                Thêm loại hàng
              </Button>
            </div>
          </Form>
        </div>
      )}
      <Table
        dataSource={currentPageData}
        columns={columns}
        pagination={false}
      />
      <Pagination
        className="pagination-category"
        current={currentPage}
        pageSize={pageSize}
        total={totalItems}
        onChange={handlePageChange}
      />
      {isModalDelete && (
        <ModalDeleteCate
          isOpen={isModalDelete}
          setIsOpen={setIsModalDelete}
          handleDelete={handleDeleteCategory}
          id={id}
        />
      )}
      <ToastContainer className="custom-toast-container" autoClose={3000} theme="dark" position="top-center" />
    </div>
  );
};
export default CategoryProduct;
