import { Button, Form, Input, Modal } from "antd"
import { useState } from "react";
import BaseURL from "../../api/baseURL";
import { ToastContainer, toast } from "react-toastify";

const ChangePassword = ({ isOpen, setIsOpen }) => {
    const [password, setPassword] = useState("");
    const [newPassword, setNewPassword] = useState("");

    const handleCancel = () => {
        setIsOpen(false)
    }
    const handleChangePass = () => {
        const params = {
            username: localStorage.getItem('name'),
            password: password,
            new_password: newPassword,
        }
        BaseURL.post("/change-password", params).then((response) => {
            if (response.status === 200) {
                localStorage.removeItem('token');
                localStorage.removeItem('name');
                window.location.href = '/sign-in';
            }
        }).catch((err) => toast("Đổi mật khẩu thất bại"))
    }
    return (<>
        <Modal
            open={isOpen}
            onCancel={handleCancel}
            zIndex={1030}
            footer={[
                <>
                    <Button onClick={handleCancel}>Hủy</Button>
                    <Button key="submit" type="primary" onClick={handleChangePass}>
                        Đổi mật khẩu
                    </Button>
                </>,
            ]}>
            <Form className="form-add-pay" autoSave="off" autoComplete="off">
                <Form.Item className="form-add-pay-item" label="Nhập mật khẩu cũ">
                    <Input.Password
                        type="password"
                        className="form-input-pay"
                        onChange={(e) => setPassword(e.target.value)}
                    />
                </Form.Item>
                <Form.Item className="form-add-pay-item" label="Nhập mật khẩu mới">
                    <Input.Password
                        type="password"
                        className="form-input-pay"
                        onChange={(e) => setNewPassword(e.target.value)}
                    />
                </Form.Item>
            </Form>
        </Modal>
        <ToastContainer autoClose={1000} theme="dark" position="top-center" />
    </>)
}

export default ChangePassword