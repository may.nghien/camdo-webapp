import { useState } from "react";
import { Button, Form, Input, Popconfirm } from "antd";
import { useNavigate } from 'react-router-dom';
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import axios from "axios";
import SendOTP from "./SendOTP";

const SignIn = () => {
    const [username, setUserName] = useState("");
    const [password, setPassword] = useState("");
    const [isOpen, setIsOpen] = useState(false)
    const [token, setToken] = useState("")
    const navigate = useNavigate();

    const handleSubmit = () => {
        const params = {
            username: username,
            password: password,
        };
        axios.post("http://localhost:5000/api/sign-in", params).then((response) => {
            if (response.status === 200) {
                const token = response.data.token;
                const username = response.data.username;
                setToken(token);
                localStorage.setItem("token", token);
                localStorage.setItem("name", username);
                navigate('/home')
            }
        }).catch((err) => toast("Đăng nhập thất bại!"));
    };

    return (
        <>
            <div className="login-container">
                <div>
                    <img src="KH.png" width={500} />
                </div>
                <Form className="login-form" onSubmit={handleSubmit} autoComplete="off" autoSave="off">
                    <Input
                        type="username"
                        className="login-input"
                        placeholder="Username"
                        value={username}
                        onChange={(e) => setUserName(e.target.value)}
                    />
                    <Input.Password
                        className="login-input"
                        placeholder="Password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                    />
                    <Button className="login-button" onClick={handleSubmit}>
                        Đăng nhập
                    </Button>
                    <Popconfirm
                        title="Bạn quên mật khẩu? Bấm xác nhận để chúng tôi gửi OTP đến email của bạn!"
                        onConfirm={() => setIsOpen(true)}
                        okText="Xác nhận"
                        cancelText="Hủy"
                    >
                        <div className="forgot-password">
                            Quên mật khẩu
                        </div>
                    </Popconfirm>
                </Form>
            </div>
            <ToastContainer autoClose={1000} theme="dark" position="top-center" />
            {isOpen && <SendOTP isOpen={isOpen} setIsOpen={setIsOpen} />}
        </>
    );
};

export default SignIn;
