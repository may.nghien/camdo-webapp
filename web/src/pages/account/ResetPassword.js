import { Button, Form, Input, Modal } from "antd"
import { ToastContainer, toast } from "react-toastify"
import BaseURL from "../../api/baseURL"
import { useEffect, useState } from "react"

const ResetPassword = ({ isOpen, setIsOpen }) => {
    const [password, setPassword] = useState("")
    const [repassword, setRePassword] = useState("")
    const [newPassword, setNewPassword] = useState("")
    const [isOk, setIsOk] = useState(false)

    const handleCancel = () => {
        setIsOpen(false)
    }
    const onChangePassword = (e) => {
        setPassword(e.target.value)
    }
    useEffect(()=>{
        if(password===repassword){
            setIsOk(false)
            setNewPassword(password)
        }else{
            setIsOk(true)
        }
    }, [password, repassword])
    const onChangeRePassword = (e) => {
        setRePassword(e.target.value)
    }
    const handleResetPass = () => {
        const params = {
            username: 'hoang',
            new_password: newPassword,
        }
        BaseURL.post("/reset-password", params).then((response) => {
            if (response.status === 200) {
                window.location.href = '/sign-in';
            }
        }).catch((err) => toast("Đặt lại mật khẩu thất bại"))
    }
    return (<>
        <Modal open={isOpen} onCancel={handleCancel} footer={[
        <>
          <Button onClick={handleCancel}>Hủy</Button>
          <Button key="submit" type="primary" onClick={handleResetPass} disabled={isOk}>
            Đặt lại mật khẩu
          </Button>
        </>,
      ]}>
            <Form className="form-add-pay" autoSave="off" autoComplete="off">
                <div className="title-otp-modal">THAY ĐỔI MẬT KHẨU</div>
                <Form.Item
                    className="form-add-pay-item"
                    label="Mật khẩu mới"
                >
                    <Input.Password
                        className="form-input-pay"
                        name="password"
                        onChange={onChangePassword}
                    />
                </Form.Item>
                <Form.Item className="form-add-pay-item" label="Nhập lại mật khẩu">
                    <Input.Password
                        className="form-input-pay"
                        name="repassword"
                        type="password"
                        onChange={onChangeRePassword}

                    />
                </Form.Item>
                {isOk===true && <div class="c-red">Mật khẩu chưa trùng lập</div>}
            </Form>
        </Modal>
        <ToastContainer autoClose={1000} theme="dark" position="top-center" />
    </>
    )
}
export default ResetPassword