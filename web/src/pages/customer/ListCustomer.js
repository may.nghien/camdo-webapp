import { useEffect, useState } from "react";
import TheContent from "../../helpers/TheContent";
import BaseURL from "../../api/baseURL";
import { Button, Input, Pagination, Select, Table } from "antd";
import { FormatDate } from "../../helpers/Help";
import ModalOrder from "../modal/ModalOrder";
import DrawerEditCustomer from "./DrawerEditCustomer";
import { ModalDeleteCustomer } from "../modal/ModalDelete";

const ListCustomer = () => {
  const [active, setActive] = useState(7);
  const [subMenu, setSubMenu] = useState(true);
  const [data, setData] = useState([]);
  const [currentPage, setCurrentPage] = useState(1); // Trang hiện tại
  const pageSize = 10; // Kích thước trang
  const [selectSearch, setSelectSearch] = useState(0);
  const [isModalOrder, setIsModalOrder] = useState(false);
  const [isDrawerDetail, setIsDrawerDetail] = useState(false);
  const [customerId, setCustomerId] = useState("");
  const [resetFlag, setResetFlag] = useState(1);
  const [modalDelete, setModalDelete] = useState(false);
  const [expandSidebar, setExpandSidebar] = useState(false)

  const columns = [
    {
      title: "Mã khách hàng",
      dataIndex: "CustomerID",
      key: "CustomerID",
    },
    {
      title: "Họ và tên",
      dataIndex: "CustomerName",
      key: "CustomerName",
    },
    {
      title: "Ngày tháng năm sinh",
      dataIndex: "BirthDate",
      key: "BirthDate",
    },
    {
      title: "Giới tính",
      dataIndex: "Gender",
      key: "Gender",
      render: (gender) => convertGender(gender),
    },
    {
      title: "CMND/CCCD",
      dataIndex: "CCCD",
      key: "CCCD",
    },
    {
      title: "Ngày cấp:",
      dataIndex: "DayCCCD",
      key: "DayCCCD",
    },
    {
      title: "Nơi cấp:",
      dataIndex: "AddressCCCD",
      key: "AddressCCCD",
    },
    {
      title: "SĐT",
      dataIndex: "Phone",
      key: "Phone",
    },
    {
      title: "Địa chỉ",
      dataIndex: "Address",
      key: "Address",
    },
    {
      title: "",
      dataIndex: "CustomerID",
      key: "CustomerID",
      render: (id) => (
        <div className="button-action">
          <Button
            className="button-action-edit"
            onClick={() => {
              setIsDrawerDetail(true);
              setCustomerId(id);
            }}
          >
            Sửa
          </Button>
          <Button
            className="button-action-delete"
            onClick={() => {
              setModalDelete(true);
              setCustomerId(id);
            }}
          >
            Xóa
          </Button>
          <Button
            className="button-action-detail"
            onClick={() => {
              setIsModalOrder(true);
              setCustomerId(id);
            }}
          >
            Thông tin giao dịch
          </Button>
        </div>
      ),
    },
  ];

  const dataSource = data.map((item, index) => ({
    key: index,
    CustomerName: item.customer_name,
    CustomerID: item.customer_id,
    BirthDate: <FormatDate date={item.birth_date} />,
    Gender: item.gender,
    Phone: item.phone,
    CCCD: item.cccd,
    Address: item.address,
    DayCCCD: <FormatDate date={item.day_cccd} />,
    AddressCCCD: item.address_cccd,
  }));
  // Số lượng tổng cộng các mục dữ liệu
  const totalItems = dataSource.length;
  const handlePageChange = (page) => {
    setCurrentPage(page);
  };
  // Lấy dữ liệu trang hiện tại
  const currentPageData = dataSource.slice(
    (currentPage - 1) * pageSize,
    currentPage * pageSize
  );

  useEffect(() => {
    selectSearch === 1
      ? BaseURL
        .post("search-customer-by-phone", {
          phone: "",
        })
        .then((response) => {
          setData(response.data);
        })
      : selectSearch === 2
        ? BaseURL
          .post("search-customer-by-cccd", {
            cccd: "",
          })
          .then((response) => {
            setData(response.data);
          })
        : BaseURL
          .post("search-customer-by-name", {
            name: "",
          })
          .then((response) => {
            setData(response.data);
          });
  }, [selectSearch]);

  const convertGender = (gender) => {
    if (gender === "0") {
      return "Nữ";
    } else {
      return "Nam";
    }
  };

  const handleChangeSelectSearch = (e) => {
    setSelectSearch(e);
  };

  const [searchContent, setSearchContent] = useState("");
  const fetchDataPage = () => {
    setCurrentPage(1);
    selectSearch === 1
      ? BaseURL
        .post("search-customer-by-phone", {
          phone: searchContent,
        })
        .then((response) => {
          setData(response.data);
        })
      : selectSearch === 2
        ? BaseURL
          .post("search-customer-by-cccd", {
            cccd: searchContent,
          })
          .then((response) => {
            setData(response.data);
          })
        : BaseURL
          .post("search-customer-by-name", {
            name: searchContent,
          })
          .then((response) => {
            setData(response.data);
          });
  };

  useEffect(() => {
    fetchDataPage();
  }, [searchContent, resetFlag]);

  const handleDelete = (id) => {
    BaseURL
      .delete(`delete-customer/${id}`)
      .then((response) => {
        if (response.status === 200) {
          setResetFlag(resetFlag + 1);
          setModalDelete(false)
        }
      })
      .catch((error) => console.log(error));
  };
  const content = (
    <>
      <div class="main-panel">
        <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
          <div class="container-fluid">
            <div class="navbar-wrapper">
              <div class="navbar-toggle">
                <Button type="button" class="navbar-toggler" onClick={() => setExpandSidebar(true)}>
                  <span class="navbar-toggler-bar bar1"></span>
                  <span class="navbar-toggler-bar bar2"></span>
                  <span class="navbar-toggler-bar bar3"></span>
                </Button>
              </div>
              <a class="navbar-brand" href="javascript:;">
                KHÁCH HÀNG
              </a>
            </div>
          </div>
        </nav>
        <div class="content">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <form>
                    <div className="search-customer">
                      <Input
                        type="text"
                        className="form-control"
                        placeholder="Tìm kiếm..."
                        onChange={(e) => setSearchContent(e.target.value)}
                      />
                      <div className="input-group-append">
                        <div className="input-group-text">
                          <i class="nc-icon nc-zoom-split"></i>
                        </div>
                      </div>
                      <Select
                        defaultValue={0}
                        onChange={(e) => handleChangeSelectSearch(e)}
                        className="select-search"
                       >
                        <Select.Option value={0}>Tên</Select.Option>
                        <Select.Option value={1}>Số điện thoại</Select.Option>
                        <Select.Option value={2}>CMND/CCCD</Select.Option>
                      </Select>
                    </div>
                  </form>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <Table
                      columns={columns}
                      dataSource={currentPageData}
                      pagination={false}
                    />
                    <Pagination
                      className="pagination-category"
                      current={currentPage}
                      pageSize={pageSize}
                      total={totalItems}
                      onChange={handlePageChange}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
  return (
    <>
      <TheContent
        content={content}
        active={active}
        setActive={setActive}
        checkSubMenuCustomer={subMenu}
        isExpand={expandSidebar}
        setIsExpand={setExpandSidebar}
      />
      {isModalOrder && (
        <ModalOrder
          customerID={customerId}
          isOpen={isModalOrder}
          setIsOpen={setIsModalOrder}
        />
      )}
      {isDrawerDetail && (
        <DrawerEditCustomer
          isOpen={isDrawerDetail}
          setIsOpen={setIsDrawerDetail}
          customerID={customerId}
          resetFlag={resetFlag}
          setResetFlag={setResetFlag}
        />
      )}
      {modalDelete && (
        <ModalDeleteCustomer
          isOpen={modalDelete}
          setIsOpen={setModalDelete}
          handleDelete={handleDelete}
          id={customerId}
        />
      )}
    </>
  );
};
export default ListCustomer;
