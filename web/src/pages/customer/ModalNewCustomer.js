import {
  Button,
  ConfigProvider,
  DatePicker,
  Form,
  Input,
  Modal,
  Radio,
  Select,
  Table,
} from "antd";
import TextArea from "antd/es/input/TextArea";
import BaseURL from "../../api/baseURL";
import moment from "moment";
import { useEffect, useState } from "react";
import { ToastContainer, toast } from "react-toastify";
import locale from 'antd/es/date-picker/locale/vi_VN';
import viVN from "antd/lib/locale/vi_VN";
import 'dayjs/locale/vi';
const ModalNewCustomer = ({
  isOpenNewCus,
  setIsOpenNewCus,
  setIsSelect,
  setMessage,
}) => {
  const [data, setData] = useState([]);
  const handleSubmit = () => { };
  const handleCancel = () => {
    setIsOpenNewCus(false);
  };
  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const handleChangeDate = (date, dateString) => {
    // Parse the original string as a moment object
    const momentObj = moment(dateString, "YYYY-MM-DD");

    // Create the target string with the expected format
    const targetString = momentObj.format("YYYY-MM-DDTHH:mm:ss[Z]");
    setFormData({ ...formData, birth_date: targetString });
  };

  const onSelectGender = (e) => {
    setFormData({ ...formData, gender: e });
  };

  const handleChangeDateCCCD = (date, dateString) => {
    // Parse the original string as a moment object
    const momentObj = moment(dateString, "YYYY-MM-DD");

    // Create the target string with the expected format
    const targetString = momentObj.format("YYYY-MM-DDTHH:mm:ss[Z]");
    setFormData({ ...formData, day_cccd: targetString });
  };

  const [formData, setFormData] = useState({
    customer_name: "",
    cccd: "",
    gender: "0",
    address: "",
    birth_date: "",
    phone: "",
    day_cccd: "",
    address_cccd: "CT CỤC CSHCQTTHXH"
  });

  const handleOk = () => {
    const params = {
      customer_name: formData.customer_name,
      cccd: formData.cccd,
      gender: formData.gender,
      address: formData.address,
      birth_date: formData.birth_date,
      phone: formData.phone,
      day_cccd: formData.day_cccd,
      address_cccd: formData.address_cccd,
    };
    BaseURL
      .post("create-customer", params)
      .then((response) => {
        if (response.status === 200) {
          setIsSelect(true);
          setIsOpenNewCus(false);
          setMessage("");
        }
      })
      .catch((err) => toast("Thêm khách hàng thất bại!"));
  };
  const [isMobile, setIsMobile] = useState(false);

  useEffect(() => {
    const handleResize = () => {
      setIsMobile(window.innerWidth <= 1200);
    };

    handleResize();

    window.addEventListener('resize', handleResize);
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  return (
    <>
      <Modal
        className={isMobile ? "modal-list-customer-mobile" : "modal-list-customer"}
        title="Thêm khách hàng"
        open={isOpenNewCus}
        onCancel={handleCancel}
        onOk={handleOk}
        bodyStyle={{ maxHeight: '500px', overflow: 'auto' }}
      >
        <>
          <Form className="form-add-pawn" autoComplete="off">
            <Form.Item className="form-add-pawn-item" label="Tên khách hàng">
              <Input
                className="form-input-pawn"
                onChange={handleChange}
                name="customer_name"
              />
            </Form.Item>
            <Form.Item className="form-add-pawn-item" label="Giới tính">
              <Select
                defaultValue="0"
                className="form-input-select"
                onSelect={onSelectGender}
              >
                <Select.Option value="0">Nữ</Select.Option>
                <Select.Option value="1">Nam</Select.Option>
              </Select>
            </Form.Item>
            <Form.Item
              className="form-add-pawn-item"
              label="Ngày sinh"
            >
              <ConfigProvider locale={viVN}>
                <DatePicker
                  className="form-input-pawn"
                  onChange={handleChangeDate}
                  name="birth_date"
                  locale={locale}
                />
              </ConfigProvider>
            </Form.Item>
            <Form.Item className="form-add-pawn-item" label="CMND/CCCD">
              <Input
                className="form-input-pawn"
                onChange={handleChange}
                name="cccd"
              />
            </Form.Item>
            <Form.Item className="form-add-pawn-item" label="Ngày cấp">
              <ConfigProvider locale={viVN}>
                <DatePicker
                  className="form-input-pawn"
                  onChange={handleChangeDateCCCD}
                  name="day_cccd"
                  locale={locale}
                />
              </ConfigProvider>
            </Form.Item>
            <Form.Item className="form-add-pawn-item" label="Nơi cấp" >
              <Input
                defaultValue={formData.address_cccd}
                className="form-input-pawn"
                onChange={handleChange}
                name="address_cccd"
              />
            </Form.Item>
            <Form.Item className="form-add-pawn-item" label="Số điện thoại">
              <Input
                className="form-input-pawn"
                onChange={handleChange}
                name="phone"
              />
            </Form.Item>
            <Form.Item className="form-add-pawn-item" label="Hộ khẩu thường trú">
              <TextArea
                className="form-input-textarea"
                onChange={handleChange}
                name="address"
              />
            </Form.Item>
          </Form>
        </>
      </Modal>
      <ToastContainer autoClose={1000} theme="dark" />
    </>
  );
};

export default ModalNewCustomer;
