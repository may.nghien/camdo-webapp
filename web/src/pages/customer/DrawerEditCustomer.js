import { Button, ConfigProvider, DatePicker, Drawer, Form, Input, Select } from "antd";
import TextArea from "antd/es/input/TextArea";
import BaseURL from "../../api/baseURL";
import dayjs from "dayjs";
import { useEffect, useState } from "react";
import locale from 'antd/es/date-picker/locale/vi_VN';
import viVN from "antd/lib/locale/vi_VN";
import 'dayjs/locale/vi';
const DrawerEditCustomer = ({
  isOpen,
  setIsOpen,
  customerID,
  resetFlag,
  setResetFlag,
}) => {
  const [dataCustomer, setDataCustomer] = useState([]);
  const [selectedDate, setSelectedDate] = useState(null);
  const [selectedDateCCCD, setSelectedDateCCCD] = useState(null);

  const [formData, setFormData] = useState({
    customer_id: customerID,
    customer_name: "",
    address: "",
    cccd: "",
    address_cccd: "",
    day_cccd: "",
    phone: "",
    gender: "",
    birth_date: "",
  });

  useEffect(() => {
    BaseURL
      .get(`${process.env.REACT_APP_API_URL}/list-customer/${customerID}`)
      .then((response) => {
        setDataCustomer(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [customerID]);

  const handleCancel = () => {
    setIsOpen(false);
  };
  useEffect(() => {
    if (dataCustomer) {

      setFormData((formData) => ({
        ...formData,
        customer_id: customerID,
        customer_name: dataCustomer.customer_name,
        address: dataCustomer.address,
        cccd: dataCustomer.cccd,
        address_cccd: dataCustomer.address_cccd,
        day_cccd: dataCustomer.day_cccd,
        phone: dataCustomer.phone,
        gender: dataCustomer.gender,
        birth_date: dataCustomer.birth_date,
      }));
    }
  }, [dataCustomer]);

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };
  const handleSelectGender = (e) => {
    setFormData({ ...formData, gender: e });
  };
  const handleChangeDate = (date, dateString) => {
    if (date != null) {
      const dateS = dayjs(date);
      const nextDay = dateS.add(1, "day");
      setSelectedDate(nextDay);
      setFormData({ ...formData, birth_date: dateS });
    }
  };

  const handleChangeDateCCCD = (date, dateString) => {
    if (date != null) {
      const dateS = dayjs(date);
      const nextDay = dateS.add(1, "day");
      setSelectedDateCCCD(nextDay);
      setFormData({ ...formData, day_cccd: dateS });
    }
  };

  const handleSubmit = () => {
    const params = {
      customer_id: customerID,
      customer_name: formData.customer_name,
      address: formData.address,
      cccd: formData.cccd,
      address_cccd: formData.address_cccd,
      day_cccd: selectedDateCCCD == null ? formData.day_cccd : selectedDateCCCD,
      phone: formData.phone,
      gender: formData.gender,
      birth_date: selectedDate == null ? formData.birth_date : selectedDate,
    }
    BaseURL.put(`edit-customer/${customerID}`, params).then((response) => {
      if (response.status === 200) {
        setIsOpen(false);
        setResetFlag(resetFlag + 1);
      }
    })
      .catch((err) => console.log(err));
  }
  return (
    <>
      <Drawer
        zIndex={1030}
        className="drawer-detail"
        title={
          <div className="drawer-detail-title">
            SỬA KHÁCH HÀNG
            <div className="drawer-detail-id">
              (Mã khách hàng: {customerID})
            </div>
          </div>
        }
        placement="right"
        onClose={handleCancel}
        open={isOpen}
      >
        <Form className="form-edit-pawn" autoSave="off" autoComplete="off">
          <Form.Item className="form-edit-pawn-item" label="Tên KH">
            <Input
              className="form-input-pawn"
              name="customer_name"
              value={formData.customer_name}
              onChange={handleChange}
            />
          </Form.Item>
          <Form.Item className="form-edit-pawn-item" label="Giới tính">
            <Select
              value={formData.gender == "0" ? "0" : "1"}
              className="form-input-select"
              onChange={handleSelectGender}
            >
              <Select.Option value="0">Nữ</Select.Option>
              <Select.Option value="1">Nam</Select.Option>
            </Select>
          </Form.Item>
          <Form.Item className="form-edit-pawn-item" label="Ngày sinh">
          <ConfigProvider locale={viVN}>
            <DatePicker
              className="form-input-pawn"
              name="birth_date"
              value={formData.birth_date != "" && dayjs(formData.birth_date, "YYYY-MM-DD")}
              onChange={handleChangeDate}
              locale={locale}
            />
            </ConfigProvider>
          </Form.Item>
          <Form.Item className="form-edit-pawn-item" label="CMND/CCCD">
            <Input
              className="form-input-pawn"
              name="cccd"
              value={formData.cccd}
              onChange={handleChange}
            />
          </Form.Item>
          <Form.Item className="form-edit-pawn-item" label="Ngày cấp">
          <ConfigProvider locale={viVN}>
            <DatePicker
              className="form-input-pawn"
              value={formData.day_cccd ? dayjs(formData.day_cccd, "YYYY-MM-DD") : null}
              onChange={handleChangeDateCCCD}
              name="day_cccd"
            />
            </ConfigProvider>
          </Form.Item>
          <Form.Item className="form-edit-pawn-item" label="Nơi cấp">
            <Input
              className="form-input-pawn"
              name="address_cccd"
              value={formData.address_cccd}
              onChange={handleChange}
            />
          </Form.Item>
          <Form.Item className="form-edit-pawn-item" label="SĐT">
            <Input
              className="form-input-pawn"
              name="phone"
              value={formData.phone}
              onChange={handleChange}
            />
          </Form.Item>
          <Form.Item className="form-edit-pawn-item" label="Hộ khẩu">
            <TextArea
              className="form-input-textarea"
              name="address"
              value={formData.address}
              onChange={handleChange}
            />
          </Form.Item>
          <div>
            <Button
              className="save-edit-button"
              onClick={() => {
                handleSubmit();
              }}
            >
              Lưu thay đổi
            </Button>
          </div>
        </Form>
      </Drawer>
    </>
  );
};
export default DrawerEditCustomer;
