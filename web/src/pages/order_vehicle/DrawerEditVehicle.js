import { Button, ConfigProvider, DatePicker, Drawer, Form, Input, Select } from "antd";
import BaseURL from "../../api/baseURL";
import { useEffect, useState } from "react";
import {
  FormatAmountFloatToInt,
  FormatAmountIntToFloat,
  FormatDate,
} from "../../helpers/Help";
import dayjs from "dayjs";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import locale from 'antd/es/date-picker/locale/vi_VN';
import viVN from "antd/lib/locale/vi_VN";
import 'dayjs/locale/vi';
const DrawerEditVehicle = ({
  isOpen,
  setIsOpen,
  orderID,
  resetFlag,
  setResetFlag,
}) => {
  const [data, setData] = useState([]);
  const [dataCate, setDataCate] = useState([]);
  const [dataCateGoal, setDataCateGoal] = useState([]);
  const [dataCatePercent, setDataCatePercent] = useState([]);

  const [selectedDate, setSelectedDate] = useState(null);
  const [selectedDateEnd, setSelectedDateEnd] = useState(null);

  const [formData, setFormData] = useState({
    product_name: "",
    estimated_price: "",
    price: "",
    weight_goal: "",
    order_date: "",
    return_date: "",
    status: "",
    interest_paid_count: "",
    category_id: "",
    category_goal_id: "",
    customer_id: "",
    percent: "",
  });

  const onClose = () => {
    setResetFlag(resetFlag + 1)
    setIsOpen(false);
  };
  useEffect(() => {
    BaseURL
      .get(`get-order-vehicle/${orderID}`)
      .then((response) => {
        setData(response.data);
      })
      .catch((error) => {
        if (error.response) {
          console.log("Server responded with a non-2xx status");
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else if (error.request) {
          console.log("No response received from the server");
          console.log(error.request);
        } else {
          console.log("Error setting up the request");
          console.log(error.message);
        }
        console.log(error.config);
      });
  }, [orderID]);

  const handleChangeDate = (date, dateString) => {
    if (date != null) {
      const dateS = dayjs(date);
      const nextDay = dateS.add(1, "day");
      setSelectedDate(nextDay);
      setFormData({ ...formData, order_date: dateS });
    }
  };

  const handleChangeDateEnd = (date, dateString) => {
    if (date != null) {
      const dateS = dayjs(date);
      const nextDay = dateS.add(1, "day");
      setSelectedDateEnd(nextDay);
      setFormData({ ...formData, return_date: dateS });
    }
  };

  useEffect(() => {
    BaseURL
      .get("list-category-percent")
      .then((response) => {
        if (response.status === 200) {
          setDataCatePercent(response.data);
        }
      })
      .catch((error) => {
        if (error.response) {
          console.log("Server responded with a non-2xx status");
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else if (error.request) {
          console.log("No response received from the server");
          console.log(error.request);
        } else {
          console.log("Error setting up the request");
          console.log(error.message);
        }
        console.log(error.config);
      });
  }, []);
  const formatNumber = (number) => {
    if (typeof number === "number") {
      const formattedNumber = number
        .toString()
        .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      return formattedNumber;
    } else {
      return 0;
    }
  };

  const handleAmountChange = (event) => {
    const value = event.target.value;
    const formattedValue = FormatAmountIntToFloat(value);
    setFormData({ ...formData, price: formattedValue });
  };

  useEffect(() => {
    if (data) {
      setFormData((prevFormData) => ({
        ...prevFormData,
        product_name: data.product_name,
        estimated_price: formatNumber(data.estimated_price),
        price: formatNumber(data.price),
        // order_date: dayjs(data.order_date).format("YYYY-MM-DD"),
        // return_date: dayjs(data.return_date).format("YYYY-MM-DD"),
        order_date: data.order_date,
        return_date: data.return_date,
        status: data.status,
        interest_paid_count: data.interest_paid_count,
        customer_id: data.customer_id,
        percent: data.percent,
        license_plate: data.license_plate,
      }));
    }
  }, [data]);

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const handleSelectStatus = (e) => {
    setFormData({ ...formData, status: e });
  };
  const handleSelectPercent = (e) => {
    setFormData({ ...formData, percent: e });
  };

  const handleSubmit = () => {
    const params = {
      product_name: formData.product_name,
      estimated_price: parseInt(
        FormatAmountFloatToInt(formData.estimated_price)
      ),
      price: parseInt(FormatAmountFloatToInt(formData.price)),
      order_date: selectedDate == null ? formData.order_date : selectedDate,
      return_date:
        selectedDateEnd == null ? formData.return_date : selectedDateEnd,
      status: formData.status,
      interest_paid_count: 0,
      percent: parseFloat(formData.percent),
      customer_id: formData.customer_id,
      license_plate: formData.license_plate,
    };
    BaseURL
      .put(`edit-order-vehicle/${orderID}`, params)
      .then((response) => {
        if (response.status === 200) {
          setIsOpen(false);
          setResetFlag(resetFlag + 1);
          toast("Cập nhật thành công!");
        }
      })
      .catch((err) => toast("Cập nhật  thất bại!"));
  };
  const onSave = () => { };

  return (
    <>
      <Drawer
        zIndex={1030}
        className="drawer-detail"
        title={
          <div className="drawer-detail-title">
            SỬA HỒ SƠ<div className="drawer-detail-id">(Mã đơn: {orderID})</div>
          </div>
        }
        placement="right"
        onClose={onClose}
        open={isOpen}
      >
        <Form className="form-edit-order" autoSave="off" autoComplete="off">
          <Form.Item className="form-edit-order-label" label="Mã khách hàng">
            <Input
              className="form-edit-order-input"
              value={formData.customer_id}
              disabled
            />
          </Form.Item>
          <Form.Item className="form-edit-order-label" label="Tên xe">
            <Input
              className="form-edit-order-input"
              value={formData.product_name}
              name="product_name"
              onChange={handleChange}
            />
          </Form.Item>
          <Form.Item className="form-edit-order-label" label="Biển số xe">
            <Input
              className="form-edit-order-input"
              value={formData.license_plate}
              name="license_plate"
              onChange={handleChange}
            />
          </Form.Item>
          <Form.Item className="form-edit-order-label" label="Trạng thái">
            <Select
              className="form-edit-order-select"
              value={formData.status}
              onChange={handleSelectStatus}
              name="status"
            >
              <Select.Option value="0">Còn hiệu lực</Select.Option>
              <Select.Option value="1">Đã xong</Select.Option>
              <Select.Option value="2">Đã hóa giá</Select.Option>
            </Select>
          </Form.Item>
          {/* <Form.Item
            className="form-edit-order-label"
            label="Số tiền ước lượng"
          >
            <Input
              className="form-edit-order-input"
              name="estimated_price"
              value={formData.estimated_price}
              onChange={handleAmountEstiChange}
            />
          </Form.Item> */}
          <Form.Item className="form-edit-order-label" label="Số tiền cầm cố">
            <Input
              className="form-edit-order-input"
              name="price"
              value={formData.price}
              onChange={handleAmountChange}
            />
          </Form.Item>
          <Form.Item className="form-edit-order-label" label="Lãi suất">
            {/* <Input
            className="form-edit-order-input"
            name="percent"
            value={formData.percent}
            onChange={handleSelectPercent}
          /> */}
            <Select
              className="form-edit-order-select"
              onChange={handleSelectPercent}
              value={formData.percent}
              name="category_goal_id"
            >
              {dataCatePercent.map((item) => (
                <Select.Option value={item.PercentID}>
                  {item.Percent}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item className="form-edit-order-label" label="Ngày cầm">
            <ConfigProvider locale={viVN}>
              <DatePicker
                className="form-edit-order-input"
                value={dayjs(formData.order_date, "YYYY-MM-DD")}
                name="order_date"
                onChange={handleChangeDate}
                locale={locale}
              />
            </ConfigProvider>
          </Form.Item>
          <Form.Item className="form-edit-order-label" label="Ngày hết hạn">
          <ConfigProvider locale={viVN}>
            <DatePicker
              className="form-edit-order-input"
              value={dayjs(formData.return_date, "YYYY-MM-DD")}
              name="return_date"
              onChange={handleChangeDateEnd}
              locale={locale}
            />
            </ConfigProvider>
          </Form.Item>
          <div className="form-edit-order-save">
            <Button className="btn-cancel" onClick={() => onSave()}>
              Huỷ
            </Button>
            <Button className="btn-ok" onClick={() => handleSubmit()}>
              Lưu thay đổi
            </Button>
          </div>
        </Form>
      </Drawer>
      <ToastContainer autoClose={1000} theme="dark" position="top-center" />
    </>
  );
};
export default DrawerEditVehicle;
