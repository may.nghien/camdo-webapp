import { Checkbox, Form, Input, Modal, Radio } from "antd"
import { useEffect, useState } from "react";
import BaseURL from "../../api/baseURL";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
const UpdateNote = ({ isOpen, setIsOpen, id, resetFlag, setResetFlag }) => {
  const [data, setData] = useState([])
  const [note, setNote] = useState("")
  const [checked, setChecked] = useState(false);
  useEffect(() => {
    BaseURL
      .get(`get-order-goal/${id}`)
      .then((response) => {
        setData(response.data.order);
        setNote(response.data.order.Note)
        setChecked(response.data.order.Miss === 1);
      })
      .catch((error) => {
        if (error.response) {
          console.log("Server responded with a non-2xx status");
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else if (error.request) {
          console.log("No response received from the server");
          console.log(error.request);
        } else {
          console.log("Error setting up the request");
          console.log(error.message);
        }
        console.log(error.config);
      });
  }, [id]);

  const handleCancel = () => {
    setIsOpen(false);
  };
  const handleChangeNote = (e) => {
    setNote(e.target.value)
  }
  const handleSubmit = () => {
    const params = {
      id: id,
      note: note,
      miss: checked == true ? 1 : 0
    }
    BaseURL.put("update-note-gold", params)
      .then((response) => {
        if (response.status === 200) {
          setIsOpen(false);
          setResetFlag(resetFlag + 1);
          toast("Cập nhật thành công!");
        }
      })
      .catch((err) => toast("Cập nhật thất bại!"));
  };

  const handleChangeCheck = (e) => {
    setChecked(e.target.checked);
  };

  return (<>
    <Modal open={isOpen} onCancel={handleCancel} onOk={handleSubmit}>
      <Form className="form-add-note" autoSave="off" autoComplete="off">
        <Form.Item className="form-add-note-item" label="Mã đơn">
          <Input className="form-input-id" value={id} disabled />
        </Form.Item>
        <Checkbox onChange={handleChangeCheck} checked={checked} className="form-check-note"
        >
          Khách mất giấy
        </Checkbox>
        <Form.Item className="form-add-note-item" label="Ghi chú">
          <Input.TextArea value={note} className="form-textarea" onChange={(e) => handleChangeNote(e)} />
        </Form.Item>
      </Form>
    </Modal>

    <ToastContainer autoClose={1000} theme="dark" position="top-center" />

  </>)
}

export default UpdateNote