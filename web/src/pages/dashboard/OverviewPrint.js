import { Button, Modal } from "antd";
import BaseURL from "../../api/baseURL";
import { useEffect, useRef, useState } from "react";
import dayjs from "dayjs";
import {
  CategoryColumn,
  ConvertCategory,
  ConvertCategoryGoal,
  ConvertIDToPercentName,
  FormatCurrency,
  FormatDate,
  NumberToWords,
} from "../../helpers/Help";

const OverviewPrint = ({ isOpen, setIsOpen, isOrder }) => {
  const contentRef = useRef(null);
  const [dataOrder, setDataOrder] = useState([]);
  const [dataCustomer, setDataCustomer] = useState([]);
  const [percentID, setPercentID] = useState("");
  const [percent, setPercent] = useState("");

  const handleCancel = () => {
    setIsOpen(false);
  };
  const handleOk = () => {
    setIsOpen(false);
    const contentToPrint = contentRef.current.innerHTML;
    // Sử dụng nội dung để in
    const printWindow = window.open("", "_blank");
    printWindow.document.open();
    printWindow.document.write(
      "<html><head><style>" +
        getPrintStyles() +
        "</style></head><body>" +
        contentToPrint +
        "</body></html>"
    );

    printWindow.document.close();
    printWindow.print();
  };

  const getPrintStyles = () => {
    // Lấy nội dung SCSS và biên dịch thành CSS
    const cssContent = `
        .modal-order-print{
          width: 700px !important;
          border-radius: 0%; 
          }
          .modal-order-print-content{
            padding-bottom:100px
          }
        .modal-order-print .ant-modal-content{
            border-radius: 0%;
        }
        .modal-order-print .modal-order-print-content{
            font-size: 16px;
        }
        .modal-order-print-content .modal-title{
            text-align: center;
            font-weight: bold;     
        }
        .modal-title .label-name{
                font-size: 20px;
            }
        .modal-body .modal-body-content{
            padding: 5px;
            
        }
        
        .modal-body-content .content-label{
            text-decoration:underline;
            font-weight: bold;
            font-size: 18px;
        
        }
        
        .modal-print-footer{
            padding: 10px 0px;
            
        }
        .modal-print-footer .footer-signature{
                display: flex;
                gap:200px;
                padding:30px 50px 50px 100px
            }
        `;

    return cssContent;
  };

  useEffect(() => {
    BaseURL
      .get(`list-customer/${dataOrder.customer_id}`)
      .then((response) => {
        if (response.status === 200) {
          setDataCustomer(response.data);
        }
      })
      .catch((error) => {
        if (error.response) {
          console.log("Server responded with a non-2xx status");
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else if (error.request) {
          console.log("No response received from the server");
          console.log(error.request);
        } else {
          console.log("Error setting up the request");
          console.log(error.message);
        }
        console.log(error.config);
      });
  }, [dataOrder]);
  useEffect(() => {
    BaseURL
      .get(`list-category-percent/${percentID}`)
      .then((response) => {
        if (response.status === 200) {
          setPercent(response.data.Percent);
        }
      })
      .catch((error) => {
        if (error.response) {
          console.log("Server responded with a non-2xx status");
        } else if (error.request) {
          console.log(error.request);
        } else {
          console.log(error.message);
        }
        console.log(error.config);
      });
  }, [percentID]);
  const GetYear = (day) => {
    const date = dayjs(day);

    // Lấy năm
    const year = date.year();
    return year;
  };
  return (
    <>
      <Modal
        success
        open={isOpen}
        onCancel={handleCancel}
        footer={[
          <>
            <Button key="print" onClick={handleCancel}>
              Đóng
            </Button>
            <Button key="print" type="primary" onClick={handleOk}>
              In
            </Button>
          </>,
        ]}
      >
        <p className="print-notification">
          <i
            class="fa fa-check-circle"
            style={{ color: "green", fontSize: "30px", paddingRight: "10px" }}
            aria-hidden="true"
          ></i>
          Thêm đơn cầm đồ thành công!
        </p>
        <p className="print-notification-button">Bạn có muốn in đơn này ?</p>
      </Modal>
      <div ref={contentRef}>
        <div className="modal-order-print-content">
          <div className="modal-title">
            <p>CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM</p>
            <p>Độc lập - Tự do - Hạnh phúc</p>
            <div style={{ fontSize: "20px" }} className="label-name">
              HỢP ĐỒNG CẦM CỐ TÀI SẢN
            </div>
          </div>
          <div className="modal-body">
            <div className="modal-body-content">
              <div className="content-label">Bên A:</div>
              <div>
                Họ và tên:<b> ÂU KIM HÒA</b>, Năm sinh: <b>1961</b>
              </div>
              <div>
                Là đại diện cơ sở:
                <b> Dịch Vụ Cầm Đồ KHẢI HOÀN - ĐT: 0293.3848329</b>
              </div>
              <div>
                Địa chỉ: <b>105 Quốc lộ 61 Cái Tắc - Hậu Giang</b>
              </div>
            </div>
            <div className="modal-body-content">
              <div className="content-label">Bên B:</div>
              <div>
                Họ và tên:<b>{dataCustomer.customer_name}</b>, Năm sinh:{" "}
                <b>{GetYear(dataCustomer.birth_date)}</b>
              </div>
              <div>
                Nơi đăng ký HKTT:<b>{dataCustomer.address}</b>
              </div>
              <div>
                Số CMND/CCCD:<b>{dataCustomer.cccd}</b> Ngày cấp:
                <b>
                  {dataCustomer.day_cccd ? (
                    <FormatDate date={dataCustomer.day_cccd} />
                  ) : (
                    "..............."
                  )}
                </b>{" "}
                Nơi cấp:
                <b>
                  {dataCustomer.address_cccd
                    ? dataCustomer.address_cccd
                    : "..............."}
                </b>
              </div>
            </div>
            <div className="modal-body-content">
              <div className="content-label">NỘI DUNG:</div>
              <div>
                Bên A nhận cầm tài sản Bên B mang đến, thống nhất thỏa thuận như
                sau:
              </div>
              <div>
                - Tên tài sản cầm cố:
                <b>
                  {dataOrder.product_name}{" "}
                  {isOrder == 2 && <>- Biển số: {dataOrder.license_plate}</>}
                </b>
              </div>
              <div>
                - Giá trị tài sản cầm cố ước tính:
                <b>
                  <FormatCurrency amount={dataOrder.estimated_price} />
                </b>
              </div>
              <div>
                - Số tiền cầm cố:
                <b>
                  <FormatCurrency amount={dataOrder.price} />
                </b>{" "}
                Ghi bằng chữ:
                <b>
                  <NumberToWords amount={dataOrder.price} /> đồng
                </b>
              </div>
              <div>
                - Lãi suất:<b>{percent} %</b>
              </div>
              <div>
                - Đóng lãi:
              </div>
              <div>
                - Thời gian cầm cố tài sản từ ngày:
                <b>
                  <FormatDate date={dataOrder.order_date} />
                </b>{" "}
                Đến ngày:
                <b>
                  <FormatDate date={dataOrder.return_date} />
                </b>
              </div>
            </div>
            <div className="modal-print-footer">
              <div>
                Sau thời hạn 60 ngày (sáu mươi), kể từ ngày:
                <b>
                  <FormatDate date={dataOrder.order_date} />
                </b>
                ,Bên B không đóng lãi, chuộc tài sản và thanh lý hợp đồng thì
                Bên A có quyền hóa giá tài sản thu hồi vốn, Bên B không được
                quyền khiếu nại gì.
              </div>

              <div className="footer-signature">
                <div>ĐẠI DIỆN BÊN B</div>
                <div>ĐẠI DIỆN BÊN A</div>
              </div>
            </div>
          </div>
        </div>
        <hr />
        <div>
            <p>Mã đơn: {dataOrder.id} - {isOrder==1 ? "(VÀNG)" : "(XE)"}</p>
            <p>Tên khách hàng: {dataCustomer.customer_name} - SĐT: {dataCustomer.phone}</p>
            {isOrder==1 ? <p>Tên hàng: <CategoryColumn categoryId={dataOrder.category_id} cate={0}/> - <CategoryColumn categoryId={dataOrder.category_goal_id} cate={1}/> </p> : <p>Tên xe: {dataOrder.product_name}</p> }
            {isOrder==1 ? <p>Cân nặng: {dataOrder.weight_goal} chỉ</p> : <p>Biển số:{dataOrder.license_plate}</p>}
            <p>Số tiền: <FormatCurrency amount={dataOrder.price}/></p>
            <p>Lãi suất: {percent} %</p>
            <p>Ngày cầm: <FormatDate date={dataOrder.order_date}/></p>
          </div>
      </div>
    </>
  );
};
export default OverviewPrint;
