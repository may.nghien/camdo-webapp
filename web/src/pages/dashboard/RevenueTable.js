import { Pagination, Table } from "antd";
import BaseURL from "../../api/baseURL";
import { useEffect, useState } from "react";
import { FormatCurrency } from "../../helpers/Help";
import moment from "moment";
import { Spin } from "antd";

const RevenueTable = ({ month, year, isOrder, isPrint }) => {
  const [dataCollect, setDataCollect] = useState([]);
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    setLoading(true)
    const params = {
      month: month,
      year: year,
    };
    BaseURL
      .post(isOrder == 0 ? "payment/get-by-month-and-year" : "payment/get-vehicle-by-month-and-year", params)
      .then((response) => {
        setDataCollect(response.data);
        setLoading(false)
      });
  }, [month, year]);

  const dataSource = dataCollect && dataCollect.map((item, index) => ({
    key: index,
    ID: item.payments && item.payments.payment_id,
    Amount: item.payments && item.payments.amount,
    OrderID: item.payments && item.payments.order_id,
    PaymentDate: item.payments && item.payments.payment_date,
    Products: isOrder == 0 ? item.products && item.products : item.order,
    OrderDate: item.payments && item.order.OrderDate
  }));

  const [currentPage, setCurrentPage] = useState(1); // Trang hiện tại
  // Số lượng tổng cộng các mục dữ liệu
  const totalItems = dataSource && dataSource.length;
  const pageSize = isPrint == 1 ? totalItems : 3; // Kích thước trang

  const handlePageChange = (page) => {
    setCurrentPage(page);
  };
  // Lấy dữ liệu trang hiện tại
  const currentPageData = dataSource && dataSource.slice(
    (currentPage - 1) * pageSize,
    currentPage * pageSize
  );
  const columns = [
    {
      title: "Mã đơn",
      dataIndex: "OrderID",
      key: "OrderID",
    },
    {
      title: isOrder == 0 ? "Sản phẩm" : "Xe",
      dataIndex: "Products",
      key: "Products",
      render: (products) => (<>
        {isOrder == 0 ? products && products.map((item, index) => (
          <div key={index}>
            <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>{" "}
            (
            {item.DetailProducts &&
              item.DetailProducts.map((detail, index) => {
                // Make sure to return the JSX element here
                return <span key={index}>{detail.Count}{" "}{detail.CategoryName}{index < item.DetailProducts.length - 1 && " + "}</span>;
              })}
            ) - {item.Product.CategoryGoldName} - {item.Product.WeightGoal} chỉ
          </div>
        ))
          :
          <div>{products.product_name} - {products.license_plate}</div>
        }
      </>),
    },
    {
      title: "Ngày cầm",
      dataIndex: "OrderDate",
      key: "OrderDate",
      render: (date) => moment(date).format("DD/MM/YYYY"),
    },
    {
      title: "Ngày đóng lãi",
      dataIndex: "PaymentDate",
      key: "PaymentDate",
      render: (date) => moment(date).format("DD/MM/YYYY"),
    },
    {
      title: "Số tiền",
      dataIndex: "Amount",
      key: "Amount",
      render: (amount) => <FormatCurrency amount={amount} />,
    },
  ];

  return (
    <div className="table-container">
      {loading ? (
        <Spin size="large" />
      ) : (
        <>
          <Table
            dataSource={currentPageData}
            columns={columns}
            pagination={false}
          />
          {isPrint != 1 &&
            <Pagination
              className="pagination-category"
              current={currentPage}
              pageSize={pageSize}
              total={totalItems}
              onChange={handlePageChange}
            />
          }
        </>
      )}
    </div>
  );
};

export default RevenueTable;
