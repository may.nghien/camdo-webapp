import { Modal } from "antd"
import { useEffect, useState } from "react"
import BaseURL from "../../api/baseURL"
import { FormatCurrency } from "../../helpers/Help"
import SpendVehicleToday from "./SpendVehicleToday"
import CollectVehicleToday from "./CollectVehicleToday"
import RevenueVehicleToday from "./RevenueVehicleToday"

const VehicleToday = ({ isOpen, setIsOpen, isTable, setIsTable }) => {
    const [currentDate, setCurrentDate] = useState('');
    const [data, setData]=useState([])
    useEffect(() => {
        // Lấy ngày hiện tại
        const today = new Date();
        const formattedDate = today.toLocaleDateString('en-GB'); // dd/mm/yyyy format for 'en-GB' locale

        // Cập nhật state với ngày hiện tại đã được định dạng
        setCurrentDate(formattedDate);
        BaseURL.get("overview-vehicle-today").then((response)=>{
            if(response.status==200){
                setData(response.data)
            }
        })
    }, []);

    const handleCancel = () => {
        setIsOpen(false)
    }
    return (
        <Modal
            className="overview-today"
            open={isOpen}
            onCancel={handleCancel}
            bodyStyle={{ maxHeight: "500px", overflow: "auto" }}
            width={1000}
        >
            <div className="overview-today-header">TỔNG QUAN HÔM NAY NGÀY {currentDate} </div>
            <div className="overview-today-content">
                <div className="title">Lợi nhuận hôm nay: <FormatCurrency amount={data.Revenue}/></div>
                <div>
                    <RevenueVehicleToday isOrder={0} />
                </div>
            </div>
            <div className="overview-today-content">
                <div className="title">Tổng thu: <FormatCurrency amount={data.Collect}/></div>
                <div>
                    <CollectVehicleToday isTable={isTable} setIsTable={setIsTable} />
                </div>
            </div>
            <div className="overview-today-content">
                <div className="title">Tổng chi: <FormatCurrency amount={data.Spend}/></div>
                <div>
                    <SpendVehicleToday />
                </div>
            </div>
        </Modal>
    )
}
export default VehicleToday