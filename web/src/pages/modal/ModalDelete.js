import { Modal } from "antd";

export const ModalDelete = ({ isOpen, setIsOpen, handleDelete, id }) => {
  const handleCancel = () => {
    setIsOpen(!isOpen);
  };
  return (
    <Modal open={isOpen} onCancel={handleCancel} onOk={() => handleDelete(id)}>
      <div className="modal-delete">
        <i
          class="fa fa-exclamation-circle"
          style={{ fontSize: "30px", color: "red" }}
        ></i>
        <div className="modal-delete-content">Bạn muốn xóa dữ liệu này ?</div>
      </div>
    </Modal>
  );
};

export const ModalDeleteCate = ({ isOpen, setIsOpen, handleDelete, id }) => {
  const handleCancel = () => {
    setIsOpen(!isOpen);
  };
  return (
    <Modal open={isOpen} onCancel={handleCancel} onOk={() => handleDelete(id)}>
      <div className="modal-delete">
        <i
          class="fa fa-exclamation-circle"
          style={{ fontSize: "30px", color: "red" }}
        ></i>
        <div className="modal-delete-content">
          <p>Bạn muốn xóa danh mục này ?</p>
          <p>Xóa danh mục này sẽ xóa những đơn liên quan!</p>
        </div>
      </div>
    </Modal>
  );
};

export const ModalDeleteCustomer = ({ isOpen, setIsOpen, handleDelete, id }) => {
  const handleCancel = () => {
    setIsOpen(!isOpen);
  };
  return (
    <Modal open={isOpen} onCancel={handleCancel} onOk={() => handleDelete(id)}>
      <div className="modal-delete">
        <i
          class="fa fa-exclamation-circle"
          style={{ fontSize: "30px", color: "red" }}
        ></i>
        <div className="modal-delete-content">
          <p>Bạn muốn xóa khách hàng này ?</p>
          <p>Xóa khách hàng này sẽ xóa những đơn liên quan!</p>
        </div>
      </div>
    </Modal>
  );
};
