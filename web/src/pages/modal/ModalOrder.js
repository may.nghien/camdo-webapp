import { Button, Modal, Table } from "antd";
import BaseURL from "../../api/baseURL";
import { useEffect, useState } from "react";
import { CategoryColumn, ConvertCategory, ConvertCategoryGoal, FormatCurrency } from "../../helpers/Help";
import moment from "moment";

const ModalOrder = ({ isOpen, setIsOpen, customerID }) => {
  const [dataOrderGoal, setDataOrderGoal] = useState([]);
  const [dataOrderVehicle, setDataOrderVehicle] = useState([]);
  const [percent, setPercent] = useState([]);

  const convertStatus = (status) => {
    let stt = "";
    if (status === "0") {
      stt = (
        <span style={{ color: "green", fontWeight: "bold" }}>Còn hiệu lực</span>
      );
    } else if (status === "1") {
      stt = (
        <span style={{ color: "red", fontWeight: "bold" }}>
          Đã chuộc tài sản
        </span>
      );
    } else {
      stt = (
        <span style={{ color: "red", fontWeight: "bold" }}>Hồ sơ hóa giá</span>
      );
    }
    return stt;
  };

  const handleCancel = () => {
    setIsOpen(false);
  };
  useEffect(() => {
    BaseURL
      .get(`get-order-goal/customer-id/${customerID}`)
      .then((response) => {
        if (response.status === 200) {
          setDataOrderGoal(response.data);
        }
      })
      .catch((error) => {
        if (error.response) {
          console.log("Server responded with a non-2xx status");
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else if (error.request) {
          console.log("No response received from the server");
          console.log(error.request);
        } else {
          console.log("Error setting up the request");
          console.log(error.message);
        }
        console.log(error.config);
      });


    BaseURL
      .get(
        `get-order-vehicle/customer-id/${customerID}`
      )
      .then((response) => {
        if (response.status === 200) {
          setDataOrderVehicle(response.data);
        }
      })
      .catch((error) => {
        if (error.response) {
          console.log("Server responded with a non-2xx status");
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else if (error.request) {
          console.log("No response received from the server");
          console.log(error.request);
        } else {
          console.log("Error setting up the request");
          console.log(error.message);
        }
        console.log(error.config);
      });
  }, [customerID]);


  useEffect(() => {
    BaseURL
      .get("list-category-percent")
      .then((response) => {
        if (response.status === 200) {
          setPercent(response.data);
        }
      })
      .catch((error) => {
        if (error.response) {
          console.log("Server responded with a non-2xx status");
        } else if (error.request) {
          console.log(error.request);
        } else {
          console.log(error.message);
        }
        console.log(error.config);
      });
  }, []);
  
  const columns = [
    {
      title: "Mã đơn",
      dataIndex: "ID",
      key: "ID",
    },
    {
      title: "Thông tin sản phẩm",
      dataIndex: "CategoryID",
      key: "CategoryID",
      width: 400,
      // responsive: ["xl", "lg", "md", "sm"],
      render: (e) =>
        e &&
        e.map((item, index) => (
          <div key={index}>
            <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>{" "}
            (
            {item.DetailProducts &&
              item.DetailProducts.map((detail, index) => {
                // Make sure to return the JSX element here
                return <span key={index}>{detail.Count}{" "}{detail.CategoryName}{index < item.DetailProducts.length - 1 && " + "}</span>;
              })}
            ) - {item.Product.CategoryGoldName} - {item.Product.WeightGoal} chỉ
          </div>
        ))
    },
    {
      title: "Giá cầm",
      dataIndex: "Price",
      key: "Price",
      render: (amount) => <FormatCurrency amount={amount} />,
    },
    {
      title: "Lãi suất",
      dataIndex: "Percent",
      key: "Percent",
      render: (percent) => convertPercent(percent),
    },
    {
      title: "Trạng thái hồ sơ",
      dataIndex: "Status",
      key: "Status",
      width: 200, // Độ rộng của cột
      render: (stt) => (
        <span style={{ width: "200px" }}>{convertStatus(stt)}</span>
      ),
    },
    {
      title: "Ngày cầm",
      dataIndex: "OrderDate",
      key: "OrderDate",
      render: (date) => moment(date).format("DD/MM/YYYY"),
    },
    {
      title: "Ngày hết hạn",
      dataIndex: "ReturnDate",
      key: "ReturnDate",
      render: (day) => moment(day).format("DD/MM/YYYY"),
    },
  ];

  const convertPercent = (cate) => {
    let name = "";
    percent.map((item) => {
      if (item.PercentID == cate) {
        name = item.Percent;
      }
    });
    return name;
  };

  const columnsVehicle = [
    {
      title: "Mã đơn",
      dataIndex: "ID",
      key: "ID",
    },
    {
      title: "Tên xe",
      dataIndex: "ProductName",
      key: "ProductName",
    },
    {
      title: "Biển số",
      dataIndex: "LicensePlate",
      key: "LicensePlate",
      width: 200,
    },
    {
      title: "Giá cầm",
      dataIndex: "Price",
      key: "Price",
      render: (amount) => <FormatCurrency amount={amount} />,
    },
    {
      title: "Lãi suất",
      dataIndex: "Percent",
      key: "Percent",
      render: (percent) => convertPercent(percent),
    },
    {
      title: "Trạng thái hồ sơ",
      dataIndex: "Status",
      key: "Status",
      width: 200, // Độ rộng của cột
      render: (stt) => (
        <span style={{ width: "200px" }}>{convertStatus(stt)}</span>
      ),
    },
    {
      title: "Ngày cầm",
      dataIndex: "OrderDate",
      key: "OrderDate",
      render: (date) => moment(date).format("DD/MM/YYYY"),
    },
    {
      title: "Ngày hết hạn",
      dataIndex: "ReturnDate",
      key: "ReturnDate",
      render: (day) => moment(day).format("DD/MM/YYYY"),
    },
  ];
  // const dataSourceGoal = dataOrderGoal.map((item, index) => ({
  //   key: index,
  //   ID: item.id,
  //   CustomerID: item.customer_id,
  //   ProductName: item.product_name,
  //   CategoryID: item.category_id,
  //   CategoryGoalID: item.category_goal_id,
  //   EstimatedPrice: item.estimated_price,
  //   Price: item.price,
  //   ReturnDate: item.return_date,
  //   WeightGoal: item.weight_goal,
  //   Status: item.status,
  //   Percent: item.percent,
  //   OrderDate: item.order_date,
  // }));
  const dataSourceGoal =
    dataOrderGoal && dataOrderGoal.map((item, index) => ({
      key: index,
      ID: item.order.ID,
      CustomerID: item.order.CustomerID,
      ProductName: item.order.ProductName,
      CategoryID: item.products && item.products,
      AddPayment: item.add_payments,
      Payments: item.payments,
      EstimatedPrice: item.order.EstimatedPrice,
      Price: item.order.Price,
      PriceOld: item.order.PriceOld,
      ReturnDate: item.order.ReturnDate,
      Note: item.order.Note,
      Miss: item.order.Miss,
      WeightGoal: item.order.WeightGoal,
      Status: item.order.Status,
      Percent: item.order.Percent,
      OrderDate: item.order.OrderDate,
      interestAmount: item.interestAmount,
    }));
  const dataSourceVehicle = dataOrderVehicle.map((item, index) => ({
    key: index,
    ID: item.id,
    CustomerID: item.customer_id,
    ProductName: item.product_name,
    EstimatedPrice: item.estimated_price,
    LicensePlate: item.license_plate,
    Price: item.price,
    ReturnDate: item.return_date,
    Status: item.status,
    Percent: item.percent,
    OrderDate: item.order_date,
  }));
  return (
    <>
      <Modal
        bodyStyle={{ maxHeight: "500px", overflow: "auto" }}
        width={1000}
        open={isOpen}
        onCancel={handleCancel}
        title={<div>Mã khách hàng : {customerID}</div>}
      >
        {dataOrderGoal.length != 0 && (
          <>
            <div style={{ fontSize: "18px" }}>Đơn cầm vàng</div>
            <Table
              dataSource={dataSourceGoal}
              columns={columns}
              pagination={false}
            />
          </>
        )}
        {dataOrderVehicle.length != 0 && (
          <>
            <div style={{ fontSize: "18px" }}>Đơn cầm xe</div>
            <Table
              dataSource={dataSourceVehicle}
              columns={columnsVehicle}
              pagination={false}
            />
          </>
        )}
      </Modal>
    </>
  );
};
export default ModalOrder;
