import { Button, Modal } from "antd";
import BaseURL from "../../api/baseURL";
import { useEffect, useRef, useState } from "react";
import { FormatCurrency, FormatDate, NumberToWords } from "../../helpers/Help";
import dayjs from "dayjs";

const ModalOrderPrint = ({ isOpen, setIsOpen, orderID }) => {
  const contentRef = useRef(null);
  const contentRef2 = useRef(null);
  const [dataOrder, setDataOrder] = useState([]);
  const [dataCustomer, setDataCustomer] = useState([]);
  const handleCancel = () => {
    setIsOpen(false);
  };
  const handleOk = () => {
    setIsOpen(false);
    const contentToPrint = contentRef.current.innerHTML;

    // Sử dụng nội dung để in
    const printWindow = window.open("", "_blank");
    printWindow.document.open();
    printWindow.document.write(
      "<html><head><style>" +
      getPrintStyles() +
      "@media print { @page { size: A5; } }" + // Thay đổi kích thước giấy in thành A5
      ".img-container { position: absolute; top: 0; right: 0; }" + // Định dạng container chứa ảnh
      ".order-id-line-customer { position: absolute; top: 40; left: 0; }" + 
      "</style></head><body>" +
      '<div class="img-container"><img src="./KH.png" alt="Mô tả ảnh" width="120px" /></div>' + // Thêm container chứa ảnh và thuộc tính width để điều chỉnh kích thước ảnh
      contentToPrint +
      "</body></html>"
    );

    printWindow.document.close();
    printWindow.print();

  };
  const formatDate = (date) => {
    const dateObject = new Date(date);

    const day = dateObject.getDate();
    const month = dateObject.getMonth() + 1;
    const year = dateObject.getFullYear();

    return `${day < 10 ? "0" + day : day}/${month < 10 ? "0" + month : month
      }/${year}`;
  };
  const GetYear = (day) => {
    // Sử dụng Day.js để chuyển đổi chuỗi thành đối tượng Day.js
    const date = dayjs(day);

    // Lấy năm
    const year = date.year();
    return year;
  };
  const getPrintStyles = () => {
    // Lấy nội dung SCSS và biên dịch thành CSS
    const cssContent = `
        .modal-order-print{
          width: 148mm !important;
          border-radius: 0%; 
          }
          .modal-order-print-content{
            padding-bottom:5px;
            font-size:13px;
            color: #4169E1;
          }
        .modal-order-print .ant-modal-content{
            border-radius: 0%;
        }
        .modal-order-print .modal-order-print-content{
            font-size: 15px;
        }
        .modal-order-print-content .modal-title{
            text-align: center;
            font-weight: bold;     
        }
        .modal-title .label-name{
                font-size: 12.5px;
            }
        .modal-body .modal-body-content{
            padding: 3px;
        }
        
        .modal-body-content .content-label{
            text-decoration:underline;
            font-weight: bold;
            font-size: 13px;
        }
        
        .modal-print-footer{
          padding: 5px 0px; 
        }
        .modal-print-footer .footer-signature{
          display: flex;
          gap:200px;
          padding:8px 5px;
        }
        .print-line-admin{
          font-size:12px;
          padding-top:50px;
        }
        b{
          color: black;
        }
        .c-red{
          color: red;
        }
        .d-flex{
            display: flex;
            gap: 2px;
        }
        `;

    return cssContent;
  };
  const [percentID, setPercentID] = useState("")
  const [percent, setPercent] = useState("")
  useEffect(() => {
    BaseURL
      .get(`get-order-vehicle/${orderID}`)
      .then((response) => {
        if (response.status === 200) {
          setDataOrder(response.data);
          setPercentID(response.data.percent)
        }
      })
      .catch((error) => {
        if (error.response) {
          console.log("Server responded with a non-2xx status");
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else if (error.request) {
          console.log("No response received from the server");
          console.log(error.request);
        } else {
          console.log("Error setting up the request");
          console.log(error.message);
        }
        console.log(error.config);
      });
  }, [isOpen]);
  useEffect(() => {
    BaseURL
      .get(`list-category-percent/${percentID}`)
      .then((response) => {
        if (response.status === 200) {
          setPercent(response.data.Percent)
        }
      })
      .catch((error) => {
        if (error.response) {
          console.log("Server responded with a non-2xx status");
        } else if (error.request) {
          console.log(error.request);
        } else {
          console.log(error.message);
        }
        console.log(error.config);
      });
  }, [percentID])
  useEffect(() => {
    BaseURL
      .get(`list-customer/${dataOrder.customer_id}`)
      .then((response) => {
        if (response.status === 200) {
          setDataCustomer(response.data);
        }
      })
      .catch((error) => {
        if (error.response) {
          console.log("Server responded with a non-2xx status");
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else if (error.request) {
          console.log("No response received from the server");
          console.log(error.request);
        } else {
          console.log("Error setting up the request");
          console.log(error.message);
        }
        console.log(error.config);
      });
  }, [dataOrder]);
  return (
    <>
      <Modal
        open={isOpen}
        className="modal-order-print"
        width={700}
        onCancel={handleCancel}
        zIndex={2040}
        footer={[
          <>
            <Button key="print" onClick={handleCancel}>
              Đóng
            </Button>
            <Button key="print" type="primary" onClick={handleOk}>
              In
            </Button>
          </>,
        ]}
      >
        <div ref={contentRef}>
        <div className="order-id-line-customer">{dataOrder && dataOrder.id} - (XE)</div>
          <div className="modal-order-print-content">
            <div className="modal-title">
              <p>CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM</p>
              <p>Độc lập - Tự do - Hạnh phúc</p>
              <div style={{ fontSize: "18px" }} className="label-name">
                HỢP ĐỒNG CẦM CỐ TÀI SẢN
              </div>
            </div>
            <div className="modal-body">
              <div className="modal-body-content">
                <div className="content-label">Bên A:</div>
                <div>
                  Họ và tên:<b> ÂU KIM HÒA</b>, Năm sinh: <b>1961</b>
                </div>
                <div className="d-flex">
                  Là đại diện cơ sở:
                  <b className="d-flex"> Dịch Vụ Cầm Đồ <div className="c-red"> KHẢI HOÀN</div> - ĐT: 0293.3848329</b>
                </div>
                <div>
                  Địa chỉ: <b>105 Quốc lộ 61 Cái Tắc - Hậu Giang</b>
                </div>
              </div>
              <div className="modal-body-content">
                <div className="content-label">Bên B:</div>
                <div>
                  Họ và tên:<b>{dataCustomer.customer_name}</b>, Năm sinh:{" "}
                  <b>{GetYear(dataCustomer.birth_date)}</b>
                </div>
                <div>
                  Nơi đăng ký HKTT:<b>{dataCustomer.address}</b>
                </div>
                <div>
                  Số CMND/CCCD:<b>{dataCustomer.cccd}</b> Ngày cấp:
                  <b>
                    {dataCustomer.day_cccd ? (
                      <FormatDate date={dataCustomer.day_cccd} />
                    ) : (
                      "..............."
                    )}
                  </b>{" "}
                  Nơi cấp:
                  <b>
                    {dataCustomer.address_cccd
                      ? dataCustomer.address_cccd
                      : "..............."}
                  </b>
                </div>
              </div>
              <div className="modal-body-content">
                <div className="content-label">NỘI DUNG:</div>
                <div>
                  Bên A nhận cầm tài sản Bên B mang đến, thống nhất thỏa thuận
                  như sau:
                </div>
                <div>
                  - Tên tài sản cầm cố:
                  <b>{dataOrder.product_name}{" "}</b>
                  Biển số: <b>{dataOrder.license_plate}</b>
                </div>
                <div>
                  - Giá trị tài sản cầm cố ước tính:
                  <b>
                    {/* <FormatCurrency amount={dataOrder.estimated_price} /> */}
                  </b>
                </div>
                <div>
                  - Số tiền cầm cố:{" "}
                  <b>
                    <FormatCurrency amount={dataOrder.price} />
                  </b>
                  Ghi bằng chữ:{" "}
                  <b>
                    {" "}
                    <NumberToWords amount={dataOrder.price} /> đồng
                  </b>
                </div>
                <div>- Cầm thêm: </div>
                <div>
                  - Lãi suất: <b>{percent} %</b>
                </div>
                <div>
                  - Đóng lãi:
                </div>
                <div>
                  - Thời gian cầm cố tài sản từ ngày:
                  <b>{formatDate(dataOrder.order_date)}</b> Đến ngày:
                  <b>{formatDate(dataOrder.return_date)}</b>
                </div>
              </div>
              <div className="modal-print-footer">
                <div>
                  Sau thời hạn 60 ngày (sáu mươi), kể từ ngày:
                  <b>
                    <FormatDate date={dataOrder.order_date} />
                  </b>
                  ,Bên B không đóng lãi, chuộc tài sản và thanh lý hợp đồng thì
                  Bên A có quyền hóa giá tài sản thu hồi vốn, Bên B không được
                  quyền khiếu nại gì.
                </div>

                <div className="footer-signature">
                  <div>ĐẠI DIỆN BÊN B</div>
                  <div>ĐẠI DIỆN BÊN A</div>
                </div>
              </div>
            </div>
          </div>
          <div className="print-line-admin">
            <p>Mã đơn: <b>{dataOrder.id} - (XE)</b></p>
            <p>Tên khách hàng: <b>{dataCustomer.customer_name} - SĐT: {dataCustomer.phone}</b></p>
            <p>Tên xe: <b>{dataOrder.product_name}</b></p>
            <p>Biển số:<b>{dataOrder.license_plate}</b></p>
            <p>Số tiền: <b><FormatCurrency amount={dataOrder.price} /></b> - Lãi suất: <b>{percent} %</b></p>
            <p>Ngày cầm: <b><FormatDate date={dataOrder.order_date} /></b></p>
          </div>
        </div>
      </Modal>
    </>
  );
};
export default ModalOrderPrint;
