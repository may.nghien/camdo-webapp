import BaseURL from "../../api/baseURL";
import { useEffect, useState } from "react";
import DrawerEditOrder from "../order_goal/DrawerEditOrder";
import DrawerDetail from "../order_goal/DrawerDetail";
import { ModalDelete } from "../modal/ModalDelete";
import PayGoal from "../pay/PayGoal";
import { Button, Pagination, Select, Table } from "antd";
import RedeemOrderGoal from "../pay/RedeemOrderGoal";
import moment from "moment";
import { FormatCurrency, FormatDate } from "../../helpers/Help";
import AddPaymentGoal from "../add/AddPaymentGoal";
import UpdateNote from "../order_vehicle/UpdateNote";
import { Spin } from "antd";

const OverOrdersVehicle = ({ searchContent, resetFlag, setResetFlag, daysDelayed }) => {
    const [loading, setLoading] = useState(true);
    const [data, setData] = useState([]);
    const [orderID, setOrderID] = useState("");
    const [modalPay, setModalPay] = useState(false);
    const [percent, setPercent] = useState([]);
    const [isUpdateNote, setIsUpdateNote] = useState(false)
    const [openModalAddPayment, setOpenModalAddPayment] = useState(false);
    //Thanh toan
    const [redeemPay, setRedeemPay] = useState(false);
    //Xoa
    const [openModalDelete, setOpenModalDelete] = useState(false);
    //Sua
    const [openDrawerEdit, setOpenDrawerEdit] = useState(false);
    //Chi tiet
    const [openDrawer, setOpenDrawer] = useState(false);
    const isOverdue = (returnDate) => {
        const currentDate = new Date();
        const returnDateObj = new Date(returnDate);
        const diffInDays = Math.floor(
            (currentDate - returnDateObj) / (1000 * 60 * 60 * 24)
        );
        return diffInDays;
    };
    const filterDelayedOrders = (data) => {
        // Lấy ngày hiện tại
        const currentDate = new Date();

        // Lọc các đơn hàng trễ theo số ngày được chọn
        const filteredOrders = data.filter((item) => {
            // Tính số ngày trễ từ ngày trả hàng
            const returnDateObj = new Date(item.ReturnDate);
            const diffInDays = Math.floor((currentDate - returnDateObj) / (1000 * 60 * 60 * 24));
            // Kiểm tra xem số ngày trễ có nằm trong số ngày được chọn hay không
            return (
                daysDelayed === 0 || // Chọn Tất cả, không áp dụng bộ lọc
                (daysDelayed === 1 && diffInDays < 30) ||
                (daysDelayed === 180 && diffInDays > 150) ||
                (diffInDays >= daysDelayed && diffInDays < daysDelayed + 30)
            );
        });

        return filteredOrders;
    };

    const isOver60d = (returnDate) => {
        const currentDate = new Date();
        const returnDateObj = new Date(returnDate);
        const diffInDays2 = Math.floor(
            (currentDate - returnDateObj.setMonth(returnDateObj.getMonth() - 1)) /
            (1000 * 60 * 60 * 24)
        );
        if (diffInDays2 >= 60) {
            return 1;
        } else return 0;
    };
    const convertPercent = (cate) => {
        let name = "";
        percent.map((item) => {
            if (item.PercentID == cate) {
                name = item.Percent;
            }
        });
        return name;
    };

    const convertStatus = (status) => {
        let stt = "";
        if (status === "0") {
            stt = (
                <span style={{ color: "green", fontWeight: "bold" }}>
                    Còn hiệu lực{" "}
                </span>
            );
        } else if (status === "1") {
            stt = (
                <span style={{ color: "#86518a", fontWeight: "bold" }}>
                    Đã chuộc tài sản
                </span>
            );
        } else {
            stt = (
                <span style={{ color: "red", fontWeight: "bold" }}>Hồ sơ hóa giá</span>
            );
        }
        return stt;
    };

    useEffect(() => {
        searchContent == "" ? fetchData() : fetchDataSearch();
        setCurrentPage(1); // Đặt lại trang hiện tại về trang đầu tiên sau khi filter
    }, [resetFlag, searchContent, daysDelayed]);

    useEffect(() => {
        BaseURL
            .get("list-category-percent")
            .then((response) => {
                if (response.status === 200) {
                    setPercent(response.data);
                }
            })
            .catch((error) => {
                if (error.response) {
                    console.log("Server responded with a non-2xx status");
                } else if (error.request) {
                    console.log(error.request);
                } else {
                    console.log(error.message);
                }
                console.log(error.config);
            });
    }, []);

    const fetchDataSearch = async () => {
        setLoading(true);
        try {
            const response1 = await BaseURL.post("search-order-vehicle-over",
                {
                    id: parseInt(searchContent),
                });
            const response2 = await BaseURL.post("payment/search-order-vehicle-over",
                {
                    id: parseInt(searchContent),
                });
            // Lấy dữ liệu từ response1 và response2
            const dataFromApi1 = response1.data;
            const dataFromApi2 = response2.data;

            // Kết hợp dữ liệu từ hai API lại với nhau
            let combinedData = [];

            if (dataFromApi1 && Array.isArray(dataFromApi1)) {
                combinedData = [...combinedData, ...dataFromApi1];
            }

            if (dataFromApi2 && Array.isArray(dataFromApi2)) {
                combinedData = [...combinedData, ...dataFromApi2];
            }
            setLoading(false);
            setData(combinedData);
        } catch (error) {
            console.error("Error fetching data:", error);
        }
    };
    console.log(data);
    const fetchData = async () => {
        setLoading(true);
        try {
            const response1 = await BaseURL.get("over/get-order-vehicle");
            const response2 = await BaseURL.get("over/get-order-vehicle-have-payments");

            // Lấy dữ liệu từ response1 và response2
            const dataFromApi1 = response1.data;
            const dataFromApi2 = response2.data;

            // Kết hợp dữ liệu từ hai API lại với nhau
            let combinedData = [];

            if (dataFromApi1 && Array.isArray(dataFromApi1)) {
                combinedData = [...combinedData, ...dataFromApi1];
            }

            if (dataFromApi2 && Array.isArray(dataFromApi2)) {
                combinedData = [...combinedData, ...dataFromApi2];
            }
            setLoading(false);
            setData(combinedData);
        } catch (error) {
            console.error("Error fetching data:", error);
        }
    };
    const renderNote = (note, record) => {
        return (
            <div className="note">
                <div>
                    {note != null && (<div className="note-content"><div>{record.Miss == 1 && "Khách mất giấy"}</div>{note}</div>)}
                    <div className="note-update">
                        <Button onClick={() => {
                            setOrderID(record.ID);
                            setIsUpdateNote(true)
                        }}>
                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                        </Button>
                    </div>
                </div>
            </div>)
    }
    const dataSource = data && data.map((item, index) => ({
        key: index,
        ID: item.order.ID,
        CustomerID: item.order.CustomerID,
        ProductName: item.order.ProductName,
        LicensePlate: item.order.LicensePlate,
        AddPayment: item.add_payments,
        Payments: item.payments,
        EstimatedPrice: item.order.EstimatedPrice,
        Price: item.order.Price,
        PriceOld: item.order.PriceOld,
        ReturnDate: item.payments == null ? item.order.ReturnDate : item.payments[item.payments.length - 1].NextPaymentDate,
        Note: item.order.Note,
        Miss: item.order.Miss,
        Status: item.order.Status,
        Percent: item.order.Percent,
        OrderDate: item.order.OrderDate,
        interestAmount: item.interestAmount,
    }))

    const [currentPage, setCurrentPage] = useState(1); // Trang hiện tại
    const pageSize = 8; // Kích thước trang
    // Số lượng tổng cộng các mục dữ liệu
    const totalItems = dataSource && dataSource.length;
    const handlePageChange = (page) => {
        setCurrentPage(page);
    };
    // Lấy dữ liệu trang hiện tại
    // const currentPageData = dataSource && dataSource.slice(
    //     (currentPage - 1) * pageSize,
    //     currentPage * pageSize
    // );
    const currentPageData = filterDelayedOrders(dataSource).slice(
        (currentPage - 1) * pageSize,
        currentPage * pageSize
    );

    const handleSelectAction = (e, id) => {
        const params = {
            id: id,
            status: "2",
        };
        if (e === 0) {
            setOrderID(id);
            setOpenDrawer(true);
        } else if (e === 1) {
            setOrderID(id);
            setOpenDrawerEdit(true);
        } else if (e === 2) {
            setOrderID(id);
            setOpenModalDelete(true);
        } else if (e === 3) {
            BaseURL
                .put(
                    "payment/update-status-order-goal",
                    params
                )
                .then((response) => {
                    setResetFlag(resetFlag + 1);
                });
        }
    };
    const handleDelete = (id) => {
        BaseURL
            .delete(`delete-order-goal/${id}`)
            .then((response) => {
                if (response.status === 200) {
                    setResetFlag(resetFlag + 1);
                    setOpenModalDelete(false);
                }
            });
    };
    const columns = [
        {
            title: "Mã đơn",
            dataIndex: "ID",
            key: "ID",
        },
        {
            title: "Tên xe",
            dataIndex: "ProductName",
            key: "ProductName",
            responsive: ["xl", "lg", "md", "sm"],
        },
        {
            title: "Biển số",
            dataIndex: "LicensePlate",
            key: "LicensePlate",
            width: 200,
            responsive: ["xl", "lg", "md", "sm"],
        },
        {
            title: "Giá cầm",
            dataIndex: "Price",
            key: "Price",
            width: 250,
            render: (amount, record) =>
            (
                <div className="add-payment">
                    <div className="add-payment-new-amount"><FormatCurrency amount={record.Price} /></div>
                    {record.AddPayment != null &&
                        (<div className="add-payment-old-amount">Tiền gốc: <FormatCurrency amount={record.PriceOld} /></div>)}
                    {record.AddPayment && record.AddPayment.map((item) => <div>Cầm thêm <FormatCurrency amount={item.Amount} /> ngày <FormatDate date={item.DateAddPayment} /></div>)}
                </div>
            )
        },
        {
            title: "Lãi suất",
            dataIndex: "Percent",
            key: "Percent",
            responsive: ["xl", "lg", "md", "sm"],
            render: (percent) => convertPercent(percent),
        },
        {
            title: "Trạng thái hồ sơ",
            dataIndex: "Status",
            key: "Status",
            width: 250, // Độ rộng của cột
            render: (stt) => (
                <span style={{ width: "200px" }}>{convertStatus(stt)}</span>
            ),
        },
        {
            title: "Ngày cầm",
            dataIndex: "OrderDate",
            key: "OrderDate",
            render: (date) => moment(date).format("DD/MM/YYYY"),
        },

        {
            title: "Thông tin đóng lãi",
            dataIndex: "interestAmount",
            key: "interestAmount",
            render: (item, record) => (
                <div>
                    {record.Status == "0" && (
                        <ul className="ul-payments-info">
                            {record.Payments && record.Payments != null ? (
                                record.Payments.map((interest) => (
                                    <li key={interest.ID}>
                                        <div className="li-date">
                                            <FormatDate date={interest.PaymentDate} />
                                        </div>
                                        <div className="li-amount">
                                            <FormatCurrency amount={interest.Amount} />{" "}
                                        </div>
                                    </li>
                                ))
                            ) : (
                                <li>Chưa đóng lãi</li>
                            )}
                        </ul>
                    )}
                </div>
            ),
        },
        {
            title: "Ngày tới hạn",
            dataIndex: "ReturnDate",
            key: "ReturnDate",
            width: 280, // Độ rộng của cột
            render: (date, record) => (
                <div className="return-date-over">
                    <FormatDate date={date} />
                    <div>
                        Trễ{" "}
                        {isOverdue(date)}
                        {" "}ngày
                    </div>
                    <div className="expired-order">
                        {isOver60d(date) > 0 && "==>  Hóa giá"}
                    </div>
                </div>
            ),
        },
        {
            title: "Ghi chú",
            dataIndex: "Note",
            key: "Note",
            render: (note, record) => renderNote(note, record),
        },
        {
            title: "",
            dataIndex: "ID",
            key: "ID",
            render: (id) => (
                <div className="button-action">
                    <Button
                        className="button-action-detail"
                        onClick={() => {
                            setOrderID(id);
                            setModalPay(true);
                        }}
                    >
                        Đóng lãi
                    </Button>
                    <Button
                        className="button-action-edit"
                        onClick={() => {
                            setOrderID(id);
                            setRedeemPay(true);
                        }}
                    >
                        Thanh toán
                    </Button>
                    <Button
                        className="button-action-payment"
                        onClick={() => {
                            setOrderID(id);
                            setOpenModalAddPayment(true);
                        }}
                    >
                        Cầm thêm
                    </Button>
                    <Select
                        defaultValue="Thao tác"
                        className="select-search-action"
                        onChange={(e) => handleSelectAction(e, id)}
                    >
                        <Select.Option value={0}>Chi tiết</Select.Option>
                        <Select.Option value={1}>Sửa</Select.Option>
                        <Select.Option value={2}>Xóa</Select.Option>
                        <Select.Option value={3}>Hóa giá</Select.Option>
                    </Select>
                </div>
            ),
        },
    ];

    return (
        <>
            {loading ? (
                <Spin size="large" /> // Hiển thị loading khi loading = true
            ) :
                <>
                    <Table
                        columns={columns}
                        dataSource={filterDelayedOrders(currentPageData)}
                        pagination={false}
                    />
                    <Pagination
                        className="pagination-category"
                        current={currentPage}
                        pageSize={pageSize}
                        total={totalItems}
                        onChange={handlePageChange}
                    />
                </>
            }
            {modalPay && (
                <PayGoal
                    isOpen={modalPay}
                    setIsOpen={setModalPay}
                    orderID={orderID}
                    resetFlag={resetFlag}
                    setResetFlag={setResetFlag}
                />
            )}
            {openDrawer && (
                <DrawerDetail
                    open={openDrawer}
                    setOpen={setOpenDrawer}
                    orderID={orderID}
                />
            )}
            {openDrawerEdit && (
                <DrawerEditOrder
                    isOpen={openDrawerEdit}
                    setIsOpen={setOpenDrawerEdit}
                    orderID={orderID}
                    resetFlag={resetFlag}
                    setResetFlag={setResetFlag}
                />
            )}
            {openModalDelete && (
                <ModalDelete
                    isOpen={openModalDelete}
                    setIsOpen={setOpenModalDelete}
                    handleDelete={handleDelete}
                    id={orderID}
                />
            )}
            {redeemPay && (
                <RedeemOrderGoal
                    isOpen={redeemPay}
                    setIsOpen={setRedeemPay}
                    orderID={orderID}
                    resetFlag={resetFlag}
                    setResetFlag={setResetFlag}
                />
            )}
            {openModalAddPayment && (
                <AddPaymentGoal
                    isOpen={openModalAddPayment}
                    setIsOpen={setOpenModalAddPayment}
                    orderID={orderID}
                    resetFlag={resetFlag}
                    setResetFlag={setResetFlag}
                />
            )}
            {isUpdateNote && (
                <UpdateNote
                    isOpen={isUpdateNote}
                    setIsOpen={setIsUpdateNote}
                    id={orderID}
                    resetFlag={resetFlag}
                    setResetFlag={setResetFlag}
                />
            )}
        </>
    );
}
export default OverOrdersVehicle;
