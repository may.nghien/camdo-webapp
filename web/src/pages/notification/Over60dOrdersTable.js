import { useEffect, useState } from "react";
import { Button, Pagination, Popover, Table } from "antd";
import { InfoCircleOutlined } from "@ant-design/icons";
import BaseURL from "../../api/baseURL";
import moment from "moment";
import {ModalDelete} from "../modal/ModalDelete";
import DrawerEditOrder from "../order_goal/DrawerEditOrder";
import DrawerDetail from "../order_goal/DrawerDetail";
import PayGoal from "../pay/PayGoal";
import { FormatCurrency } from "../../helpers/Help";
import DrawerEditVehicle from "../order_vehicle/DrawerEditVehicle";
import DrawerDetailVehicle from "../order_vehicle/DrawerDetailVehicle";

const Over60dOrdersTable = ({ isOrder }) => {
  const [active, setActive] = useState(4);
  const [subMenu, setSubMenu] = useState(true);
  const [orderID, setOrderID] = useState("");
  const [data, setData] = useState([]);
  const [popoverContent, setPopoverContent] = useState({});
  const [modalPay, setModalPay] = useState(false);
  const [resetFlag, setResetFlag] = useState(1);
  const [openDrawer, setOpenDrawer] = useState(false);
  const [openDrawerEdit, setOpenDrawerEdit] = useState(false);
  const [openModalDelete, setOpenModalDelete] = useState(false);
  const [dataCateGoal, setDataCateGoal] = useState([]);
  const [dataCate, setDataCate] = useState([]);
  const [percent, setPercent] = useState([]);
  const formatCurrency = (amount) => {
    const formatter = new Intl.NumberFormat("vi-VN", {
      style: "currency",
      currency: "VND",
      minimumFractionDigits: 0,
    });

    return formatter.format(amount);
  };
  const fetchData = async (id) => {
    try {
      const response = await BaseURL.get(
        `list-customer/${id}`
      );
      const customerData = response.data;

      // Xây dựng nội dung popover từ kết quả API
      const content = (
        <div className="content-customer">
          <ul>
            <li>
              <b>Tên:</b> <div>{customerData.customer_name}</div>
            </li>
            <li>
              <b>CCCD:</b> <div>{customerData.cccd}</div>
            </li>
            <li>
              <b>SĐT:</b> <div>{customerData.phone}</div>
            </li>
            <li>
              <b>Địa chỉ:</b>
              <div>{customerData.address}</div>{" "}
            </li>
          </ul>
        </div>
      );

      // Cập nhật nội dung popover
      setPopoverContent({ [id]: content });
    } catch (error) {
      console.error("Error fetching customer data:", error);
    }
  };

  const dataSource =
    isOrder == 0
      ? data.map((item, index) => ({
          key: index,
          ID: item.id,
          CustomerID: item.customer_id,
          ProductName: item.product_name,
          CategoryID: item.category_id,
          CategoryGoalID: item.category_goal_id,
          EstimatedPrice: item.estimated_price,
          Price: item.price,
          ReturnDate: item.return_date,
          WeightGoal: item.weight_goal,
          Status: item.status,
          Percent: item.percent,
          OrderDate: item.order_date,
        }))
      : data.map((item, index) => ({
          key: index,
          ID: item.id,
          CustomerID: item.customer_id,
          ProductName: item.product_name,
          LicensePlate: item.license_plate,
          EstimatedPrice: item.estimated_price,
          Price: item.price,
          ReturnDate: item.return_date,
          Status: item.status,
          Percent: item.percent,
          OrderDate: item.order_date,
        }));

  const [currentPage, setCurrentPage] = useState(1); // Trang hiện tại
  const pageSize = 8; // Kích thước trang
  // Số lượng tổng cộng các mục dữ liệu
  const totalItems = dataSource.length;
  const handlePageChange = (page) => {
    setCurrentPage(page);
  };
  // Lấy dữ liệu trang hiện tại
  const currentPageData = dataSource.slice(
    (currentPage - 1) * pageSize,
    currentPage * pageSize
  );
  useEffect(() => {
    BaseURL
      .get(`list-category-goal`)
      .then((response) => {
        if (response.status === 200) {
          setDataCateGoal(response.data);
        }
      })
      .catch((error) => {
        if (error.response) {
          console.log("Server responded with a non-2xx status");
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else if (error.request) {
          console.log("No response received from the server");
          console.log(error.request);
        } else {
          console.log("Error setting up the request");
          console.log(error.message);
        }
        console.log(error.config);
      });
    BaseURL
      .get("list-category")
      .then((response) => {
        if (response.status === 200) {
          setDataCate(response.data);
        }
      })
      .catch((error) => {
        if (error.response) {
          console.log("Server responded with a non-2xx status");
        } else if (error.request) {
          console.log(error.request);
        } else {
          console.log(error.message);
        }
        console.log(error.config);
      });
    BaseURL
      .get("list-category-percent")
      .then((response) => {
        if (response.status === 200) {
          setPercent(response.data);
        }
      })
      .catch((error) => {
        if (error.response) {
          console.log("Server responded with a non-2xx status");
        } else if (error.request) {
          console.log(error.request);
        } else {
          console.log(error.message);
        }
        console.log(error.config);
      });
  }, []);
  const convertDataGoalCate = (cate) => {
    let name = "";
    dataCateGoal.map((item) => {
      if (item.CategoryID == cate) {
        name = item.CategoryName;
      }
    });
    return name;
  };
  const convertPercent = (cate) => {
    let name = "";
    percent.map((item) => {
      if (item.PercentID == cate) {
        name = item.Percent;
      }
    });
    return name;
  };

  const convertDataCate = (cate) => {
    let name = "";
    dataCate.map((item) => {
      if (item.CategoryID == cate) {
        name = item.CategoryName;
      }
    });
    return name;
  };
  const convertStatus = (status) => {
    let stt = "";
    if (status === "0") {
      stt = (
        <span style={{ color: "green", fontWeight: "bold" }}>
          Còn hiệu lực{" "}
        </span>
      );
    } else if (status === "1") {
      stt = (
        <span style={{ color: "#86518a", fontWeight: "bold" }}>
          Đã chuộc tài sản
        </span>
      );
    } else {
      stt = (
        <span style={{ color: "red", fontWeight: "bold" }}>Hồ sơ hóa giá</span>
      );
    }
    return stt;
  };
  useEffect(() => {
    BaseURL
      .get(
        isOrder == 0
          ? "get-order-goal-over-60d"
          : "get-order-vehicle-over-60d"
      )
      .then((response) => {
        if (response.status === 200) {
          setData(response.data);
        }
      })
      .catch((error) => {
        if (error.response) {
          console.log("Server responded with a non-2xx status");
        } else if (error.request) {
          console.log(error.request);
        } else {
          console.log(error.message);
        }
        console.log(error.config);
      });
  }, [resetFlag, isOrder]);

  const columns =
    isOrder == 0
      ? [
          {
            title: "Mã đơn",
            dataIndex: "ID",
            key: "ID",
          },
          {
            title: "Mã khách hàng",
            dataIndex: "CustomerID",
            key: "CustomerID",
            render: (id) => (
              <div className="info-id-customer">
                <div className="info-id-customer-id">{id}</div>
                <div className="info-id-customer-icon">
                  <Popover
                    content={popoverContent[id]}
                    trigger="hover"
                    title="Thông tin chi tiết khách hàng:"
                  >
                    <InfoCircleOutlined onMouseEnter={() => fetchData(id)} />
                  </Popover>
                </div>
              </div>
            ),
          },
          {
            title: "Tên hàng",
            dataIndex: "ProductName",
            key: "ProductName",
            width: 200,
          },
          {
            title: "Loại hàng",
            dataIndex: "CategoryID",
            key: "CategoryID",
            render: (id) => convertDataCate(id),
          },
          {
            title: "Loại vàng",
            dataIndex: "CategoryGoalID",
            key: "CategoryGoalID",
            render: (id) => convertDataGoalCate(id),
          },
          {
            title: "Giá ước lượng",
            dataIndex: "EstimatedPrice",
            key: "EstimatedPrice",
            render: (amount) => formatCurrency(amount),
          },
          {
            title: "Giá cầm",
            dataIndex: "Price",
            key: "Price",
            render: (amount) => formatCurrency(amount),
          },
          {
            title: "Cân nặng",
            dataIndex: "WeightGoal",
            key: "WeightGoal",
          },
          {
            title: "Lãi suất",
            dataIndex: "Percent",
            key: "Percent",
            render: (percent) => convertPercent(percent),
          },
          {
            title: "Trạng thái hồ sơ",
            dataIndex: "Status",
            key: "Status",
            width: 250, // Độ rộng của cột
            render: (stt) => (
              <span style={{ width: "200px" }}>{convertStatus(stt)}</span>
            ),
          },
          {
            title: "Ngày cầm",
            dataIndex: "OrderDate",
            key: "OrderDate",
            render: (date) => moment(date).format("DD/MM/YYYY"),
          },
          {
            title: "Ngày hết hạn",
            dataIndex: "ReturnDate",
            key: "ReturnDate",
            render: (day) => (
              <div style={{ color: "red", fontWeight: "bold" }}>
                {moment(day).format("DD/MM/YYYY")}
              </div>
            ),
          },
          {
            title: "",
            dataIndex: "ID",
            key: "ID",
            render: (id) => (
              <div className="button-action">
                <Button
                  className="button-action-edit"
                  onClick={() => {
                    setOrderID(id);
                    setOpenDrawerEdit(true);
                  }}
                >
                  Sửa
                </Button>
                <Button
                  className="button-action-detail"
                  onClick={() => {
                    setOrderID(id);
                    setModalPay(true);
                  }}
                >
                  Đóng lãi
                </Button>
                <Button
                  className="button-action-detail"
                  onClick={() => {
                    setOrderID(id);
                    setOpenDrawer(true);
                  }}
                >
                  Chi tiết
                </Button>
                <Button
                  className="button-action-delete"
                  // onClick={()=>handleDelete(id)}
                  onClick={() => {
                    setOrderID(id);
                    setOpenModalDelete(true);
                  }}
                >
                  Xóa
                </Button>
              </div>
            ),
          },
        ]
      : [
          {
            title: "Mã đơn",
            dataIndex: "ID",
            key: "ID",
          },
          {
            title: "Mã khách hàng",
            dataIndex: "CustomerID",
            key: "CustomerID",
            render: (id) => (
              <div className="info-id-customer">
                <div className="info-id-customer-id">{id}</div>
                <div className="info-id-customer-icon">
                  <Popover
                    content={popoverContent[id]}
                    trigger="hover"
                    title="Thông tin chi tiết khách hàng:"
                  >
                    <InfoCircleOutlined onMouseEnter={() => fetchData(id)} />
                  </Popover>
                </div>
              </div>
            ),
          },
          {
            title: "Tên xe",
            dataIndex: "ProductName",
            key: "ProductName",
          },
          {
            title: "Biển số",
            dataIndex: "LicensePlate",
            key: "LicensePlate",
            width: 200,
          },
          {
            title: "Giá ước lượng",
            dataIndex: "EstimatedPrice",
            key: "EstimatedPrice",
            render: (amount) => <FormatCurrency amount={amount} />,
          },
          {
            title: "Giá cầm",
            dataIndex: "Price",
            key: "Price",
            render: (amount) => <FormatCurrency amount={amount} />,
          },
          {
            title: "Lãi suất",
            dataIndex: "Percent",
            key: "Percent",
            render: (percent) => convertPercent(percent),
          },
          {
            title: "Trạng thái hồ sơ",
            dataIndex: "Status",
            key: "Status",
            width: 250, // Độ rộng của cột
            render: (stt) => (
              <span style={{ width: "200px" }}>{convertStatus(stt)}</span>
            ),
          },
          {
            title: "Ngày cầm",
            dataIndex: "OrderDate",
            key: "OrderDate",
            render: (date) => moment(date).format("DD/MM/YYYY"),
          },
          {
            title: "Ngày hết hạn",
            dataIndex: "ReturnDate",
            key: "ReturnDate",
            render: (day) => (
              <div style={{ color: "red", fontWeight: "bold" }}>
                {moment(day).format("DD/MM/YYYY")}
              </div>
            ),
          },
          {
            title: "",
            dataIndex: "ID",
            key: "ID",
            render: (id) => (
              <div className="button-action">
                <Button
                  className="button-action-edit"
                  onClick={() => {
                    setOrderID(id);
                    setOpenDrawerEdit(true);
                  }}
                >
                  Sửa
                </Button>
                <Button
                  className="button-action-detail"
                  onClick={() => {
                    setOrderID(id);
                    setModalPay(true);
                  }}
                >
                  Đóng lãi
                </Button>
                <Button
                  className="button-action-detail"
                  onClick={() => {
                    setOrderID(id);
                    setOpenDrawer(true);
                  }}
                >
                  Chi tiết
                </Button>
                <Button
                  className="button-action-delete"
                  onClick={() => {
                    setOrderID(id);
                    setOpenModalDelete(true);
                  }}
                >
                  Xóa
                </Button>
              </div>
            ),
          },
        ];

  const handleDelete = (id) => {
    BaseURL
      .delete(`delete-order-goal/${id}`)
      .then((response) => {
        if (response.status === 200) {
          setResetFlag(resetFlag + 1);
          setOpenModalDelete(false);
        }
      });
  };
  return (
    <>
      <Table
        columns={columns}
        dataSource={currentPageData}
        pagination={false}
      />
      <Pagination
        className="pagination-category"
        current={currentPage}
        pageSize={pageSize}
        total={totalItems}
        onChange={handlePageChange}
      />
      {modalPay && (
        <PayGoal
          isOpen={modalPay}
          setIsOpen={setModalPay}
          orderID={orderID}
          resetFlag={resetFlag}
          setResetFlag={setResetFlag}
        />
      )}
      {isOrder == 0 && openDrawer && (
        <DrawerDetail
          open={openDrawer}
          setOpen={setOpenDrawer}
          orderID={orderID}
        />
      )}
      {isOrder == 1 && openDrawer && (
        <DrawerDetailVehicle
          open={openDrawer}
          setOpen={setOpenDrawer}
          orderID={orderID}
        />
      )}
      {isOrder == 0 && openDrawerEdit && (
        <DrawerEditOrder
          isOpen={openDrawerEdit}
          setIsOpen={setOpenDrawerEdit}
          orderID={orderID}
          resetFlag={resetFlag}
          setResetFlag={setResetFlag}
        />
      )}
      {isOrder == 1 && openDrawerEdit && (
        <DrawerEditVehicle
          isOpen={openDrawerEdit}
          setIsOpen={setOpenDrawerEdit}
          orderID={orderID}
          resetFlag={resetFlag}
          setResetFlag={setResetFlag}
        />
      )}
      {openModalDelete && (
        <ModalDelete
          isOpen={openModalDelete}
          setIsOpen={setOpenModalDelete}
          handleDelete={handleDelete}
          id={orderID}
        />
      )}
    </>
  );
};
export default Over60dOrdersTable;
