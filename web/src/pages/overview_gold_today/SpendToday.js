import { Pagination, Table } from "antd";
import BaseURL from "../../api/baseURL";
import { useEffect, useState } from "react";
import { FormatCurrency } from "../../helpers/Help";
import moment from "moment";

const SpendToday = () => {
    const [combinedData, setCombinedData] = useState([]);
    const [orderData, setOrderData] = useState([]);
    const [paymentData, setPaymentData] = useState([]);
    useEffect(() => {
        const fetchOrderData = async () => {
            try {
                const orderDataResponse = await BaseURL.get("overview/order-gold-today");
                setOrderData(orderDataResponse.data);
            } catch (error) {
                console.error("Error fetching order data:", error);
            }
        };

        const fetchPaymentData = async () => {
            try {
                const paymentDataResponse = await BaseURL.get("overview/add-payment-gold-today");
                setPaymentData(paymentDataResponse.data);
            } catch (error) {
                console.error("Error fetching payment data:", error);
            }
        };

        fetchOrderData();
        fetchPaymentData();
    }, []);

    useEffect(() => {
        const combineData = (orderData, paymentData) => {
            const combinedData = [];
            if (orderData === null) {
                paymentData && paymentData.forEach((payment) => {
                    combinedData.push({
                        id: payment.add_payment.id_gold,
                        price: payment.add_payment.amount, // Đặt giá trị order_goal là null vì không tìm thấy dữ liệu từ orderData
                        date_payment: payment.add_payment.date_add_payment,
                        products: payment.products,
                        price_old: null,
                        note: 1
                    });

                });
            } else {
                const orderIdsSet = new Set(orderData.map((order) => order.order.ID));

                orderData && orderData.forEach((order) => {
                    const matchingPayment = paymentData && paymentData.find((payment) => payment.add_payment.id_gold === order.order.ID);
                    if (matchingPayment) {
                        combinedData.push({
                            id: order.order.ID,
                            price: order.order.Price,
                            date: order.order.OrderDate,
                            price_old: order.order.PriceOld,
                            products: order.products,
                            note: 0
                        });
                    } else {
                        // Nếu không có "id" trùng nhau, lấy dữ liệu từ API "add-payment/get-by-month-and-year"
                        combinedData.push({
                            id: order.order.ID,
                            price: order.order.Price,
                            date: order.order.OrderDate,
                            price_old: order.order.PriceOld,
                            products: order.products,
                            note: 0
                        });
                    }
                });

                // Lấy dữ liệu từ API "add-payment/get-by-month-and-year" cho các "id" chưa có trong combinedData
                paymentData && paymentData.forEach((payment) => {
                    if (!orderIdsSet.has(payment.add_payment.id_gold)) {
                        combinedData.push({
                            id: payment.add_payment.id_gold,
                            price: payment.add_payment.amount, // Đặt giá trị order_goal là null vì không tìm thấy dữ liệu từ orderData
                            date_payment: payment.add_payment.date_add_payment,
                            products: payment.products,
                            price_old: null,
                            note: 1
                        });
                    }
                });
            }


            return combinedData;
        };
        // Call the combineData function and set the state with the combined data
        const combinedData = combineData(orderData, paymentData);
        setCombinedData(combinedData);
    }, [orderData, paymentData]);

    const dataSource = combinedData && combinedData.map((item, index) => ({
        key: index,
        ID: item.id,
        Price: item.price,
        OrderDate: item.date,
        DateAddPayment: item.date_payment,
        PriceOld: item.price_old,
        Note: item.note,
        Products: item.products,
    }));
    const [currentPage, setCurrentPage] = useState(1); // Trang hiện tại
    // Số lượng tổng cộng các mục dữ liệu
    const totalItems = dataSource && dataSource.length;
    const pageSize = 3; // Kích thước trang

    const handlePageChange = (page) => {
        setCurrentPage(page);
    };
    // Lấy dữ liệu trang hiện tại
    const currentPageData = dataSource && dataSource.slice(
        (currentPage - 1) * pageSize,
        currentPage * pageSize
    );
    const columns = [
        {
            title: "Mã đơn",
            dataIndex: "ID",
            key: "ID",
        },
        {
            title: "Sản phẩm",
            dataIndex: "Products",
            key: "Products",
            render: (product) =>
                product &&
                product.map((item, index) => (
                    <div key={index}>
                        <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>{" "}
                        (
                        {item.DetailProducts &&
                            item.DetailProducts.map((detail, index) => {
                                // Make sure to return the JSX element here
                                return <span key={index}>{detail.Count}{" "}{detail.CategoryName}{index < item.DetailProducts.length - 1 && " + "}</span>;
                            })}
                        ) - {item.Product.CategoryGoldName} - {item.Product.WeightGoal} chỉ
                    </div>
                ))
        },
        {
            title: "Ngày cầm",
            dataIndex: "OrderDate",
            key: "OrderDate",
            render: (date) => moment(date).format("DD/MM/YYYY"),
        },
        {
            title: "Giá cầm",
            dataIndex: "Price",
            key: "Price",
            render: (amount) => <FormatCurrency amount={amount} />,
        },
        {
            title: "Ghi chú",
            dataIndex: "Note",
            key: "Note",
            render: (note, record) => {
                let price = record.Price - record.PriceOld
                if (note == 1) {
                    return <div className="add-payment-note">Cầm thêm</div>
                }
                if (record.PriceOld) {
                    return <div className="add-payment-note">
                        {price > 0 && <>Có cầm thêm <FormatCurrency amount={price} /></>}
                    </div>
                }
            }
        },
    ];
    return (
        <div className="table-container">
            <Table dataSource={currentPageData} columns={columns} pagination={false} />
            <Pagination
                className="pagination-category"
                current={currentPage}
                pageSize={pageSize}
                total={totalItems}
                onChange={handlePageChange}
            />

        </div>
    );
};
export default SpendToday;
