import React from "react";
import { lazyRetry } from "./helpers/commonHelper";
import PrintOverView from "./pages/print/PrintOverview";
import SpendTableGold from "./pages/dashboard/SpendTableGold";


const Home = React.lazy(() => lazyRetry(() => import("./pages/Home"), "Home"));
const HomeVehicle = React.lazy(() => lazyRetry(() => import("./pages/HomeVehicle"), "HomeVehicle"));
const SignIn = React.lazy(() => lazyRetry(() => import("./pages/account/SignIn"), "SignIn"));
const OrderGoal = React.lazy(() => lazyRetry(() => import("./pages/order_goal/OrderGoal"), "OrderGoal"));
const OrderVehicle = React.lazy(() => lazyRetry(() => import("./pages/order_vehicle/OrderVehicle"), "OrderVehicle"));


const Customer = React.lazy(() => lazyRetry(() => import("./pages/customer/Customer"), "Customer"));
const ListCustomer = React.lazy(() => lazyRetry(() => import("./pages/customer/ListCustomer"), "ListCustomer"));
const Category = React.lazy(() => lazyRetry(() => import("./pages/category/Category"), "Category"));
const Notification = React.lazy(() => lazyRetry(() => import("./pages/notification/Notification"), "Notification"));
const ListOrderVehicle = React.lazy(() => lazyRetry(() => import("./pages/order_vehicle/ListOrderVehicle"), "ListOrderVehicle"));
const ListOrderGoal = React.lazy(() => lazyRetry(() => import("./pages/order_goal/ListOrderGoal"), "ListOrderGoal"));
export const PublicRouter = [
  {
    path: "/",
    exact: true,
    name: "Login",
    component: <SignIn />,
  },
  {
    path: "/sign-in",
    exact: true,
    name: "Login",
    component: <SignIn />,
  }
]
export const PrivateRouter = [
  {
    path: "/test",
    exact: true,
    name: "SpendTableGold",
    component: <SpendTableGold />,
  },
  {
    path: "/home",
    exact: true,
    name: "Home",
    component: <Home />,
  },
  {
    path: "/home-vehicle",
    exact: true,
    name: "HomeVehicle",
    component: <HomeVehicle />,
  },
  {
    path: "/order-goal",
    exact: true,
    name: "OrderGoal",
    component: <OrderGoal />,
  },
  {
    path: "/order-vehicle",
    exact: true,
    name: "OrderVehicle",
    component: <OrderVehicle />,
  },
  {
    path: "/list-goal",
    exact: true,
    name: "ListOrderGoal",
    component: <ListOrderGoal />,
  },
  {
    path: "/list-vehicle",
    exact: true,
    name: "ListOrderVehicle",
    component: <ListOrderVehicle />,
  },
  {
    path: "/customer",
    exact: true,
    name: "Customer",
    component: <Customer />,
  },
  {
    path: "/list-customer",
    exact: true,
    name: "ListCustomer",
    component: <ListCustomer />,
  },
  {
    path: "/category",
    exact: true,
    name: "Category",
    component: <Category />,
  },
  {
    path: "/notification",
    exact: true,
    name: "Notification",
    component: <Notification />,
  },
  {
    path: "/test",
    exact: true,
    name: "Print",
    component: <PrintOverView />,
  },
];
