import { Badge } from "antd";
import { useState } from "react";
import SignOut from "../pages/account/SignOut";
import ChangePassword from "../pages/account/ChangePassword";

const TheSideBar = ({
  isExpand,
  setIsExpand,
  active,
  setActive,
  checkSubMenuCustomer,
  checkSubMenuGoal,
  checkSubMenuVehicle,
  numberNotification,
  numberNotificationVehicle,
}) => {
  const [subMenu, setSubMenu] = useState(false);
  const isSubMenuVisible = subMenu || (checkSubMenuCustomer && true);
  const [subMenuGoal, setSubMenuGoal] = useState(false);
  const isSubMenuGoalVisible = subMenuGoal || (checkSubMenuGoal && true);
  const [isChangePass, setIsChangePass] = useState(false);
  const [subMenuVehicle, setSubMenuVehicle] = useState(false);

  const isSubMenuVehicleVisible = subMenuVehicle || (checkSubMenuVehicle && true);
  
  const handleChangePassword = ()=>{
    setIsChangePass(true);
  }

  return (
    <>
      <div class={isExpand ==true ? "sidebar open" :"sidebar"} data-color="white" data-active-color="danger">
        <div class="logo">
          <a
          onClick={()=>setIsExpand(!isExpand)}
            class="simple-text logo-normal"
          >
            <img src="KH.png" width={250} />
          </a>
        </div>
        <div class="sidebar-wrapper">
          <ul class="nav">
            <li class={active === 0 && "active"}>
              <a href="/home">
                <i class="fa fa-university" aria-hidden="true"></i>
                <p>Thống kê vàng</p>
              </a>
            </li>
            <li class={active === 1 && "active"}>
              <a href="/home-vehicle">
                <i class="fa fa-tachometer" aria-hidden="true"></i>
                <p>Thống kê xe</p>
              </a>
            </li>
            <li class={active === 2 && "active"}>
              <a href="/notification">
                {" "}
                <i class="fa fa-bell-o" aria-hidden="true"></i>
                <div className="notification">
                  <p>Thông báo</p>
                  <div className="notification-icon">
                    <span className="icon">
                      {/* Notification icon SVG or image */}
                    </span>
                    {numberNotification > 0 && (
                      <span className="count">{numberNotification}</span>
                    )}
                  </div>
                  <div className="notification-icon-vehicle">
                    <span className="icon">
                    </span>
                    {numberNotificationVehicle > 0 && (
                      <span className="count">{numberNotificationVehicle}</span>
                    )}
                  </div>
                </div>
              </a>
            </li>
            <li class={(active === 3 || active === 4) && "active"}>
              <a
                onClick={() => {
                  setSubMenuGoal(!subMenuGoal);
                }}
              //href="/order-goal"
              >
                <i class="fa fa-diamond" aria-hidden="true"></i>
                <p>Vàng</p>
              </a>
              {isSubMenuGoalVisible && (
                <ul className="dropdown-nav">
                  <li class={active === 3 && "active"}>
                    <a href="/order-goal">
                      <i class="fa fa-plus-square-o" aria-hidden="true"></i>
                      <p>Thêm đơn vàng</p>
                    </a>
                  </li>
                  <li class={active === 4 && "active"}>
                    <a href="/list-goal">
                      <i class="fa fa-th-list" aria-hidden="true"></i>
                      <p>QUẢN LÝ ĐƠN</p>
                    </a>
                  </li>
                </ul>
              )}
            </li>
            <li class={(active === 5 || active === 6) && "active"}>
              <a
                onClick={() => {
                  //setActive(3);
                  setSubMenuVehicle(!subMenuVehicle);
                }}
              //href="/order-vehicle"
              >
                <i class="fa fa-motorcycle" aria-hidden="true"></i>
                <p>Xe</p>
              </a>
              {isSubMenuVehicleVisible && (
                <ul className="dropdown-nav">
                  <li class={active === 5 && "active"}>
                    <a href="/order-vehicle">
                      <i class="fa fa-plus-square-o" aria-hidden="true"></i>
                      <p>Thêm đơn xe</p>
                    </a>
                  </li>
                  <li class={active === 6 && "active"}>
                    <a href="/list-vehicle">
                      <i class="fa fa-th-list" aria-hidden="true"></i>
                      <p>QUẢN LÝ ĐƠN</p>
                    </a>
                  </li>
                </ul>
              )}
            </li>
            <li class={(active === 7 || active === 8) && "active"}>
              <a
                onClick={() => {
                  setSubMenu(!subMenu);
                }}
              >
                <i class="fa fa-users" aria-hidden="true"></i>
                <p>Khách hàng</p>
              </a>
              {isSubMenuVisible && (
                <ul className="dropdown-nav">
                  <li class={active === 7 && "active"}>
                    <a href="/list-customer">
                      <i class="fa fa-th-list" aria-hidden="true"></i>
                      <p>Danh sách</p>
                    </a>
                  </li>
                  <li class={active === 8 && "active"}>
                    <a href="/customer">
                      <i class="fa fa-user-plus" aria-hidden="true"></i>
                      <p>Thêm KH</p>
                    </a>
                  </li>
                </ul>
              )}
            </li>
            <li class={active === 9 && "active"}>
              <a href="/category">
                <i class="fa fa-list" aria-hidden="true"></i>
                <p>Danh mục</p>
              </a>
            </li>
          </ul>

          <footer class="footer">
            <div class="container-fluid">
              <div class="row">
                <nav class="footer-nav">
                  <ul>
                    <li>
                      <a onClick={handleChangePassword}>
                        Đổi mật khẩu
                      </a>
                    </li>
                    <li>
                      <a onClick={SignOut}>
                        Đăng xuất
                      </a>
                    </li>
                  </ul>
                </nav>
              </div>
            </div>
          </footer>
        </div>

      </div>
      {isChangePass && <ChangePassword isOpen={isChangePass} setIsOpen={setIsChangePass}/>}
    </>
  );
};
export default TheSideBar;
