import React from 'react';

function LoadingIndicator() {
  return (
    <div className="loading-indicator">
      <i className="fa fa-spinner fa-spin"></i>
      <span>Loading...</span>
    </div>
  );
}

export default LoadingIndicator;