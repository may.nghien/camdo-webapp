import axios from "axios";

const BaseURL = axios.create({
    baseURL: 'http://localhost:5000/api/',
    timeout: 1000,
    headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`
    }
});
BaseURL.interceptors.request.use(
    (config) => {
        const token = localStorage.getItem('token');
        if (token) {
            config.headers.Authorization = `Bearer ${token}`;
        }
        return config;
    },
    (error) => {
        return Promise.reject(error);
    }
);
BaseURL.interceptors.response.use(
    (response) => {
        return response;
    },
    (error) => {
        if (error.response && error.response.status === 401) {
            window.location.href = '/sign-in';
        } 
        return Promise.reject(error);
    }
);

export default BaseURL