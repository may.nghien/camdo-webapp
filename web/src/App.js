import React from "react";
import "./scss/style.scss";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { PrivateRouter, PublicRouter } from "./routes";
import AuthWrapper from "./helpers/AuthWrapper";

const App = () => {
  return (
    <>
      <Router>
        <Routes>
          {PublicRouter.map((route, index) => (
            <Route
              key={index}
              exact={route.exact}
              path={route.path}
              element={route.component}
            />
          ))}
          {PrivateRouter.map((route, index) => (
            <Route
              key={index}
              exact={route.exact}
              path={route.path}
              element={route.component}
            />
          ))}
        </Routes>
      </Router>
    </>
  );
};

export default App;
